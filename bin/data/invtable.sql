PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE invtable(time real, deltaT real, Fx real, Fy real, Fz real, Wx real,Wy real ,Wz real,Wox real,Woy real, Woz real,Wopy real,I2CTemp real,psi real , theta real,gamma real,lambda real,fai  real,H real,Gpsi real,Gtheta real,Ggamma real,GAx real,GAy real,GAz real,GVx real, GVy real ,GVz real ,Ax real ,Ay real, Az real, Vx real ,Vy real ,Vz real,Sx real , Sy real ,Sz real,Glambda real ,Gfai real , Gh real,id integer primary key autoincrement);
CREATE INDEX invidx on invtable(time);
COMMIT;
