#include "FHButtonPanel.h"


#define BUTTONHEIGHT 24

CFHButtonPanel::CFHButtonPanel(void)
{
}


CFHButtonPanel::~CFHButtonPanel(void)
{
}


bool CFHButtonPanel::mousePressed(ofMouseEventArgs & args)
{
	return CFHDockPanel::mousePressed(args);
}

bool CFHButtonPanel::mouseReleased(ofMouseEventArgs & args)
{
	return CFHDockPanel::mouseReleased(args);
}
bool CFHButtonPanel::mouseMoved(ofMouseEventArgs & args)
{
	return CFHDockPanel::mouseMoved(args);
}
void CFHButtonPanel::generateDraw()
{
	CFHDockPanel::generateDraw();
	ofPoint tmpPos;
	ofRectangle b=getBodyRect();
	int buttoncount = m_pButtons.size();
	tmpPos.x = b.x + BPPADDING;
	tmpPos.y = b.y + defaultHeaderHeight + BUTTONGAP;

	vector<CFHButton*>::iterator piter = m_pButtons.begin();
	while(piter != m_pButtons.end()){
		(*piter)->move(tmpPos);
		(*piter)->generateDraw();
		tmpPos.y += BUTTONGAP + BUTTONHEIGHT;
		++piter;
	}
}
void CFHButtonPanel::render()
{
	CFHDockPanel::render();
}

void CFHButtonPanel::addButton(CFHButton & button)
{
	ofRectangle b=getBodyRect();
	button.setHeight(BUTTONHEIGHT);
	button.setWidth(b.width - 2 * BPPADDING);
	m_pButtons.push_back(&button);
	generateDraw();
}
