/*
 *  ofxSimpleSlider.cpp
 *  Created by Golan Levin on 2/24/12.
 *
 */

#include "myButton.h"

//-----------------------------------------------------------------------------------------------------------------------
void CMyButton::setup (string str){
	
	
	ofAddListener(ofEvents().draw, this, &CMyButton::draw);
	ofAddListener(ofEvents().mouseMoved, this, &CMyButton::mouseMoved);
	ofAddListener(ofEvents().mousePressed, this, &CMyButton::mousePressed);
	ofAddListener(ofEvents().mouseReleased, this, &CMyButton::mouseReleased);
	setLabelString(str);
	m_bHide = false;
	//ofAddListener(ofEvents().mouseDragged, this, &CMyButton::mouseDragged);
	//ofAddListener(ofEvents().touchDown, this, &ofxSimpleSlider::defaultTouchHandler);
	//ofAddListener(ofEvents().touchUp, this, &ofxSimpleSlider::defaultTouchHandler);
	//ofAddListener(ofEvents().touchMoved, this, &ofxSimpleSlider::defaultTouchHandler);
	//ofAddListener(ofEvents().touchCancelled, this, &ofxSimpleSlider::defaultTouchHandler);
}

void CMyButton::resize(float inw,float inh)
{
	width = inw; 
	height = inh;
	box.set(x,y, width, height); 
}

void CMyButton::enable()
{
	bEnabled = true;
}
void CMyButton::disable()
{
	bEnabled = false;
}

bool CMyButton::isEnabled()
{
	return bEnabled;
}

void CMyButton::move(float inx,float iny)
{
	x = inx;
	y = iny;
	box.setX(x);
	box.setY(y);
}

void CMyButton::move(ofPoint & pos)
{
	box.setPosition(pos);
}


//----------------------------------------------------
void CMyButton::setLabelString (string str){
	labelString = str;
	width = str.length() * 8 + 4;
	box.setWidth(width);
}


//----------------------------------------------------
void CMyButton::draw(ofEventArgs& event){
	if(m_bHide)
		return;
	ofEnableAlphaBlending();
	ofDisableSmoothing();
	ofPushMatrix();
	ofTranslate(x,y,0);
	
	// Use different alphas if we're actively maniupulating me. 
	float sliderAlpha = (bHasFocus) ? 128:64;
	float spineAlpha  = (bHasFocus) ? 255:160;
	
	// draw box outline
	ofNoFill();
	ofSetLineWidth(1.0);
	ofSetColor(64,64,64, sliderAlpha); 
	ofRect(0,0, width,height); 
	
	// draw spine
	float labelStringWidth = labelString.size();
	ofSetLineWidth(1.0);
	ofSetColor(0,0,0, spineAlpha); 
	if (bPressed){
		ofLine(0,0, width,0); 
		ofLine(0,0,0,height);
		ofDrawBitmapString( labelString, width / 2 - labelStringWidth*8 /2 + 1, height/2 + 4 + 1); 
	} 
	else
		ofDrawBitmapString( labelString, width / 2 - labelStringWidth*8 /2, height/2 + 4); 
	
	//ofDrawBitmapString( ofToString(getValue(),numberDisplayPrecision), width+5,height);
	ofPopMatrix();
	ofSetLineWidth(1.0);
	ofDisableAlphaBlending();
}

//----------------------------------------------------
void CMyButton::mouseMoved(ofMouseEventArgs& event)
{
	if(bEnabled && box.inside(event.x,event.y)){
		bHasFocus = true;
	}
	else{
		bHasFocus = false;
	}
}
void CMyButton::mouseDragged(ofMouseEventArgs& event){
	if (bEnabled && bHasFocus){
		//updatePercentFromMouse (event.x, event.y); 
	}
}
void CMyButton::mousePressed(ofMouseEventArgs& event){
	
	if (bEnabled && box.inside(event.x, event.y) && bHasFocus){
		bPressed = true;
	}
}
void CMyButton::mouseReleased(ofMouseEventArgs& event){
	if (bEnabled && bHasFocus){
		if (box.inside(event.x, event.y)){
			int ea = 1;
			ofNotifyEvent(btnEvent,ea,this);
		}
	}
	bPressed = false;
}

void CMyButton::defaultMouseHandler(ofMouseEventArgs & event){
	if(box.inside(event.x,event.y)){
		std::ostringstream ost;
		ost << "defaultMouseHandler:" << " x:" << event.x << " y:" << event.y; 
		ost << " button: " << event.button;
		ofLog(OF_LOG_ERROR,"Failed to get the environment using GetEnv()");
	}
}

void CMyButton::defaultTouchHandler(ofTouchEventArgs & event){
	if(box.inside(event.x,event.y)){
		std::ostringstream ost;
		ost << "defaultTouchHandler:" << " x:" << event.x << " y:" << event.y; 
		ost << " type: " << event.type;
		ofLog(OF_LOG_ERROR,"Failed to get the environment using GetEnv()");
	}
}

		

