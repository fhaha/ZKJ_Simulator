#ifndef FHUTIL_H_INCLUDED
#define FHUTIL_H_INCLUDED
#include <string>

std::string ws2s(const std::wstring& ws);
std::wstring s2ws(const std::string& s);
std::string UTF82Ansi(const std::string s);
std::string ws2utf8(const std::wstring & ws);

#endif // FHUTIL_H_INCLUDED
