//
//  FHLogWindow.cpp
//  ZKJ_Simulator
//
//  Created by auser on 15/4/23.
//
//

#include <stdio.h>
#include "FHLogWindow.h"

#include "ofxPanel.h"
#include "ofxSliderGroup.h"
#include "ofGraphics.h"
#include "ofMain.h"
#include "ofxLabel.h"

CFHLogWindow::CFHLogWindow(){
    minimized = false;
    parent = NULL;
    spacing  = 1;
    spacingNextElement = 3;
    header = defaultHeight;
    bGuiActive = false;
    m_logindex=0;
}

CFHLogWindow::CFHLogWindow(const ofParameterGroup & parameters, string filename, float x, float y){
    minimized = false;
    parent = NULL;
    setup(parameters,LOGWINDEFWIDTH, filename, x, y);
}

CFHLogWindow * CFHLogWindow::setup(string collectionName, float windowwidth,string filename, float x, float y){
    parameters.setName(collectionName);
    
    CFHLogWindow* ret = setup(parameters,windowwidth,filename,x,y);
    for(int i = 0;i<LOGLINENUM;i++){
        add(logstring[i].set("."));
    }
    return ret;
}

CFHLogWindow * CFHLogWindow::setup(const ofParameterGroup & _parameters, float windowwidth,string _filename, float x, float y){
    b.x = x;
    b.y = y;
    header = defaultHeight;
    spacing  = 1;
    spacingNextElement = 3;
    b.width = windowwidth;
    
    clear();
    filename = _filename;
    bGuiActive = false;
    
    for(int i=0;i<_parameters.size();i++){
        string type = _parameters.getType(i);
        if(type==typeid(ofParameter<int>).name()){
            ofParameter<int> p = _parameters.getInt(i);
            add(p);
        }else if(type==typeid(ofParameter<float>).name()){
            ofParameter<float> p = _parameters.getFloat(i);
            add(p);
        }else if(type==typeid(ofParameter<bool>).name()){
            ofParameter<bool> p = _parameters.getBool(i);
            add(p);
        }else if(type==typeid(ofParameter<ofVec2f>).name()){
            ofParameter<ofVec2f> p = _parameters.getVec2f(i);
            add(p);
        }else if(type==typeid(ofParameter<ofVec3f>).name()){
            ofParameter<ofVec3f> p = _parameters.getVec3f(i);
            add(p);
        }else if(type==typeid(ofParameter<ofVec4f>).name()){
            ofParameter<ofVec4f> p = _parameters.getVec4f(i);
            add(p);
        }else if(type==typeid(ofParameter<ofColor>).name()){
            ofParameter<ofColor> p = _parameters.getColor(i);
            add(p);
        }else if(type==typeid(ofParameter<ofShortColor>).name()){
            ofParameter<ofShortColor> p = _parameters.getShortColor(i);
            add(p);
        }else if(type==typeid(ofParameter<ofFloatColor>).name()){
            ofParameter<ofFloatColor> p = _parameters.getFloatColor(i);
            add(p);
        }else if(type==typeid(ofParameter<string>).name()){
            ofParameter<string> p = _parameters.getString(i);
            add(p);
        }else if(type==typeid(ofParameterGroup).name()){
            ofParameterGroup p = _parameters.getGroup(i);
            CFHLogWindow * panel = new CFHLogWindow(p);
            add(panel);
        }else{
            ofLogWarning() << "ofxBaseGroup; no control for parameter of type " << type;
        }
    }
    
    parameters = _parameters;
    registerMouseEvents();
    
    generateDraw();
    
    return this;
}

void CFHLogWindow::add(ofxBaseGui * element){
    collection.push_back( element );
    
    element->setPosition(b.x, b.y + b.height  + spacing);
    
    b.height += element->getHeight() + spacing;
    
    //if(b.width<element->getWidth()) b.width = element->getWidth();
    
    element->unregisterMouseEvents();
    
    CFHLogWindow * subgroup = dynamic_cast<CFHLogWindow*>(element);
    if(subgroup!=NULL){
        subgroup->filename = filename;
        subgroup->parent = this;
        subgroup->setWidthElements(b.width*.98);
    }else{
        if(parent!=NULL){
            element->setSize(b.width*.98,element->getHeight());
            element->setPosition(b.x + b.width-element->getWidth(),element->getPosition().y);
        }
    }
    
    parameters.add(element->getParameter());
    generateDraw();
}

void CFHLogWindow::setWidthElements(float w){
    for(int i=0;i<(int)collection.size();i++){
        collection[i]->setSize(w,collection[i]->getHeight());
        collection[i]->setPosition(b.x + b.width-w,collection[i]->getPosition().y);
        CFHLogWindow * subgroup = dynamic_cast<CFHLogWindow*>(collection[i]);
        if(subgroup!=NULL){
            subgroup->setWidthElements(w*.98);
        }
    }
    sizeChangedCB();
    generateDraw();
}

void CFHLogWindow::add(const ofParameterGroup & parameters){
    CFHLogWindow * panel = new CFHLogWindow(parameters);
    panel->parent = this;
    add(panel);
}

void CFHLogWindow::add(ofParameter<float> & parameter){
    add(new ofxFloatSlider(parameter,b.width));
}

void CFHLogWindow::add(ofParameter<int> & parameter){
    add(new ofxIntSlider(parameter,b.width));
}

void CFHLogWindow::add(ofParameter<bool> & parameter){
    add(new ofxToggle(parameter,b.width));
}

void CFHLogWindow::add(ofParameter<string> & parameter){
    add(new ofxLabel(parameter,b.width));
}

void CFHLogWindow::add(ofParameter<ofVec2f> & parameter){
    add(new ofxVecSlider_<ofVec2f>(parameter,b.width));
}

void CFHLogWindow::add(ofParameter<ofVec3f> & parameter){
    add(new ofxVecSlider_<ofVec3f>(parameter,b.width));
}

void CFHLogWindow::add(ofParameter<ofVec4f> & parameter){
    add(new ofxVecSlider_<ofVec4f>(parameter,b.width));
}

void CFHLogWindow::add(ofParameter<ofColor> & parameter){
    add(new ofxColorSlider_<unsigned char>(parameter,b.width));
}

void CFHLogWindow::add(ofParameter<ofShortColor> & parameter){
    add(new ofxColorSlider_<unsigned short>(parameter,b.width));
}

void CFHLogWindow::add(ofParameter<ofFloatColor> & parameter){
    add(new ofxColorSlider_<float>(parameter,b.width));
}

void CFHLogWindow::clear(){
    collection.clear();
    b.height = header + spacing + spacingNextElement ;
}

bool CFHLogWindow::mouseMoved(ofMouseEventArgs & args){
    ofMouseEventArgs a = args;
    for(int i = 0; i < (int)collection.size(); i++){
        if(collection[i]->mouseMoved(a)) return true;
    }
    if(isGuiDrawing() && b.inside(ofPoint(args.x,args.y))){
        //operator<<("moved ").operator<<(args.button).operator<<('\n');
        return true;
    }else{
        return false;
    }
}

bool CFHLogWindow::mousePressed(ofMouseEventArgs & args){
    if(setValue(args.x, args.y, true)){
        return true;
    }
    if( bGuiActive ){
        ofMouseEventArgs a = args;
        for(int i = 0; i < (int)collection.size(); i++){
            if(collection[i]->mousePressed(a)) return true;
        }
        m_lastDragPos = args;
    }
    return false;
}

bool CFHLogWindow::mouseDragged(ofMouseEventArgs & args){
    if(setValue(args.x, args.y, false)){
        return true;
    }
    if( bGuiActive ){
        ofMouseEventArgs a = args;
        for(int i = 0; i < (int)collection.size(); i++){
            if(collection[i]->mouseDragged(a)) return true;
        }
        //printf("logindex old %d\n",m_logindex);
        int vecsize = strvalues.size();
        int diff =(args.y - m_lastDragPos.y)/
        getHeight() * LOGLINENUM * LOGLINENUM;
        m_lastDragPos = args;
        if(diff != 0){
            if(diff > 0){
                if( m_logindex < vecsize-1){
                    m_logindex += diff;
                }
            }
            else if(vecsize > LOGLINENUM && m_logindex >= LOGLINENUM){//diff < 0
                m_logindex += diff;
            }
            if(m_logindex > strvalues.size()-1)
                m_logindex = strvalues.size()-1;
            if(m_logindex < LOGLINENUM){
                m_logindex = vecsize > LOGLINENUM?LOGLINENUM-1:vecsize-1;
            }
            //printf("logindex new %d diff %d\n",m_logindex,diff);
            generateDraw();
        }
    }
    return false;
}


bool CFHLogWindow::mouseReleased(ofMouseEventArgs & args){
    bGuiActive = false;
    for(int k = 0; k < (int)collection.size(); k++){
        ofMouseEventArgs a = args;
        if(collection[k]->mouseReleased(a)) return true;
    }
    if(isGuiDrawing() && b.inside(ofPoint(args.x,args.y))){
        //operator<<("released ").operator<<(args.button).operator<<('\n');
        return true;
    }else{
        return false;
    }
}

void CFHLogWindow::generateDraw(){
    border.clear();
    border.setFillColor(ofColor(thisBorderColor,180));
    border.setFilled(true);
    border.rectangle(b.x,b.y+ spacingNextElement,b.width+1,b.height);
    
    
    headerBg.clear();
    headerBg.setFillColor(thisHeaderBackgroundColor);
    headerBg.setFilled(true);
    headerBg.rectangle(b.x,b.y +1 + spacingNextElement, b.width, header);
    
    textMesh = getTextMesh(getName(), textPadding + b.x, header / 2 + 4 + b.y+ spacingNextElement);
    if(minimized){
        textMesh.append(getTextMesh("+", b.width-textPadding-8 + b.x, header / 2 + 4+ b.y+ spacingNextElement));
    }else{
        textMesh.append(getTextMesh("-", b.width-textPadding-8 + b.x, header / 2 + 4 + b.y+ spacingNextElement));
    }
    int vecsize = strvalues.size();
    //printf("vecsize %d\n",vecsize);
    if(vecsize <=0 )
        return;
    if(m_logindex > vecsize-1)
        m_logindex = vecsize -1;
    int vecindex = m_logindex;
    stringstream tmpbuf;
    for(int i = LOGLINENUM-1;i>=0 &&vecindex >=0;--i){
        tmpbuf << vecindex << ". " << strvalues[vecindex];
		//printf("i %d vecindex %d str %s\n",i,vecindex,tmpbuf.str().c_str());
        logstring[i]=tmpbuf.str() ;
        tmpbuf.clear();
        tmpbuf.str("");
        vecindex--;
    }

}

void CFHLogWindow::render(){
    border.draw();
    headerBg.draw();
    
    ofBlendMode blendMode = ofGetStyle().blendingMode;
    if(blendMode!=OF_BLENDMODE_ALPHA){
        ofEnableAlphaBlending();
    }
    ofColor c = ofGetStyle().color;
    ofSetColor(thisTextColor);
    
    bindFontTexture();
    textMesh.draw();
    unbindFontTexture();
    
    if(!minimized){
        for(int i = 0; i < (int)collection.size(); i++){
            collection[i]->draw();
        }
    }
    
    ofSetColor(c);
    if(blendMode!=OF_BLENDMODE_ALPHA){
        ofEnableBlendMode(blendMode);
    }
}

vector<string> CFHLogWindow::getControlNames(){
    vector<string> names;
    for(int i=0; i<(int)collection.size(); i++){
        names.push_back(collection[i]->getName());
    }
    return names;
}

ofxIntSlider & CFHLogWindow::getIntSlider(string name){
    return getControlType<ofxIntSlider>(name);
}

ofxFloatSlider & CFHLogWindow::getFloatSlider(string name){
    return getControlType<ofxFloatSlider>(name);
}

ofxToggle & CFHLogWindow::getToggle(string name){
    return getControlType<ofxToggle>(name);
}

ofxButton & CFHLogWindow::getButton(string name){
    return getControlType<ofxButton>(name);
}

CFHLogWindow & CFHLogWindow::getGroup(string name){
    return getControlType<CFHLogWindow>(name);
}

ofxBaseGui * CFHLogWindow::getControl(string name){
    for(int i=0; i<(int)collection.size(); i++){
        if(collection[i]->getName()==name){
            return collection[i];
        }
    }
    return NULL;
}

bool CFHLogWindow::setValue(float mx, float my, bool bCheck){
    
    if( !isGuiDrawing() ){
        bGuiActive = false;
        return false;
    }
    
    
    if( bCheck ){
        if( b.inside(mx, my) ){
            bGuiActive = true;
            
            ofRectangle minButton(b.x+b.width-textPadding*3,b.y,textPadding*3,header);
            if(minButton.inside(mx,my)){
                minimized = !minimized;
                if(minimized){
                    minimize();
                }else{
                    maximize();
                }
                return true;
            }
        }
    }
    
    return false;
}

void CFHLogWindow::minimize(){
    minimized=true;
    int headerheight = header + spacing + spacingNextElement + 1;
    b.y += b.height - headerheight;
    b.height = headerheight /*border*/;
    
    if(parent) parent->sizeChangedCB();
    generateDraw();
}

void CFHLogWindow::maximize(){
    minimized=false;
    for(int i=0;i<(int)collection.size();i++){
        b.height += collection[i]->getHeight() + spacing;
    }
    int headerheight = header + spacing + spacingNextElement + 1;
    b.y -= (b.height -  headerheight);
    if(parent) parent->sizeChangedCB();
    generateDraw();
}

void CFHLogWindow::minimizeAll(){
    for(int i=0;i<(int)collection.size();i++){
        CFHLogWindow * group = dynamic_cast<CFHLogWindow*>(collection[i]);
        if(group)group->minimize();
    }
}

void CFHLogWindow::maximizeAll(){
    for(int i=0;i<(int)collection.size();i++){
        CFHLogWindow * group = dynamic_cast<CFHLogWindow*>(collection[i]);
        if(group)group->maximize();
    }
}

void CFHLogWindow::sizeChangedCB(){
    float y;
    if(parent){
        y = b.y  + header + spacing + spacingNextElement;
    }else{
        y = b.y  + header + spacing;
    }
    for(int i=0;i<(int)collection.size();i++){
        collection[i]->setPosition(collection[i]->getPosition().x,y + spacing);
        y += collection[i]->getHeight() + spacing;
    }
    b.height = y - b.y;
    if(parent) parent->sizeChangedCB();
    generateDraw();
}


int CFHLogWindow::getNumControls(){
    return collection.size();
}

ofxBaseGui * CFHLogWindow::getControl(int num){
    if(num<(int)collection.size())
        return collection[num];
    else
        return NULL;
}

ofAbstractParameter & CFHLogWindow::getParameter(){
    return parameters;
}

void CFHLogWindow::setPosition(ofPoint p){
    ofVec2f diff = p - b.getPosition();
    
    for(int i=0;i<(int)collection.size();i++){
        collection[i]->setPosition(collection[i]->getPosition()+diff);
    }
    
    b.setPosition(p);
    generateDraw();
}

void CFHLogWindow::setPosition(float x, float y){
    setPosition(ofVec2f(x,y));
}

template<class ControlType>
ControlType & CFHLogWindow::getControlType(string name){
    ControlType * control = dynamic_cast<ControlType*>(getControl(name));
    if(control){
        return *control;
    }else{
        ofLogWarning() << "getControlType " << name << " not found, creating new";
        control = new ControlType;
        control->setName(name);
        add(control);
        return *control;
    }
}

