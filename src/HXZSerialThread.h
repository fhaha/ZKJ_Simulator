#pragma once
#include <ofThread.h>
#include <ofMain.h>
#include "comhead.h"
#include "zdpkg.h"


class CHXZSerialThread :
	public ofThread
{
public:
	CHXZSerialThread(void);
	~CHXZSerialThread(void);

public:
	ofSerial m_serial;
	bool setup(std::string device, int baudrate);
	void threadedFunction();
	void quit();
private:
	bool m_bIsQuiting;
	bool parseNBXData(unsigned char * pdata,int len);
	unsigned char calcCRC(comhead_st * pPkg);
};

