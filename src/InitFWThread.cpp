#include "InitFWThread.h"
#include "ofLog.h"
#include "ofApp.h"
#include <errno.h>
#include <fstream>
#include <Poco/ScopedLock.h>

CInitFWThread::CInitFWThread(void)
{
}


CInitFWThread::~CInitFWThread(void)
{
}

void CInitFWThread::loadData(std::string & filename)
{
	std::fstream fdata(filename.c_str());
	if(!fdata.is_open()){
		ofLogWarning("FW") << "open " << filename << " file to read data failed for " << strerror(errno);
		return;
	}
	int firsttemp = -500;
	std::string str;
	m_cfgs.clear();
	getline(fdata,str);
	while(getline(fdata,str)){
		std::stringstream sstr ;
		fwcfg ncfg;
		sstr << str;
		sstr >> ncfg.ffactor[0] >> ncfg.df[0] >> ncfg.ffactor[1] >> ncfg.df[1] >> ncfg.ffactor[2] >> ncfg.df[2] >> ncfg.dw[0]  >> ncfg.dw[1] >> ncfg.dw[2] >> ncfg.dw[3]>>ncfg.temperature ;
		if(firsttemp==-500){
			firsttemp = int(ncfg.temperature * 10);
		}
		Poco::ScopedLock<Poco::Mutex> lock(_mutex);
		m_cfgs.push_back(ncfg);
	}
	m_fwQueue.enqueueNotification(new FWNotification(firsttemp));
}

void CInitFWThread::threadedFunction()
{
	while(isThreadRunning()){

		ofApp* pApp = (ofApp*) ofGetAppPtr();
		AutoPtr<Notification> pN2f(m_fwQueue.waitDequeueNotification(1));
		while(pN2f){
			FWNotification * pNot = dynamic_cast<FWNotification*>(pN2f.get());
			if(pNot){
				pApp->m_zkjQueue.enqueueNotification(new FWNotification((int)m_cfgs.size()));
				if(m_cfgs.empty()){
					//TODO

					break;
				}
				Poco::ScopedLock<Poco::Mutex> lock(_mutex);
				fwcfg& ncfg=*m_cfgs.begin();

				pApp->writeFWCfg(ncfg);
				m_cfgs.erase(m_cfgs.begin());
			}
			pN2f = m_fwQueue.waitDequeueNotification(1);
		}
	}
}
