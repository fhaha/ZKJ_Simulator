#pragma once

static char tofilestr[1024]={0};

#define SOSF(format, ...)	if(m_bLog)do{sprintf(tofilestr,format,##__VA_ARGS__);\
				ofLogVerbose("ZKJ") << tofilestr ;
				

#define SOSHEAD()	SOSF("\n \t \t \t \t \t \t \t \t \t \t \t")
#define SOSLHEAD()	SOSF("\n \t \t \t \t \t \t \t \t \t \t \t \t")

#define SOSKSSHEAD(STEPN)	SOSF(" \t \t \t \t \t \t \t \t \t \t" #STEPN )

#define SOSKSHEAD(STEPN)	SOSF(" \t \t \t \t \t \t \t \t \t \t" #STEPN )

#define POSF(format, ...)	if(m_bLog)do{sprintf(tofilestr,format,##__VA_ARGS__);\
				if(m_bSaveToFile)\
					calfile << tofilestr ;\
				else \
					printf(tofilestr);}while(0)

#define POSLD(line,Mat)		if(m_bLog)do{for(int bbi=0;bbi<line;++bbi){\
								POSF("\t\t");\
								for(int bbj=0;bbj<line;++bbj){\
										POSF("[%02d][%02d]%13g(%16a) ",bbi,bbj,Mat[bbi][bbj],Mat[bbi][bbj]);\
										/*POSF("%g ",Mat[bbi][bbj]);*/\
								}\
								POSF("\n");\
							}}while(0)

#define SOSLD(line,Mat)		if(m_bLog)do{for(int bbi=0;bbi<line;++bbi){\
								SOSLHEAD();\
								for(int bbj=0;bbj<line;++bbj){\
										SOSF("%13g\t",Mat[bbi][bbj]);\
								}\
							}}while(0)

#define POSLU(line,Mat,num) if(m_bLog)do{for(int bbi=0;bbi<line;++bbi){\
								POSF("\t\t");\
								for(int bbj=0;bbj<line;++bbj){\
										POSF("[%02d][%02d]%13g(%16a) ",bbi,bbj,Mat[bbi][bbj]*num,Mat[bbi][bbj]*num);/*POSF("%g,",Mat[bbi][bbj]);*/\
								}\
								POSF("\n");\
							}}while(0)

#define POSLV(num,Vec)		if(m_bLog)do{for(int bbi=0;bbi<num;++bbi){\
								POSF("%13g(%16a) ",Vec[bbi],Vec[bbi]);\
								/*POSF("%g ",Vec[bbi]);*/\
							}\
							POSF("\n");}while(0)

#define SOSLV(num,Vec)		if(m_bLog)do{for(int bbi=0;bbi<num;++bbi){\
								SOSF("\t%13g",Vec[bbi]);\
							}}while(0)

#define POSLN(row,col,Mat)		if(m_bLog)do{for(int bbi=0;bbi<row;++bbi){\
								POSF("\t\t");\
								for(int bbj=0;bbj<col;++bbj){\
										POSF("[%02d][%02d]%13g(%16a) ",bbi,bbj,Mat[bbi][bbj],Mat[bbi][bbj]);\
										/*POSF("%g ",Mat[bbi][bbj]);*/\
								}\
								POSF("\n");\
							}}while(0)

#define SOSLN(row,col,Mat)	if(m_bLog)do{for(int bbi=0;bbi<row;++bbi){\
								SOSLHEAD();\
								for(int bbj=0;bbj<col;++bbj){\
										SOSF("\t%13g",bbi,bbj,Mat[bbi][bbj]);\
								}\
							}}while(0)