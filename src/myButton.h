/*
 *  ofxSimpleSlider.h
 *  Created by Golan Levin on 2/24/12.
 *
 */


#pragma once

#include "ofMain.h"

class CMyButton {

	public:
		CMyButton(){
			x = 0;
			y = 0; 
			width = 50; 
			height = 20;
			box.set(x,y, width, height); 
	
			bHasFocus = false;
			bEnabled = true;
			bPressed = false;
			labelString = ""; 
		}
		~CMyButton(){

		}
		void	setup (string str="");
		void	resize(float inw,float inh);
		void	move(float inx,float iny);
		void	move(ofPoint & pos);
		void	enable();
		void	disable();
		bool	isEnabled();
		void	draw(ofEventArgs& event);
		void	mouseMoved(ofMouseEventArgs& event);
		void	mouseDragged(ofMouseEventArgs& event);
		void	mousePressed(ofMouseEventArgs& event);
		void	mouseReleased(ofMouseEventArgs& event);
		void	defaultMouseHandler(ofMouseEventArgs & event);
		void	defaultTouchHandler(ofTouchEventArgs & event);
	
		
		void	setLabelString (string str);
		ofEvent<int> btnEvent;
		float getX(){return x;}
		float getY() { return y;}
		float getWidth() { return width;}
		float getHeight() { return height;}
		bool m_bHide;
	protected: 
		float	x;
		float	y; 
		float	width; 
		float	height;
		ofRectangle box; 
		bool	bHasFocus; 
		bool	bEnabled;
		bool	bPressed;
	
		string	labelString; 
	
	
	
};
