#pragma once
#include <ofThread.h>
#include <Poco/Notification.h>
#include <Poco/NotificationQueue.h>
#include <Poco/AutoPtr.h>
#include <Poco/Mutex.h>
#include "../../../../../stm32f405_inv/bsp/stm32f40x/applications/fwconfig.h"
#include <vector>
#include <string>


using Poco::Notification;
using Poco::NotificationQueue;
using Poco::AutoPtr;


class FWNotification:public Notification
{
public:
	FWNotification(int temp,int source=0):m_temp(temp),m_source(source){};
	~FWNotification(){};
	int getTemp(){return m_temp;}

	int getSource() {return m_source;};
private:
	int m_temp;
	int m_source;
};

class CInitFWThread :
	public ofThread
{
public:
	CInitFWThread(void);
	~CInitFWThread(void);
	NotificationQueue m_fwQueue;
	void threadedFunction();
	void loadData(std::string &filename);
private:
	Poco::Mutex _mutex;
	std::vector<fwcfg> m_cfgs;
};

