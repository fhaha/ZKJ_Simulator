#pragma once
#include <ofThread.h>
#include <ofMain.h>
#include "comhead.h"
#include "zdpkg.h"
#include "sqlite3.h"
#include "comhead.h"
#include "invpkg.h"
#include <Poco/Notification.h>
#include <Poco/NotificationQueue.h>
#include <Poco/AutoPtr.h>
#include "../../../../../stm32f405_inv/bsp/stm32f40x/applications/fwconfig.h"

#include "inv/mavlink.h"
#include "common/mavlink.h"
#include "protocol.h"
using Poco::Notification;
using Poco::NotificationQueue;
using Poco::AutoPtr;

int MAVLINK_SEND_UART_BYTES(mavlink_channel_t chan, const char *buf, uint16_t len);
struct Time_Stamps
{
	Time_Stamps()
	{
		reset_timestamps();
	}

	uint64_t heartbeat;
	uint64_t sys_status;
	uint64_t battery_status;
	uint64_t radio_status;
	uint64_t local_position_ned;
	uint64_t global_position_int;
	uint64_t position_target_local_ned;
	uint64_t position_target_global_int;
	uint64_t highres_imu;
	uint64_t attitude;

	void
	reset_timestamps()
	{
		heartbeat = 0;
		sys_status = 0;
		battery_status = 0;
		radio_status = 0;
		local_position_ned = 0;
		global_position_int = 0;
		position_target_local_ned = 0;
		position_target_global_int = 0;
		highres_imu = 0;
		attitude = 0;
	}

};


// Struct containing information on the MAV we are currently connected to

struct Mavlink_Messages {

	int sysid;
	int compid;

	// Heartbeat
	mavlink_heartbeat_t heartbeat;

	// System Status
	mavlink_sys_status_t sys_status;

	// Battery Status
	mavlink_battery_status_t battery_status;

	// Radio Status
	mavlink_radio_status_t radio_status;

	// Local Position
	mavlink_local_position_ned_t local_position_ned;

	// Global Position
	mavlink_global_position_int_t global_position_int;

	// Local Position Target
	mavlink_position_target_local_ned_t position_target_local_ned;

	// Global Position Target
	mavlink_position_target_global_int_t position_target_global_int;

	// HiRes IMU
	mavlink_highres_imu_t highres_imu;

	// Attitude
	mavlink_attitude_t attitude;

	// System Parameters?


	// Time Stamps
	Time_Stamps time_stamps;

	void
	reset_timestamps()
	{
		time_stamps.reset_timestamps();
	}

};


class CZKJSerialThread :
	public ofThread
{
public:
	CZKJSerialThread(void);
	~CZKJSerialThread(void);
	st_zd_pkg zd_pkg;

public:
	ofSerial m_serial;
	bool setup(std::string device, int baudrate);
	void threadedFunction();
	void quit();
	string m_devname;
	int m_baudrate;

	void ZD(st_zd_pkg &pkg);
	void GC(gc_pkg & pkg);			//Gravity Check
	void PC(pos_pkg & pkg);			//Posture Check
	template<typename Goo>
	void Send(Goo &pkg)
	{
		pkg.crc = calcCRC((pcomhead_st)&pkg);
		ofLogVerbose("ZKJ") << "Send IDCommand:" <<std::hex <<pkg.id <<  endl;//pkg << endl; 
		if(pkg.id == SCID){
			pscpkg pPkg = (pscpkg)&pkg;
			ofLogVerbose("ZKJ")<<"special command: " << std::hex << (int)pPkg->cmd << std::endl;
		}
		m_serial.writeBytes((unsigned char *)&pkg,sizeof(Goo));
	}
	
	unsigned char calcCRC(pcomhead_st pPkg);
	bool m_bPrintData;
	void Idle(std::string filename="");
	bool hasFileOpen(){
		return datafile.is_open()||capfile.is_open();
	}
	CZKJSerialThread & operator<<(fwcfg & cfg);
	string oldfilename;
	void startAccelCali();
	void oriAccelMode();
	void IdleMode();
private:
	bool m_bIsQuiting;
	std::ofstream datafile; 
	std::ofstream capfile;
	sqlite3 * pDB;
	void openStore();
	void insertIntoSqlite3(float etime,pcarpos_pkg pCarPkg);
	bool parseNBXData(unsigned char * pdata,int len);
	int read_message(mavlink_message_t &message);
	mavlink_status_t lastStatus;
	Mavlink_Messages current_messages;
};

