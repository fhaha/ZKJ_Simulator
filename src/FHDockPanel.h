#pragma once
#include <ofMain.h>
#include <ofxBaseGui.h>
#include <ofImage.h>
#include "ofxTrueTypeFontUC.h"
#include "FHButton.h"

enum dockSide{
	DSLEFT,
	DSRIGHT,
};

enum dockState {
	DSHIDE,
	DSCHANGING,
	DSSHOW,
};

#define DOCKMARGIN	20
#define ICONSIZE	22
#define UNHIDEWIDTH ICONSIZE/2
#define STEPRAD	(PI / 100)
#define THSLEEPTIME	10
#define BPPADDING 5
#define BUTTONGAP	4

#define LABELFONTSIZE 14

class CFHDockPanel :public ofThread
{
public:
	CFHDockPanel(void);
	~CFHDockPanel(void);
	template<class nametype>
	void setName(nametype &name);
	void setName(const wchar_t * name);
	void	draw(ofEventArgs& event);
	virtual void setup(ofEventArgs & event);
	virtual void exit(ofEventArgs &event);
	virtual void windowResized(ofResizeEventArgs & resize);
	void setPosition(float x,float y);
	void setPosition(ofPoint &p);
	ofPoint getPosition();
	float getWidth();
	float getHeight();
	
	void setFont(ofxTrueTypeFontUC * pFont);
	//virtual bool keyPressed(ofMouseEventArgs & args);
	//virtual bool keyReleased(ofMouseEventArgs & args);

	virtual bool mousePressed(ofMouseEventArgs & args);
	//virtual bool mouseDragged(ofMouseEventArgs & args);
	virtual bool mouseReleased(ofMouseEventArgs & args);
	virtual bool mouseMoved(ofMouseEventArgs & args);
	virtual void generateDraw();
	virtual void render();
	virtual void threadedFunction();
	ofRectangle getBodyRect();
public:
	
	float m_headerHeight;
	static int rowPadding;
	static int defaultWidth;
	static int defaultHeaderHeight;
	static ofImage lockIcon,unlockIcon;
protected:
	dockSide m_eDockSide;
	bool isGuiDrawing();
	void registerMouseEvents();
    void unregisterMouseEvents();
	
	ofPath border, headerBg;
	dockState m_eDockState;
	unsigned long currentFrame;
	void showDock();
	void hideDock();
	ofxTrueTypeFontUC * pzhFont;
	bool m_isLocked;
private:
	
	int m_iOldWindowWidth;
	int m_iOriBX;
	void loadIcons();
	Poco::FastMutex m_mutex;
	static ofColor TextColor,BorderColor,HeaderColor,BackgroundColor;
	ofRectangle b,iconBox;
	vector<ofPath> labelMesh;
	wstring m_wsLabel;
};

