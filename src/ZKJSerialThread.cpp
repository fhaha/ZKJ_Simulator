
#include "ofApp.h"
#include "ZKJSerialThread.h"
#include <time.h>
int gettimeofday(struct timeval *tp, void *tzp);
// ----------------------------------------------------------------------------------
//   Time
// ------------------- ---------------------------------------------------------------
uint64_t
get_time_usec()
{
	static struct timeval _time_stamp;
	gettimeofday(&_time_stamp, NULL);
	return _time_stamp.tv_sec*1000000 + _time_stamp.tv_usec;
}

CZKJSerialThread::CZKJSerialThread(void)
{
	m_bIsQuiting = false;
	m_bPrintData = false;
}


CZKJSerialThread::~CZKJSerialThread(void)
{
	m_bIsQuiting = true;
}

void CZKJSerialThread::quit()
{
	m_bIsQuiting = true;
	if(datafile.is_open()){
		datafile.close();
		sqlite3_close(pDB);
	}
	if(capfile.is_open()){
		capfile.close();
	}
}

bool CZKJSerialThread::setup(std::string device, int baudrate)
{
	m_serial.listDevices();
	vector <ofSerialDeviceInfo> deviceList = m_serial.getDeviceList();
	vector<ofSerialDeviceInfo>::iterator iter = deviceList.begin();
	string devname;
	while(iter != deviceList.end()){
		if(device == (*iter).getDeviceName()){
			devname = device;
		}
		++iter;
	}
	if(devname.empty()){
		ofLogVerbose("ZKJ") << "ZKJ Serial Device Config Failed" << endl;
		return false;
	}
	m_serial.setup(devname,baudrate);
	if(m_serial.isInitialized()){
		startThread();
		m_devname = devname;
		m_baudrate = baudrate;
	}
	else{
		ofLogVerbose("ZKJ") << "Open dev " << devname << "failed" << endl;
	}
	pDB= NULL;
	Idle();
	return true;
}

unsigned char CZKJSerialThread::calcCRC(pcomhead_st pPkg)
{
	int i = 0 ;
	int len = pPkg->len;
	unsigned char ret = 0;
	unsigned char * pData = (unsigned char*)pPkg;
	for(i=0;i<len-1;i++){
		ret ^= pData[i];
	}
	return ret;
}


#define BUFLEN	2048
void CZKJSerialThread::threadedFunction()
{
	//del in 20160815
#if 0
	unsigned char buf[BUFLEN]={0};
	int recvOff = 0;

	while(!m_bIsQuiting){
		if(m_serial.available()|| recvOff >= sizeof(comhead_st)){
			int couldlen = m_serial.available();
			if(couldlen> BUFLEN-recvOff)
				couldlen = BUFLEN-recvOff;
			recvOff += m_serial.readBytes(buf+recvOff,couldlen);
			
			if(m_bPrintData){
				printf("now buf len %d\n",recvOff);
				printf("data:");
				int i = 0 ;
				for(i=0;i<recvOff;i++){
					printf("%02x ",buf[i]);
				}
				printf("\n");
			}
			if(recvOff>=sizeof(comhead_st)){
				pcomhead_st pPkgHead = (pcomhead_st)buf;
				if(pPkgHead->phead != COMHEAD)
				{
					int i = 0 ;
					bool findHead = false;

					for(i = 0 ;i<recvOff;i++){
						if(buf[i]==COMH1){
							if(i<recvOff-1){
								if(buf[i+1] == COMH2){
									findHead = true;
									char tmpbuf[BUFLEN]={0};
									memcpy(tmpbuf,buf+i,recvOff-i);
									memcpy(buf,tmpbuf,recvOff-i);
									recvOff -= i;
									break;
								}
							}
							else{
								recvOff = 1;
								buf[0]=0xEB;
							}
						}
					}
					if(!findHead){
						if(m_bPrintData){
							printf("ZKJ drop flag %x len %d \n",pPkgHead->phead,recvOff);
							for(i = 0 ;i<recvOff;i++){
								printf("%02x ",buf[i]);
							}
						}
						recvOff = 0;
						m_serial.flush();
						m_serial.close();
						m_serial.setup(m_devname,m_baudrate);
					}
					continue;
				}

				if(recvOff>= pPkgHead->len){
					if(m_bPrintData){
						printf("parse data ");
						for(int i =0;i<recvOff;i++){
							printf("%02x ",buf[i]);
						}
						printf("\n");
					}
					if(parseNBXData(buf,recvOff)){
						int leftlen = recvOff - (pPkgHead->len);
						//printf("leftlen %d\n",leftlen);
						if(leftlen>0){
							char tmpbuf[BUFLEN]={0};
							memcpy(tmpbuf,buf+pPkgHead->len,leftlen);
							memcpy(buf,tmpbuf,leftlen);
							//printf("leftdata :");

							//for(int i =0;i<leftlen;i++){
							//	printf("%02x ",buf[i]);
							//}
							//printf("\n");
						}
						recvOff = leftlen;
					}
					else{
						printf("!!!!!!parse failed,clear buf\n");
						recvOff = 0;
					}

				}
				else{
#ifdef _DEBUG
					//printf("preParse off %d need %d,id %x\n",recvOff,pPkgHead->len,pPkgHead->id);
#endif
					//sleep(1);
					yield();
				}
			}
			if(recvOff > BUFLEN * 0.95){
				printf("warning buf overflow!!!!!!!!!!!!\n");
				recvOff = 0;
			}
		}
		else{
			if(m_bPrintData){
				static int lastlen=0;
				if(lastlen != recvOff){
					printf("idle buf status len %d data:",recvOff);
					int i = 0 ;
					for(i=0;i<recvOff;i++){
						printf("%02x ",buf[i]);
					}
					printf("\n");
					lastlen = recvOff;
				}
			}
			sleep(1);
		}
	}
	//m_serial.close();
#else
	// add 20160815 for mavlink
	mavlink_message_t message;
	bool success;               // receive success flag
	Time_Stamps this_timestamps;
	while(!m_bIsQuiting){
		success = read_message(message);
		if( success )
		{

			// Store message sysid and compid.
			// Note this doesn't handle multiple message sources.
			current_messages.sysid  = message.sysid;
			current_messages.compid = message.compid;

			// Handle Message ID
			switch (message.msgid)
			{

				case MAVLINK_MSG_ID_HEARTBEAT:
				{
					printf("MAVLINK_MSG_ID_HEARTBEAT\n");
					mavlink_msg_heartbeat_decode(&message, &(current_messages.heartbeat));
					current_messages.time_stamps.heartbeat = get_time_usec();
					this_timestamps.heartbeat = current_messages.time_stamps.heartbeat;
					break;
				}
				case MAVLINK_MSG_ID_COMMAND_LONG:
				{

					break;
				}
				case MAVLINK_MSG_ID_SYS_STATUS:
				{
					//printf("MAVLINK_MSG_ID_SYS_STATUS\n");
					mavlink_msg_sys_status_decode(&message, &(current_messages.sys_status));
					current_messages.time_stamps.sys_status = get_time_usec();
					this_timestamps.sys_status = current_messages.time_stamps.sys_status;
					break;
				}

				case MAVLINK_MSG_ID_BATTERY_STATUS:
				{
					//printf("MAVLINK_MSG_ID_BATTERY_STATUS\n");
					mavlink_msg_battery_status_decode(&message, &(current_messages.battery_status));
					current_messages.time_stamps.battery_status = get_time_usec();
					this_timestamps.battery_status = current_messages.time_stamps.battery_status;
					break;
				}

				case MAVLINK_MSG_ID_RADIO_STATUS:
				{
					//printf("MAVLINK_MSG_ID_RADIO_STATUS\n");
					mavlink_msg_radio_status_decode(&message, &(current_messages.radio_status));
					current_messages.time_stamps.radio_status = get_time_usec();
					this_timestamps.radio_status = current_messages.time_stamps.radio_status;
					break;
				}

				case MAVLINK_MSG_ID_LOCAL_POSITION_NED:
				{
					//printf("MAVLINK_MSG_ID_LOCAL_POSITION_NED\n");
					mavlink_msg_local_position_ned_decode(&message, &(current_messages.local_position_ned));
					current_messages.time_stamps.local_position_ned = get_time_usec();
					this_timestamps.local_position_ned = current_messages.time_stamps.local_position_ned;
					break;
				}

				case MAVLINK_MSG_ID_GLOBAL_POSITION_INT:
				{
					//printf("MAVLINK_MSG_ID_GLOBAL_POSITION_INT\n");
					mavlink_msg_global_position_int_decode(&message, &(current_messages.global_position_int));
					current_messages.time_stamps.global_position_int = get_time_usec();
					this_timestamps.global_position_int = current_messages.time_stamps.global_position_int;
					break;
				}

				case MAVLINK_MSG_ID_POSITION_TARGET_LOCAL_NED:
				{
					//printf("MAVLINK_MSG_ID_POSITION_TARGET_LOCAL_NED\n");
					mavlink_msg_position_target_local_ned_decode(&message, &(current_messages.position_target_local_ned));
					current_messages.time_stamps.position_target_local_ned = get_time_usec();
					this_timestamps.position_target_local_ned = current_messages.time_stamps.position_target_local_ned;
					break;
				}

				case MAVLINK_MSG_ID_POSITION_TARGET_GLOBAL_INT:
				{
					//printf("MAVLINK_MSG_ID_POSITION_TARGET_GLOBAL_INT\n");
					mavlink_msg_position_target_global_int_decode(&message, &(current_messages.position_target_global_int));
					current_messages.time_stamps.position_target_global_int = get_time_usec();
					this_timestamps.position_target_global_int = current_messages.time_stamps.position_target_global_int;
					break;
				}

				case MAVLINK_MSG_ID_HIGHRES_IMU:
				{
					//printf("MAVLINK_MSG_ID_HIGHRES_IMU\n");
					mavlink_msg_highres_imu_decode(&message, &(current_messages.highres_imu));
					current_messages.time_stamps.highres_imu = get_time_usec();
					this_timestamps.highres_imu = current_messages.time_stamps.highres_imu;
					break;
				}

				case MAVLINK_MSG_ID_ATTITUDE:
				{
					//printf("MAVLINK_MSG_ID_ATTITUDE\n");
					mavlink_msg_attitude_decode(&message, &(current_messages.attitude));
					current_messages.time_stamps.attitude = get_time_usec();
					this_timestamps.attitude = current_messages.time_stamps.attitude;
					break;
				}

				default:
				{
					// printf("Warning, did not handle message id %i\n",message.msgid);
					break;
				}


			} // end: switch msgid

		} // end: if read message
		else
		{
			Sleep(1);
		}
	};

#endif
}

int CZKJSerialThread::read_message(mavlink_message_t &message)
{
	uint8_t          cp;
	mavlink_status_t status;
	bool debug = true;
	uint8_t          msgReceived = false;

	int couldlen = m_serial.available();
	while(!msgReceived && couldlen >0 && !m_bIsQuiting){
		// the parsing
		cp = m_serial.readByte();
		msgReceived = mavlink_parse_char(MAVLINK_COMM_1, cp, &message, &status);

		// check for dropped packets
		if ( (lastStatus.packet_rx_drop_count != status.packet_rx_drop_count) && debug )
		{
			printf("ERROR: DROPPED %d PACKETS\n", status.packet_rx_drop_count);
			unsigned char v=cp;
			fprintf(stderr,"%02x ", v);
		}
		lastStatus = status;
		couldlen = m_serial.available();
	}

	// --------------------------------------------------------------------------
	//   DEBUGGING REPORTS
	// --------------------------------------------------------------------------
	if(msgReceived && debug)
	{
		// Report info
		printf("Received message from serial with ID #%d (sys:%d|comp:%d):\n", message.msgid, message.sysid, message.compid);

		fprintf(stderr,"Received serial data: ");
		unsigned int i;
		uint8_t buffer[MAVLINK_MAX_PACKET_LEN];

		// check message is write length
		unsigned int messageLength = mavlink_msg_to_send_buffer(buffer, &message);

		// message length error
		if (messageLength > MAVLINK_MAX_PACKET_LEN)
		{
			fprintf(stderr, "\nFATAL ERROR: MESSAGE LENGTH IS LARGER THAN BUFFER SIZE\n");
		}

		// print out the buffer
		else
		{
			for (i=0; i<messageLength; i++)
			{
				unsigned char v=buffer[i];
				fprintf(stderr,"%02x ", v);
			}
			fprintf(stderr,"\n");
		}
		switch(message.msgid){
		case MAVLINK_MSG_ID_STATUSTEXT:
			{
				ofApp * pApp = dynamic_cast<ofApp *>(ofGetAppPtr());
				if(pApp){
					mavlink_statustext_t statustext={0};
					mavlink_msg_statustext_decode(&message,&statustext);
					pApp->getLW() << statustext.text << CFHLogWindow::nl;
				}
			}
			break;
		case MAVLINK_MSG_ID_COMMAND_LONG:
			{
				ofApp * pApp = dynamic_cast<ofApp *>(ofGetAppPtr());
				if(pApp){
					mavlink_command_long_t command_long;
					mavlink_msg_command_long_decode(&message,&command_long);
					switch(command_long.command){
					case MAV_CMD_USER_2:	// Ori Accel
						pApp->m_zkjQueue.enqueueNotification(new OriAccelNotification(command_long.param1,command_long.param2,command_long.param3));
						break;
					}
				}
				break;
			}
		}
	}

	// Done!
	return msgReceived;
}

void CZKJSerialThread::startAccelCali()
{
	mavlink_msg_command_long_send(MAVLINK_COMM_0,1,2,MAV_CMD_PREFLIGHT_CALIBRATION,0,0,0,0,0,1,0,0);
}

void CZKJSerialThread::IdleMode()
{
	mavlink_msg_command_long_send(MAVLINK_COMM_0,1,2,MAV_CMD_USER_1,0,2,0,0,0,0,0,0);
}

void CZKJSerialThread::oriAccelMode()
{
	mavlink_msg_command_long_send(MAVLINK_COMM_0,1,2,MAV_CMD_USER_1,0,1,0,0,0,0,0,0);
}

void CZKJSerialThread::GC(gc_pkg & pkg)
{
	pkg.crc = calcCRC((pcomhead_st)&pkg);
	ofLogVerbose("ZKJ") << "Send Gravity Check" << endl;
	m_serial.writeBytes((unsigned char *)&pkg,sizeof(pkg));
}

void CZKJSerialThread::Idle(std::string filename)
{
	char tmpbuf[64];
	time_t nowtime;
	struct tm * pnowtm;
	nowtime = time(NULL);
	pnowtm = localtime(&nowtime);
	strcpy(tmpbuf,asctime(pnowtm));
	char * ppos = strchr(tmpbuf,'\n');
	while(ppos){
		*ppos='_';
		ppos = strchr(tmpbuf,' ');
	}
	ppos = strchr(tmpbuf,':');
	while(ppos){
		*ppos = '_';
		ppos = strchr(tmpbuf,':');
	}
	std::string dbfilename=tmpbuf;
	if(!filename.empty())
		dbfilename = filename.substr(0,filename.length()-4);
	dbfilename+=".db";
	strcat(tmpbuf,".txt");
	ofApp* pApp = (ofApp*) ofGetAppPtr();
	sleep(1000);
	pApp->update();
	pApp->m_zkjQueue.clear();
	if(datafile.is_open()){
		datafile.close();
		if(!oldfilename.empty() && !filename.empty() && oldfilename != filename)
			rename(oldfilename.c_str(),filename.c_str());
	}
	if(capfile.is_open()){
		capfile.close();
		std::string capfilename = "Cap";
		capfilename += oldfilename;
		if( !filename.empty() ){
			filename = "Cap" + filename;
			rename(capfilename.c_str(),filename.c_str());
		}
	}
	if(pDB){
		sqlite3_close(pDB);
		rename("data/fhaha.db",dbfilename.c_str());
		pDB = NULL;
	}
	oldfilename = tmpbuf;
}

//
//template<typename Goo>
//void CZKJSerialThread::Send(Goo &pkg)
//{
//	pkg.crc = calcCRC((pcomhead_st)&pkg);
//	ofLogVerbose("ZKJ") << "Send ZhuangDing Data" << endl;//pkg << endl;
//	m_serial.writeBytes((unsigned char *)&pkg,sizeof(pkg));
//}

void CZKJSerialThread::ZD(st_zd_pkg & pkg)
{
	pkg.crc = calcCRC((pcomhead_st)&pkg);
	ofLogVerbose("ZKJ") << "Send ZhuangDing Data" << endl;//pkg << endl;
	m_serial.writeBytes((unsigned char *)&pkg,sizeof(pkg));
}

CZKJSerialThread & CZKJSerialThread::operator<<(fwcfg & cfg)
{
	fwpkg tPkg={{COMHEAD,DELTAPKGLEN,DELTAID},cfg,0};
	tPkg.crc = calcCRC((pcomhead_st)&tPkg);
	ofLogVerbose("ZKJ") << "Send FW Data in temp" <<cfg.temperature <<  endl;//pkg << endl;
	m_serial.writeBytes((unsigned char *)&tPkg,sizeof(tPkg));
	return *this;
}

#define CHECK_PCARPKG(item)	if(pCarPkg->item != pCarPkg->item)pCarPkg->item=0.0f
void CZKJSerialThread::insertIntoSqlite3(float etime,pcarpos_pkg pCarPkg)
{
	if(pDB==NULL){
		std::cerr << "database not inited" << endl;
		return ;
	}
	int res;
	char * errMsg;
	res = sqlite3_exec(pDB,"begin transaction;",0,0,&errMsg);
	std::stringstream strsql;
	strsql << "insert into invtable(idtype,time,deltaT,Fx,Fy,Fz,Wx,Wy,Wz,Wox,Woy,Woz,Wopy,I2CTemp,psi,theta,gamma,lambda,fai,H,Gpsi,Gtheta,Ggamma,GAx,GAy,GAz,GVx,GVy,GVz,Ax,Ay,Az,Vx,Vy,Vz,Sx,Sy,Sz,Glambda,Gfai,Gh,V1x,V1y,V1z,A1x,A1y,A1z,S1x,S1y,S1z) values(";
	CHECK_PCARPKG(psi);
	CHECK_PCARPKG(gamma);
	CHECK_PCARPKG(theta);
	CHECK_PCARPKG(H);
	CHECK_PCARPKG(lambda);
	CHECK_PCARPKG(fai);
	for(int i = 0 ;i<3;i++){
		CHECK_PCARPKG(segg[i]);
		CHECK_PCARPKG(Vegg[i]);
		CHECK_PCARPKG(Aegg[i]);
	}
	strsql << pCarPkg->id << "," << etime << "," << pCarPkg->delT << "," << pCarPkg->fibb[0] << "," << pCarPkg->fibb[1] << "," << pCarPkg->fibb[2] << ","
				<< pCarPkg->wibb[0] << "," << pCarPkg->wibb[1] << "," << pCarPkg->wibb[2] << "," 
				<< pCarPkg->woibb[0] << "," << pCarPkg->woibb[1] << "," << pCarPkg->woibb[2] << ","<<pCarPkg->woibb[3] << ","
				<< pCarPkg->i2ctemp << "," << pCarPkg->psi << "," << pCarPkg->theta << "," << pCarPkg->gamma << "," 
				<< pCarPkg->lambda << "," << pCarPkg->fai << "," << pCarPkg->H << "," << pCarPkg->Gpsi << ","
				<< pCarPkg->Gtheta << "," << pCarPkg->Ggamma << "," << pCarPkg->GAegg[0] << "," << pCarPkg->GAegg[1] << "," 
				<< pCarPkg->GAegg[2] << "," << pCarPkg->GVegg[0] << "," << pCarPkg->GVegg[1] << "," << pCarPkg->GVegg[2] << ","
				<< pCarPkg->Aegg[0] << "," << pCarPkg->Aegg[1] << "," << pCarPkg->Aegg[2] << "," 
				<< pCarPkg->Vegg[0] << "," << pCarPkg->Vegg[1] << "," << pCarPkg->Vegg[2] << ","
				<< pCarPkg->segg[0] << "," << pCarPkg->segg[1] << "," << pCarPkg->segg[2] << ","
				<< pCarPkg->Glambda << "," << pCarPkg->Gfai << "," << pCarPkg->GH  <<","
				<< pCarPkg->Vegg_1[0] << "," << pCarPkg->Vegg_1[1] << "," << pCarPkg->Vegg_1[2] << ","
				<< pCarPkg->Aegg_1[0] << "," << pCarPkg->Aegg_1[1] << "," << pCarPkg->Aegg_1[2] << ","
				<< pCarPkg->segg_1[0] << "," << pCarPkg->segg_1[1] << "," << pCarPkg->segg_1[2] 
				<< ");";
	res = sqlite3_exec(pDB,strsql.str().c_str(),0,0,&errMsg);
	if(res != SQLITE_OK){
		std::cout << "sql: " << strsql.str() << endl; 
		std::cerr << "insert failed : " << errMsg << endl;
		datafile << pCarPkg->id << '\t'<< etime << '\t';
		datafile << pCarPkg->delT << '\t' << pCarPkg->fibb[0] << '\t' << pCarPkg->fibb[1] << '\t' << pCarPkg->fibb[2] << '\t'
			<< pCarPkg->wibb[0] << '\t' << pCarPkg->wibb[1] << '\t' << pCarPkg->wibb[2] << '\t' 
			<< pCarPkg->woibb[0] << '\t' << pCarPkg->woibb[1] << '\t' << pCarPkg->woibb[2] << '\t'<<pCarPkg->woibb[3] << '\t'
			<< pCarPkg->i2ctemp << '\t' << pCarPkg->psi << '\t' << pCarPkg->theta << '\t' << pCarPkg->gamma << '\t' 
			<< pCarPkg->lambda << '\t' << pCarPkg->fai << '\t' << pCarPkg->H << '\t' << pCarPkg->Gpsi << '\t'
			<< pCarPkg->Gtheta << '\t' << pCarPkg->Ggamma << '\t' << pCarPkg->GAegg[0] << '\t' << pCarPkg->GAegg[1] << '\t' 
			<< pCarPkg->GAegg[2] << '\t' << pCarPkg->GVegg[0] << '\t' << pCarPkg->GVegg[1] << '\t' << pCarPkg->GVegg[2] << '\t'
			<< pCarPkg->Aegg[0] << '\t' << pCarPkg->Aegg[1] << '\t' << pCarPkg->Aegg[2] << '\t' 
			<< pCarPkg->Vegg[0] << '\t' << pCarPkg->Vegg[1] << '\t' << pCarPkg->Vegg[2] << '\t'
			<< pCarPkg->segg[0] << '\t' << pCarPkg->segg[1] << '\t' << pCarPkg->segg[2] << '\t'
			<< pCarPkg->Glambda << '\t' << pCarPkg->Gfai << '\t' << pCarPkg->GH << std::endl;
	}
	sqlite3_exec(pDB,"commit transaction;",0,0,&errMsg);
}

void CZKJSerialThread::openStore()
{
	datafile.open(oldfilename.c_str());
	if(!datafile.is_open())
		printf("open car file %s failed\n",oldfilename.c_str());
	else
		datafile << "Type\tTime\tdeltaT\tFx\tFy\tFz\tWx\tWy\tWz\tWox\tWoy\tWoz\tWopy\tI2CTemp\tpsi\ttheta\tgamma\tlambda\tfai\tH\tGpsi\tGtheta\tGgamma\tGAx\tGAy\tGAz\tGVx\tGVy\tGVz\tAx\tAy\tAz\tVx\tVy\tVz\tSx\tSy\tSz\tGlambda\tGfai\tGh" << std::endl;
	if(pDB){
		sqlite3_close(pDB);
		pDB = NULL;
	}
	unlink("data/fhaha.db");
	int res=sqlite3_open("data/fhaha.db",&pDB);
	if(res){
		std::cout << "open database fhaha.db failed: " << sqlite3_errmsg(pDB);
		pDB= NULL;
	}
	else{
		char * errMsg;
		res = sqlite3_exec(pDB,"begin transaction;",0,0,&errMsg);
		res = sqlite3_exec(pDB,"CREATE TABLE invtable(idtype integer,time real, deltaT real, Fx real, Fy real, Fz real, Wx real,Wy real ,Wz real,Wox real,Woy real, Woz real,Wopy real,I2CTemp real,psi real , theta real,gamma real,lambda real,fai  real,H real,Gpsi real,Gtheta real,Ggamma real,GAx real,GAy real,GAz real,GVx real, GVy real ,GVz real ,Ax real ,Ay real, Az real, Vx real ,Vy real ,Vz real,Sx real , Sy real ,Sz real,Glambda real ,Gfai real , Gh real,V1x real ,V1y real ,V1z real ,A1x real ,A1y real ,A1z real ,S1x real ,S1y real , S1z real,id integer primary key autoincrement);",0,0,&errMsg);
		res = sqlite3_exec(pDB,"CREATE INDEX invidx on invtable(time);",0,0,&errMsg);
		res = sqlite3_exec(pDB,"commit transaction;",0,0,&errMsg);
		std::cout << "create table invtable ok" << std::endl;
	}
}

bool CZKJSerialThread::parseNBXData(unsigned char * pdata,int len)
{
	pcomhead_st pHead = (pcomhead_st)pdata;
	ofApp* pApp = (ofApp*) ofGetAppPtr();
	switch(pHead->id){
	case ERRPKGID:
		{
			pzkjerr_st pErrPkg = (pzkjerr_st)pdata;
			unsigned calcc= calcCRC((pcomhead_st)pdata);
			if(pErrPkg->crc != calcc){
				//printf("drop err pkg once for bad crc need %x get %x errnum %x %f %f %f\n",calcc,pErrPkg->crc,pErrPkg->errnum,
				//	pErrPkg->extra1,pErrPkg->extra2,pErrPkg->extra3);
				break;
			}
			pApp->m_zkjQueue.enqueueNotification(new ZKJERRORNotification(pErrPkg));

		}
		break;
	case ACRESID:
		{
			pacrespkg pACResPkg = (pacrespkg)pdata;
			unsigned calcc = calcCRC((pcomhead_st)pdata);
			if(pACResPkg->crc != calcc){
				printf("drop acres once\n");
				break;
			}
			pApp->m_zkjQueue.enqueueNotification(new CustomNotification<acrespkg>(pACResPkg));
			break;
		}
	case CARPOSID:
	case CARGPSPOSID:
		{
			int i = 0;
			pcarpos_pkg pCarPkg = (pcarpos_pkg)pdata;
			unsigned calcc = calcCRC((pcomhead_st)pdata);
			if(pCarPkg->crc != calcc){
				printf("drop car once get crc %x %x %x need crc %x ,need len %d get %d\n",*((&pCarPkg->crc)-1),pCarPkg->crc,*((&pCarPkg->crc)+1),calcc,sizeof(carpos_pkg),pCarPkg->len);
				break;
			}
			if(!datafile.is_open()){
				openStore();
			}
			float etime = ofGetElapsedTimef();
			 

			pApp->m_zkjQueue.enqueueNotification(new CarPosNotification(pCarPkg));
			insertIntoSqlite3(etime,pCarPkg);
		}
		break;
	case CAPPKGID:
		{
			pcap_pkg pCapPkg = (pcap_pkg)pdata;
			unsigned calcc= calcCRC((pcomhead_st)pdata);
			if(pCapPkg->crc != calcc){
				printf("drop cap once\n");
				break;
			}
			int tmpvalue = 0;
			int i = 0 ,j=0;
			if(!capfile.is_open()){
				std::string capfilename = "Cap";
				capfilename += oldfilename;
				capfile.open(capfilename.c_str());
				if(!capfile.is_open())
					printf("open file %s failed\n",capfilename.c_str());
				else
					capfile << "Time\tDT\tFx\tFy\tFz\tWx\tWy\tWz\tWpy\tGVx\tGVy\tGVz\tGLo\tGLa\tGH\tI2CTemp" << std::endl;

			}
			capfile << ofGetElapsedTimef() << '\t' << 0.007801904f << '\t';
			for(i=0;i<7;i++){
				/*tmpvalue = 0;
				for(j=0;j<3;j++){
					tmpvalue |= pCapPkg->data[i*3+j];
					tmpvalue <<= 8;
				}
				tmpvalue <<= 8;
				tmpvalue >>= 8;
				//printf("%d ",tmpvalue);
				datafile<<tmpvalue << '\t';*/
				capfile << pCapPkg->data[i] << '\t' ;
			}
			capfile << (float) pApp->m_getLVe/100.0f << '\t';
			capfile << (float) pApp->m_getLVn/100.0f << '\t';
			capfile << (float) pApp->m_getLVu/100.0f << '\t';
			capfile << (float) pApp->m_fGLambda << '\t';
			capfile << (float) pApp->m_fGFai << '\t';
			capfile << (float) pApp->m_fGH << '\t';
			capfile << pCapPkg->i2ctemp << std::endl;
			pApp->m_zkjQueue.enqueueNotification(new ZKJCAPNotification(pCapPkg));
			//printf("\n");
			break;
		}

	case INITPOSRESID:
	case POSRESID:
	case GCRESID:{
			pgcres_pkg pResPkg = (pgcres_pkg) pdata;
			unsigned calcc = calcCRC((pcomhead_st)pdata);
			if(pResPkg->crc == calcc ){
				pApp->m_zkjQueue.enqueueNotification(new GCResNotification(pResPkg));
			}
			else{
				if(m_bPrintData){
					std::stringstream ostr;
					ostr << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!drop len " << (int)pHead->len <<"(" <<std::hex<<(int)pHead->len << ") id "<< std::hex << std::setfill('0') << std::setw(2)<< pHead->id  <<" need crc " << (unsigned int)calcc << " get " << (unsigned int)pResPkg->crc << "data:";
					for(int i = 0 ;i<pHead->len;++i){
						ostr << std::hex << std::setw(2) << (unsigned int) pdata[i] << ' ' ;
					}
					ostr<< "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
					ofLogVerbose("ZKJ") << ostr.str();
				}
			}
			break;
		}
	case INVPKG_ID:{
			pinv_pkg pPkg = (pinv_pkg) pdata;
			pApp->m_zkjQueue.enqueueNotification(new ZKJNotification(pPkg));
			break;
		}
	case DELTAACKID:{
			pApp->m_zkjQueue.enqueueNotification(new FWNotification(0,1));
			break;
		}
	default:
		ofLogVerbose("ZKJ") << "unknown ZKJ serial Package " << std::hex << pHead->id << endl;
		return false;
	}

	return true;
}

void CZKJSerialThread::PC(pos_pkg & pkg)
{
	pkg.crc = calcCRC((pcomhead_st)&pkg);
	ofLogVerbose("ZKJ") << "Send PostureCheck Init Data" << endl;//pkg << endl;
	m_serial.writeBytes((unsigned char *)&pkg,sizeof(pkg));
}



