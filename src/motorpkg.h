//
//  motorpkg.h
//  ZKJ_Simulator
//
//  Created by ibmer on 15/4/29.
//
//

#ifndef ZKJ_Simulator_motorpkg_h
#define ZKJ_Simulator_motorpkg_h

#define MPHEAD  0xA5

#define XADD    0xa1
#define XSUB    0xa2
#define XNADD		0xa3
#define XNSUB		0xa4
#define YADD    0xb1
#define YSUB    0xb2
#define YNADD		0xb3
#define YNSUB		0xb4
#define ZFAR    0xc1
#define ZNEAR   0xc2
#define HDONE		0xEE
#pragma pack(push)
#pragma pack(1)
typedef struct _st_motorpkg{
    unsigned char head; //0xa5 MPHEAD
    unsigned char action;
    unsigned short  actionvalue;    
}motorpkg,*pmotorpkg;
#pragma pack(pop)
#endif
