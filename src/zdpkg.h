#pragma once

#define GCRESPKGLEN	sizeof(gcres_pkg)
#define GCPKGLEN	sizeof(gc_pkg)
#define POSPKGLEN	sizeof(pos_pkg)
#define ZDPKGLEN	sizeof(st_zd_pkg)
#define CAPPKGLEN sizeof(cap_pkg)
#define SDATACMDLEN	sizeof(st_senddata_pkg)

#define DIMIANCESHI 0xAABB
#define FEIXINGZHUANGTAI	0xBBAA

#pragma pack(push)
#pragma pack(1)
typedef struct _st_senddata_pkg{
	unsigned short phead;	//0x90EB
	unsigned char len;		//10
	unsigned short id;	//0x5310
	int cmd;
	unsigned char crc;
}st_senddata_pkg,*pst_senddata_pkg;

typedef struct _st_zd_pkg {
	unsigned short phead;	//0x90EB
	unsigned char len;		//26
	unsigned short id;		//0x530C
	int fsdlongitude;				//fashedian 1e-6 degree -180~180
	int fsdlatitude;				//fashedian 1e-6 degree -180~180
	int fsdhegiht;					//fashedian 0.1m	0~5000
	unsigned short psi;			//0.01degree 0~360
	unsigned short theta;		//0.01degree 0~80
	unsigned short qktime;		//qikong 0.001second 30s~60s
	unsigned short workmode;
	unsigned char crc;
}st_zd_pkg,*pst_zd_pkg;


typedef struct _st_gc_pkg {
	unsigned short phead;	//0x90EB
	unsigned char len;		//22
	unsigned short id;		//0x530D
	float	dfx;	//1e+4 V
	float dfy;	//1e+4 V
	float dfz;	//1e+4 V
	float ffactorx;	//1e+3
	float ffactory;
	float ffactorz;
	float dw[3];
	float wfactor[4];
	int flag;
	unsigned char crc;
}gc_pkg,*pgc_pkg;

#define SCPOSFIX (1u<<0)	//Position Fixed
#define SCHAE	(1u<<1)	//HalfAdjust Enable
#define SCHAD	(1u<<2)	//HalfAdjust Disable
#define DYNGE	(1u<<3)	//Dynamic G Enable
#define DYNGD	(1u<<4)	//Dynamic G Disable
#define GAPER5	(1u<<5)	//GPS Adjust Period 5s
#define GAPER10	(1u<<6)	//GPS Adjust Period 10s
#define GAPER20	(1u<<7)	//GPS Adjust Period 20s

typedef struct _st_sc_pkg {
	unsigned short phead;	//0x90EB
	unsigned char len;		//22
	unsigned short id;		//CSID 0x5305
	unsigned char cmd;
	unsigned char crc;
}scpkg,*pscpkg;

typedef struct _st_pos_pkg {
	unsigned short phead;	//0x90EB
	unsigned char len;		//46
	unsigned short id;		//0x530F for pos,//0x530A for initpos
	unsigned char flag;
	float	dfx;	
	float dfy;	
	float dfz;	
	float ffactorx;	
	float ffactory;
	float ffactorz;
	float dwx;	
	float dwy;	
	float dwz;	
	float wxfactor;	
	float wyfactor;
	float wypfactor;
	float wzfactor;
	unsigned char crc;
}pos_pkg,*ppos_pkg,initpos_pkg,*pinitpos_pkg;


typedef struct _st_carinit_pkg{
	unsigned short phead;	//0x90EB
	unsigned char len;		//46
	unsigned short id;		//CARPOSID
	float fa[3][3];		//f angles
	float wa[3][3];	//w angles
	float df[3];	//delta f
	float ff[3];	//fibb factor
	float dw[4];	//delta w
	float wf[4];	//wibb factor
	float psi;
	unsigned char flag;
	unsigned char crc;
}carinit_pkg,*pcarinit_pkg;

typedef struct _st_carpos_pkg{
	unsigned short phead;	//0x90EB
	unsigned char len;		//46
	unsigned short id;		//CARPOSID
	float Glambda;
	float Gfai;
	float GH;
	float GVegg[3];
	float GAegg[3];
	float Gpsi;
	float Gtheta;
	float Ggamma;
	float psi;
	float theta;
	float gamma;
	//float psiT;
	//float thetaT;
	//float gammaT;
	float H;
	float Aegg[3];
	float Vegg[3];
	//float wefactor[3];
	float lambda;
	float fai;
	float delT;
	//unsigned int tick;
	float gpsdelT;
	float wibb[3];
	float fibb[3];
	float woibb[4];
	float Aegg_1[3];
	float Vegg_1[3];
	float segg_1[3];
	float segg[3];
	float i2ctemp;
	unsigned char crc;
}carpos_pkg,*pcarpos_pkg;

typedef struct _st_cap_pkg {
	unsigned short phead;
	unsigned char len;
	unsigned short id;
	//unsigned char data[3*7];
	float data[7];
	float i2ctemp;
	unsigned char crc;
}cap_pkg,*pcap_pkg;

typedef struct _gcres_pkg{
	unsigned short phead;//0x90EB
	unsigned char len;	//
	unsigned short id; //0x530E
	float foibbx;
	float foibby;
	float foibbz;
	float fmibbx;
	float fmibby;
	float fmibbz;
	float fibbx;			
	float fibby;				
	float fibbz;				
	float figgx;				
	float figgy;				
	float figgz;	
	float wibbx;
	float wibby;
	float wibbz;
	float wpibbx;
	float wpibby;
	float wpibbz;
	float wpibbpy;
	float wmibbx;
	float wmibby;
	float wmibbz;
	float dfx;
	float dfy;
	float dfz;
	float gpsv[3];
	float gpsv1[3];
	float g;					
	float gn;					
	float theta;				
	float gamma;				
	float psi;		
	float temperature[4];
	float Cgb[3][3];
	float i2ctemp;
	float deltatime;
	unsigned char crc;
}gcres_pkg,*pgcres_pkg,posres_pkg,*pposres_pkg;
#pragma pack(pop)
