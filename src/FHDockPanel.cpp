#include "FHDockPanel.h"
#include "ofGraphics.h"



ofImage CFHDockPanel::lockIcon;
ofImage CFHDockPanel::unlockIcon;
ofColor CFHDockPanel::TextColor(255);
ofColor CFHDockPanel::BorderColor(0,0,0,20);
ofColor CFHDockPanel::BackgroundColor(90,90,90,50);
ofColor CFHDockPanel::HeaderColor(100,30,150,200);

int CFHDockPanel::defaultHeaderHeight = LABELFONTSIZE+2*BPPADDING;
int CFHDockPanel::defaultWidth = 150;
int CFHDockPanel::rowPadding=4;

CFHDockPanel::CFHDockPanel(void):m_eDockSide(DSLEFT),m_eDockState(DSHIDE),m_isLocked(false),pzhFont(NULL)
{
	b.set(0,0,defaultWidth,ofGetScreenHeight() - 2 * DOCKMARGIN);

	ofAddListener(ofEvents().setup,this,&CFHDockPanel::setup);
}


CFHDockPanel::~CFHDockPanel(void)
{
	unregisterMouseEvents();
}

bool CFHDockPanel::mouseReleased(ofMouseEventArgs & args){
    if(isGuiDrawing() && b.inside(ofPoint(args.x,args.y))){
    	return false;
    }else{
    	return false;
    }
}

void CFHDockPanel::draw(ofEventArgs& event)
{
	currentFrame = ofGetFrameNum();
	render();
}
bool CFHDockPanel::isGuiDrawing(){
	if( ofGetFrameNum() - currentFrame > 1 ){
		return false;
	}else{
		return true;
	}
}


void CFHDockPanel::setName(const wchar_t * pname)
{
    std::wstring name;
    name = pname;
	setName(name);

}

template<class nametype>
void CFHDockPanel::setName(nametype & name)
{
	m_wsLabel = name;
	return;
}

void CFHDockPanel::loadIcons()
{
	if(!lockIcon.isAllocated())
	{
		lockIcon.loadImage("lock.png");
		lockIcon.resize(16,16);
	}
	if(!unlockIcon.isAllocated()){
		unlockIcon.loadImage("unlock.png");
		lockIcon.resize(16,16);
	}
}

ofPoint CFHDockPanel::getPosition()
{
	return ofPoint(b.x,b.y);
}

void CFHDockPanel::threadedFunction()
{
	float  diffPos = 0.0f ;
	int width = getWidth() - UNHIDEWIDTH;
	vector<ofVec3f> oriVecs;

	dockState oldState = m_eDockState;
	while(isThreadRunning()){
		if(oldState != m_eDockState){
			oldState = DSCHANGING;
			if(m_eDockState == DSHIDE){
				diffPos -= STEPRAD;
			}
			else{
				diffPos += STEPRAD;
			}
			if((diffPos>=PI/2 && m_eDockState == DSSHOW) || (diffPos<=0 && m_eDockState == DSHIDE)){
				oldState = m_eDockState;
			}
			else{
				if(m_mutex.tryLock()){
					switch(m_eDockSide){
					case DSLEFT:
						b.x = m_iOriBX + sinf(diffPos) * width;
						break;
					case DSRIGHT:
						b.x = m_iOriBX - sinf(diffPos) * width;
						break;
					}
					generateDraw();

					m_mutex.unlock();
				}
			}
			sleep(THSLEEPTIME);
		}
		else{
			sleep(1000);
		}

		//yield();
	}
}

void CFHDockPanel::render()
{

	ofColor c = ofGetStyle().color;
	ofBlendMode blendMode = ofGetStyle().blendingMode;
	if(blendMode!=OF_BLENDMODE_ALPHA){
		ofEnableAlphaBlending();
	}
	m_mutex.lock();
	border.draw();
	headerBg.draw();
	ofSetColor(255,255,255,255);
	if(m_isLocked){
		lockIcon.draw(iconBox);
	}
	else{
		unlockIcon.draw(iconBox);
	}

	ofPushMatrix();

	ofTranslate(b.x+BPPADDING,b.y+ m_headerHeight /2 + BPPADDING);
	vector<ofPath>::iterator iter = labelMesh.begin();
	while(iter != labelMesh.end()){
		(*iter).draw();
		iter++;
	}

	ofPopMatrix();

	m_mutex.unlock();
	if(blendMode!=OF_BLENDMODE_ALPHA){
		ofEnableBlendMode(blendMode);
	}

	ofSetColor(c);
}


void CFHDockPanel::showDock()
{
	m_eDockState=DSSHOW;
}

void CFHDockPanel::hideDock()
{
	if(!m_isLocked)
		m_eDockState = DSHIDE;
}

bool CFHDockPanel::mousePressed(ofMouseEventArgs & args)
{
	if(isGuiDrawing()){
		if(iconBox.inside(args.x,args.y)){
			m_isLocked=!m_isLocked;
			if(m_isLocked){
				m_eDockState = DSSHOW;
			}
		}
	}
	return false;
}

bool CFHDockPanel::mouseMoved(ofMouseEventArgs & args){
    ofMouseEventArgs a = args;

    if(isGuiDrawing()){
		if(b.inside(ofPoint(args.x,args.y))){
			showDock();
		}
		else{
			hideDock();
		}

        return false;
    }else{
        return false;
    }
}

float CFHDockPanel::getWidth(){
	return b.width;
}
float CFHDockPanel::getHeight(){
	return b.height;
}
	
void CFHDockPanel::setFont(ofxTrueTypeFontUC * pFont){
	pzhFont = pFont;
}

void CFHDockPanel::setPosition(float x,float y)
{
	b.x = x;
	b.y = y;
	generateDraw();
}

void CFHDockPanel::setPosition(ofPoint & p)
{
	b.set(p.x,p.y,b.width,b.height);
	generateDraw();
}


void CFHDockPanel::generateDraw(){

	
	border.clear();
	border.setStrokeColor(BorderColor);
	border.setColor(BackgroundColor);
	border.rectangle(b);
	border.setStrokeWidth(1);
	border.setFilled(true);

	headerBg.clear();
	headerBg.setFillColor(HeaderColor);
	headerBg.setFilled(true);
	headerBg.rectangle(b.x,b.y+1,b.width,m_headerHeight);

	iconBox.set(b.x+b.width - ICONSIZE-1,b.y + m_headerHeight - ICONSIZE - 1,ICONSIZE,ICONSIZE);

	if(labelMesh.empty()){
		if(pzhFont && pzhFont->isLoaded())
			labelMesh = pzhFont->getStringAsPoints(m_wsLabel);
	}
	

}

ofRectangle CFHDockPanel::getBodyRect()
{
	return b;
}

void CFHDockPanel::registerMouseEvents()
{
	ofAddListener(ofEvents().mouseMoved, this, &CFHDockPanel::mouseMoved);
	ofAddListener(ofEvents().mousePressed, this, &CFHDockPanel::mousePressed);
	ofAddListener(ofEvents().mouseReleased, this, &CFHDockPanel::mouseReleased);
    ofAddListener(ofEvents().draw, this, &CFHDockPanel::draw);
    ofAddListener(ofEvents().exit, this, &CFHDockPanel::exit);
	ofAddListener(ofEvents().windowResized,this,&CFHDockPanel::windowResized);
}

void CFHDockPanel::exit(ofEventArgs & event)
{
	stopThread();
	waitForThread();
}


void CFHDockPanel::unregisterMouseEvents()
{
	ofRemoveListener(ofEvents().mouseMoved, this, &CFHDockPanel::mouseMoved);
	ofRemoveListener(ofEvents().mousePressed, this, &CFHDockPanel::mousePressed);
	ofRemoveListener(ofEvents().mouseReleased, this, &CFHDockPanel::mouseReleased);
    ofRemoveListener(ofEvents().draw, this, &CFHDockPanel::draw);
	ofRemoveListener(ofEvents().windowResized,this,&CFHDockPanel::windowResized);
	ofRemoveListener(ofEvents().setup,this,&CFHDockPanel::setup);
	ofRemoveListener(ofEvents().exit,this,&CFHDockPanel::exit);

}

void CFHDockPanel::setup(ofEventArgs & event)
{
	int width = ofGetWindowWidth();
	int height = ofGetScreenHeight();
	m_iOldWindowWidth = width;
	switch(m_eDockSide){
	case DSLEFT:
		m_iOriBX = b.x = UNHIDEWIDTH - defaultWidth ;
		break;
	case DSRIGHT:
		m_iOriBX = b.x = ofGetWindowWidth() - UNHIDEWIDTH;
		break;
	}
	b.y = DOCKMARGIN;
	m_headerHeight = defaultHeaderHeight;
	b.width = defaultWidth;
	b.height = height - 2 * DOCKMARGIN;
	loadIcons();
	b.height = ofGetWindowHeight() - 2 * DOCKMARGIN;
	generateDraw();
	startThread();
	registerMouseEvents();
}

void CFHDockPanel::windowResized(ofResizeEventArgs & resize)
{
	//windowResized(resize.width,resize.height);
	m_mutex.lock();
	switch(m_eDockSide){
	case DSLEFT:
		m_iOriBX =  UNHIDEWIDTH - defaultWidth ;
		break;
	case DSRIGHT:
		m_iOriBX =  resize.width - UNHIDEWIDTH;
		m_eDockState = m_eDockState==DSHIDE?DSSHOW:DSHIDE;
		break;
	}
	b.height = resize.height - 2 * DOCKMARGIN;

	generateDraw();
	m_mutex.unlock();
	m_iOldWindowWidth=resize.width;
}

