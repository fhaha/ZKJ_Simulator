#pragma once
#pragma pack(push)
#pragma pack(1)
#define COMHEAD	0x90EB
#define COMH1	0xEB
#define COMH2	0x90


#define ACRESID	0x5303
#define ACID 0x5304
#define SCID	0x5305
#define CARGPSPOSID	0x5306
#define DELTAID	0x5307
#define CARINITID	0x5308
#define INITPOSRESID	0x5309
#define INITPOSID	0x530A
#define POSRESID 0x530B
#define ZDID	0x530C
#define GCID	0x530D
#define GCRESID	0x530E
#define POSID	0x530F

#define CAPPKGID	0x6001
#define IDLEID		0x6002
#define DELTAACKID	0x6003
#define INVPKG_ID	0x5A0A
#define ROLLPKG_ID	0x5A0B
#define CARPOSID	0x5A0C
#define SDATAID		0x5A0D



#define ERRPKGID	0x5FFF


#define ZD_ID1	0x0C
#define ZD_ID2	0X53
#define GC_ID1	0x0D
#define GC_ID2	0x53
#define GCRES_ID1	0x0E
#define GCRES_ID2	0x53
#define POS_ID1		0x0F
#define POS_ID2		0x53
#define INITPOS_ID1	0x0A
#define INITPOS_ID2 0x53
#define INITPOSRES_ID1 0x09
#define INITPOSRES_ID2 0x53
#define ERRORPKGID1 0xFF
#define ERRORPKGID2	0x5F

#define ROLLPKGID1	0x0B
#define ROLLPKGID2	0x5A

#define INVPKGID1	0x0A
#define INVPKGID2	0x5A


#define CALCGAMMA_ERROR 0xFEu
#define DELTATIME_ERROR	0xFDu
#define GPSEXPIRE_ERROR	0xFCu
#define SDATA_ERROR 0xFBu
#define MUTEX_ERROR 0xFAu
#define INV_ERROR 0xF0u
#define NOGPS_ERROR	0xEFu

typedef struct _comhead_st {
	unsigned short phead;	//0x90EB
	unsigned char len;
	unsigned short id;
}comhead_st,*pcomhead_st;

typedef struct _zkjerror_st {
	unsigned short phead;	//0x90EB
	unsigned char len;
	unsigned short id;
	unsigned char errnum;
	float extra1;
	float extra2;
	float extra3;
	unsigned char crc; 
}zkjerr_st,*pzkjerr_st;

#pragma pack(pop)
