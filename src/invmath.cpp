#include "invmath.h"
#include "ofAppRunner.h"

#if defined(TARGET_OSX) || defined(TARGET_LINUX)
#include "../../../../stm32f405_inv/bsp/stm32f40x/applications/invmath.c"
#else
#include "..\..\stm32f405_inv\bsp\stm32f40x\applications\invmath.c"
#endif
