#pragma once

#include "ofxGuiGroup.h"

class ofxGuiGroup;

class CFHDashBoard : public ofxGuiGroup {
public:
	CFHDashBoard();
	CFHDashBoard(const ofParameterGroup & parameters, string filename="settings.xml", float x = 10, float y = 10);
	~CFHDashBoard();

	CFHDashBoard * setup(string collectionName="", string filename="settings.xml", float x = 10, float y = 10);
	CFHDashBoard * setup(const ofParameterGroup & parameters, string filename="settings.xml", float x = 10, float y = 10);

	bool mouseReleased(ofMouseEventArgs & args);

	ofEvent<void> loadPressedE;
	ofEvent<void> savePressedE;

	virtual bool keyPressed(ofMouseEventArgs & args)
	{
		return false;
	}
	virtual bool keyReleased(ofMouseEventArgs & args)
	{
		return false;
	}
protected:
	void render();
	bool setValue(float mx, float my, bool bCheck);
	void generateDraw();
	void loadIcons();
private:
	ofRectangle loadBox, saveBox;
	static ofImage loadIcon, saveIcon;
    
    ofPoint grabPt;
	bool bGrabbed;
};
