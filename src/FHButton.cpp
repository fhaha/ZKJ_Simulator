/*
 *  ofxSimpleSlider.cpp
 *  Created by Golan Levin on 2/24/12.
 *
 */

#include "FHButton.h"
#include "FHUtil.h"
#define FONTSIZE 16
#define HMARGIN	4
#define VMARGIN 4

ofxTrueTypeFontUC * CFHButton::pzhFont=NULL;
void CFHButton::removeDefaultListener()
{
    ofRemoveListener(ofEvents().draw, this, &CFHButton::draw);
	ofRemoveListener(ofEvents().mouseMoved, this, &CFHButton::mouseMoved);
	ofRemoveListener(ofEvents().mousePressed, this, &CFHButton::mousePressed);
	ofRemoveListener(ofEvents().mouseReleased, this, &CFHButton::mouseReleased);
}
void CFHButton::addDefaultListener()
{
    ofAddListener(ofEvents().draw, this, &CFHButton::draw);
	ofAddListener(ofEvents().mouseMoved, this, &CFHButton::mouseMoved);
	ofAddListener(ofEvents().mousePressed, this, &CFHButton::mousePressed);
	ofAddListener(ofEvents().mouseReleased, this, &CFHButton::mouseReleased);
}


CFHButton &  CFHButton::setup(std::wstring & str,ofxTrueTypeFontUC * pFont,std::wstring altstr)
{
	if(pzhFont==NULL)
		pzhFont = pFont;
	m_wsLabel = str; 
	m_wsAltLabel = altstr;
	generateDraw();
	if(pzhFont->isLoaded())
		m_labelshape = pzhFont->getStringAsPoints(m_wsLabel);
    addDefaultListener();
	return *this;
}

CFHButton &  CFHButton::setup(std::string & str,ofxTrueTypeFontUC * pFont,std::string  altstr)
{
    std::wstring wstr = s2ws(str);
    std::wstring waltstr = s2ws(altstr);
	return setup(wstr,pFont,waltstr);
}


bool CFHButton::isToggled()
{
	return m_bToggled;
}

void CFHButton::resize(float inw,float inh)
{
	/*
	width = inw;
	height = inh;
	box.set(x,y, width, height);\*/
	ofLogVerbose("FHB") << "not support resize!" << std::endl;
}

void CFHButton::enable()
{
	bEnabled = true;
}
void CFHButton::disable()
{
	bEnabled = false;
}

bool CFHButton::isEnabled()
{
	return bEnabled;
}

void CFHButton::move(float inx,float iny)
{
	x = inx;
	y = iny;
	box.setX(x);
	box.setY(y);
}

void CFHButton::move(ofPoint & pos)
{
	box.setPosition(pos);
}

void CFHButton::generateDraw()
{
	if(pzhFont==NULL || !pzhFont->isLoaded())
		return;
	ofRectangle outRec = pzhFont->getStringBoundingBox(m_wsLabel,0,0);
	outRec.width += 2* HMARGIN;
	outRec.height += 2* VMARGIN;

	if(m_wsAltLabel.empty()){
		m_eButtonType = FHBTNORMAL;
	}
	else{
		m_eButtonType = FHBTTOGGLE;
		if(m_bToggled && pzhFont && pzhFont->isLoaded()){
			outRec = pzhFont->getStringBoundingBox(m_wsLabel,0,0);
			outRec.width += 2* HMARGIN;
			outRec.height += 2* VMARGIN;
		}
	}
	m_labelDrawPos.x = box.x + (box.width - outRec.width) / 2;
	m_labelDrawPos.y= box.y + box.height - (outRec.height-FONTSIZE) /2 ;


	/*if(outRec.width>box.width || outRec.height > box.height){
		box.set(outRec);
	}*/
}


void CFHButton::render()
{
	ofColor oldColor = ofStyle().color;
	ofEnableAlphaBlending();
	ofDisableSmoothing();
	ofPushMatrix();
	ofTranslate(x,y,0);

	// Use different alphas if we're actively maniupulating me.
	float sliderAlpha = (bHasFocus) ? 128:64;
	float spineAlpha  = (bHasFocus) ? 255:160;

	// draw box outline
	ofNoFill();
	ofSetLineWidth(1.0);
	ofSetColor(64,64,64, sliderAlpha);
	ofRect(box.x,box.y, box.width,box.height);

	// draw spine
	//float labelStringWidth = m_labelshape.size()*14;
	ofSetLineWidth(1.0);
	ofSetColor(0,0,0, spineAlpha);
	if (bPressed){
		ofLine(box.x,box.y, box.width+box.x,box.y);
		ofLine(box.x,box.y,box.x,box.y + box.height);
	}
	ofSetColor(200,255,100,255);
	vector<ofPath>::iterator iter = m_labelshape.begin();
	for(;iter!=m_labelshape.end();iter++){
		(*iter).draw(m_labelDrawPos.x,m_labelDrawPos.y);
	}
	//ofDrawBitmapString( ofToString(getValue(),numberDisplayPrecision), width+5,height);
	ofPopMatrix();
	ofSetLineWidth(1.0);
	ofDisableAlphaBlending();
	ofSetColor(oldColor);
}

void CFHButton::draw()
{
	if(m_bHide)
		return;
	render();
}

//----------------------------------------------------
void CFHButton::draw(ofEventArgs& event){
	if(m_bHide)
		return;
	render();
}

//----------------------------------------------------
void CFHButton::mouseMoved(ofMouseEventArgs& event)
{
	if(bEnabled && box.inside(event.x,event.y)){
		bHasFocus = true;
	}
	else{
		bHasFocus = false;
	}
}
void CFHButton::mouseDragged(ofMouseEventArgs& event){
	if (bEnabled && bHasFocus){
		//updatePercentFromMouse (event.x, event.y);
	}
}
void CFHButton::mousePressed(ofMouseEventArgs& event){

	if (bEnabled && box.inside(event.x, event.y) && bHasFocus){
		bPressed = true;

		if(m_eButtonType==FHBTTOGGLE){
			m_labelshape.clear();
			m_bToggled = !m_bToggled;
			if(m_bToggled){
				if(pzhFont || pzhFont->isLoaded())
					m_labelshape = pzhFont->getStringAsPoints(m_wsAltLabel);
			}
			else{
				if(pzhFont || pzhFont->isLoaded())
					m_labelshape = pzhFont->getStringAsPoints(m_wsLabel);
			}
		}
	}
}
void CFHButton::mouseReleased(ofMouseEventArgs& event){
	if (bEnabled && bHasFocus){
		if (box.inside(event.x, event.y)){
			int ea = 1;
			ofNotifyEvent(btnEvent,ea,this);
		}
	}
	bPressed = false;
}

void CFHButton::defaultMouseHandler(ofMouseEventArgs & event){
	if(box.inside(event.x,event.y)){
		std::ostringstream ost;
		ost << "defaultMouseHandler:" << " x:" << event.x << " y:" << event.y;
		ost << " button: " << event.button;
		ofLog(OF_LOG_ERROR,"Failed to get the environment using GetEnv()");
	}
}

void CFHButton::defaultTouchHandler(ofTouchEventArgs & event){
	if(box.inside(event.x,event.y)){
		std::ostringstream ost;
		ost << "defaultTouchHandler:" << " x:" << event.x << " y:" << event.y;
		ost << " type: " << event.type;
		ofLog(OF_LOG_ERROR,"Failed to get the environment using GetEnv()");
	}
}



