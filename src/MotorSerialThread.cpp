#include "ofApp.h"
#include "motorpkg.h"
#include "MotorSerialThread.h"
#include "invmath.h"
//#define USELOG

#ifndef USELOG
#define COUTLOG()	std::cout<<m_LogWindow.str();\
		m_LogWindow.clear();\
		m_LogWindow.str("")
#endif


#define FGLASTSIZE	20

#define PROCESMVALUE(vec,data)	do{\
	vec.push_back(data);\
	while(vec.size() > FGLASTSIZE){\
		vec.erase(vec.begin());\
	}}while(0)
CMotorSerialThread::CMotorSerialThread(void)
{
	m_bIsQuiting = false;
	m_bPositiveDirec = true;
	m_iXYStep = X_ASS1;
}

CMotorSerialThread::~CMotorSerialThread(void)
{
}

void CMotorSerialThread::quit()
{
	m_bIsQuiting = true;
}


bool CMotorSerialThread::setup(std::string device, int baudrate)
{
	m_serial.listDevices();
	vector <ofSerialDeviceInfo> deviceList = m_serial.getDeviceList();
	vector<ofSerialDeviceInfo>::iterator iter = deviceList.begin();
	string devname;
	while(iter != deviceList.end()){
		if(device == (*iter).getDeviceName()){
			devname = device;
		}
		++iter;
	}
	if(devname.empty()){
		return false;
	}
	m_serial.setup(devname,baudrate);
	startThread();
	return true;
}


void CMotorSerialThread::SendFXChange(bool isAdd,unsigned short step)
{
	motorpkg mpkg = {MPHEAD,YADD,step};
	if(!isAdd)
		mpkg.action = YSUB;
	m_fOldMean = m_fNewMean;

	printf("send x %s step %d\n",isAdd?"forward":"backward",step);
	m_serial.writeBytes((unsigned char *)&mpkg,sizeof(motorpkg));

}

void CMotorSerialThread::SendFYChange(bool isAdd,unsigned short step)
{

	motorpkg mpkg = {MPHEAD,XADD,step};
	if(!isAdd)
		mpkg.action = XSUB;
	m_fOldMean = m_fNewMean;
	printf("send y %s step %d\n",isAdd?"forward":"backward",step);
	m_serial.writeBytes((unsigned char *)&mpkg,sizeof(motorpkg));
}


void CMotorSerialThread::changeMotorDirection()
{
	m_bPositiveDirec = !m_bPositiveDirec;
	//Thread::Sleep(4000);
	sleep(4000);
}

CMotorSerialThread & CMotorSerialThread::operator<<(const gcres_pkg &gcrespkg)
{
		PROCESMVALUE(m_lastfmibbx,gcrespkg.fmibbx);
		PROCESMVALUE(m_lastfmibby,gcrespkg.fmibby);
		PROCESMVALUE(m_lastfmibbz,gcrespkg.fmibbz);
		/* PROCESMVALUE(m_lastwmibbx,gcrespkg.wmibbx);
		PROCESMVALUE(m_lastwmibby,gcrespkg.wmibby);
		PROCESMVALUE(m_lastwmibbz,gcrespkg.wmibbz);*/
		return *this;
	}


void CMotorSerialThread::CaptureData()
{
	m_lastfmibbx.clear();
	while(m_lastfmibbx.size() < FGLASTSIZE){
		sleep(10) ;
	}

	m_fNewMean = 0.0f;
	switch(m_eADMode){
	case AXADJ:{
		for(int i = 0 ;i<FGLASTSIZE;i++){
			m_fNewMean += m_lastfmibbx[i];
		}
		break;
	}
	case AYADJ:{
		for(int i = 0 ;i<FGLASTSIZE;i++){
			m_fNewMean += m_lastfmibby[i];
		}
		break;
	}
	case AZADJ:{
		for(int i = 0 ;i<FGLASTSIZE;i++){
			m_fNewMean += m_lastfmibbz[i];
		}
		break;
	}
	case GXADJ:{
		for(int i = 0 ;i<FGLASTSIZE;i++){
			m_fNewMean += m_lastgmibbx[i];
		}
		break;
	}
	case GYADJ:{
		for(int i = 0 ;i<FGLASTSIZE;i++){
			m_fNewMean += m_lastgmibby[i];
		}
		break;
	}
	case GZADJ:{
		for(int i = 0 ;i<FGLASTSIZE;i++){
			m_fNewMean += m_lastgmibbz[i];
		}
		break;
	}
	case ADJNORMAL:
	default:
		break;
	}
	m_fNewMean /= FGLASTSIZE;
}

void CMotorSerialThread::StepForward()
{
	sleep(1000);
	CaptureData();
#ifdef USELOG
	CFHLogWindow &m_LogWindow = ((ofApp*)ofGetAppPtr())->getLW();
#else
	stringstream m_LogWindow;
#endif
	//Step 1~3 X Axis
	//Step 4~6 Y Axis


	if(m_eADMode != ADJNORMAL){
		m_LogWindow << "step " << m_iXYStep << " old " << m_fOldMean << " new " << m_fNewMean << LOGNL;
		COUTLOG();
		switch(m_iXYStep){
		case X_ASS1:
			m_fAssValue[1] = fabsf(m_fNewMean);
			m_iXYStep = X_ASS2;
			SendFXChange(m_bPositiveDirec,ADASSSTEP);
			break;
		case X_ASS2:
			m_fAssValue[2] = fabsf(m_fNewMean);
			if(compareFunc(m_fAssValue[0],m_fAssValue[1])){
				if(compareFunc(m_fAssValue[1] , m_fAssValue[2])){ //continue forward
					m_LogWindow << "x assess //" << LOGNL;
					COUTLOG();
					//m_iXYStep = X_ADD;
					m_fAssValue[0]=m_fAssValue[1];
					m_fAssValue[1]=m_fAssValue[2];
					SendFXChange(m_bPositiveDirec,ADASSSTEP);
				}
				else if(compareFunc(m_fAssValue[2] , m_fAssValue[1])){ //rever backward
					m_LogWindow << "x assess /\\" << LOGNL;
					COUTLOG();
					changeMotorDirection();
					m_iXYStep = X_ADD;
					SendFXChange(m_bPositiveDirec,ADASSSTEP);
				}
				else{
					m_LogWindow << "step " << m_iXYStep << "Should not here" << __FILE__ << __LINE__ << LOGNL;
					COUTLOG();
				}

			}
			else if(compareFunc(m_fAssValue[1] , m_fAssValue[0])){
				if(compareFunc(m_fAssValue[2] , m_fAssValue[1])){ //back first point
					if(++m_iBackTime>=2)
					{
						m_LogWindow << "here is near x best point"  << LOGNL;
						COUTLOG();
						m_iBackTime = 0;
						m_iXYStep = X_SUB;
						changeMotorDirection();
						SendFXChange(m_bPositiveDirec,2*ADASSSTEP+ TUNNINGSTEP);
						m_fOldMean = m_fAssValue[0];
					}
					else{
						m_LogWindow << "x assess \\\\ back to x assess 1"  << LOGNL;
						COUTLOG();
						m_iXYStep = X_ASS1;
						changeMotorDirection();
						SendFXChange(m_bPositiveDirec,3*ADASSSTEP);
						m_fOldMean = m_fAssValue[0];
						m_iXYStep = X_ASS1;
					}
				}
				else{
					m_LogWindow << "step " << m_iXYStep << "x assess \\/ Should not here" << __FILE__ << __LINE__ << " !!!readjust" << LOGNL;
					COUTLOG();
					changeMotorDirection();
					SendFXChange(m_bPositiveDirec,ADASSSTEP);
					m_fAssValue[0] = m_fAssValue[2];
					m_iXYStep = X_ASS1;
				}
			}
			break;
		case Y_ASS1:
			m_fAssValue[1] = fabsf(m_fNewMean);
			m_iXYStep = Y_ASS2;
			SendFYChange(m_bPositiveDirec,ADASSSTEP);
			break;
		case Y_ASS2:
			m_fAssValue[2] = fabsf(m_fNewMean);
			if(compareFunc(m_fAssValue[0] , m_fAssValue[1])){
				if(compareFunc(m_fAssValue[1] , m_fAssValue[2])){ //continue forward
					m_LogWindow << "y assess //" << LOGNL;
					COUTLOG();
					m_fAssValue[0]=m_fAssValue[1];
					m_fAssValue[1]=m_fAssValue[2];
					SendFYChange(m_bPositiveDirec,ADASSSTEP);
				}
				else if(compareFunc(m_fAssValue[2] , m_fAssValue[1])){ //rever backward
					m_LogWindow << "y assess /\\" << LOGNL;
					COUTLOG();
					changeMotorDirection();

					SendFYChange(m_bPositiveDirec,ADASSSTEP);
					m_iXYStep = Y_ADD;
				}
				else{
					m_LogWindow << "step " << m_iXYStep << "Should not here" << __FILE__ << __LINE__ << " !!!readjust" << LOGNL;
					COUTLOG();
				}

			}
			else if(compareFunc(m_fAssValue[1] , m_fAssValue[0])){
				if(compareFunc(m_fAssValue[2] , m_fAssValue[1])){ //back first point
					if(++m_iBackTime>=2){
						m_LogWindow << "here is near y best point" << LOGNL;
						COUTLOG();
						m_iXYStep = Y_SUB;
						changeMotorDirection();
						SendFYChange(m_bPositiveDirec,2*ADASSSTEP+TUNNINGSTEP);
						m_fOldMean = m_fAssValue[0];

					}
					else{
						m_LogWindow << "y assess \\\\ back to yass1" << LOGNL;
						COUTLOG();
						m_iXYStep = Y_ASS1;
						changeMotorDirection();
						SendFYChange(m_bPositiveDirec,3*ADASSSTEP);
						m_fOldMean = m_fAssValue[0];
						//fAssValue[0] = m_fAssValue[2];
					}
				}
				else{
					m_LogWindow << "y assess \\/ " << m_iXYStep << "Should not here" << __FILE__ << __LINE__ << LOGNL;
					COUTLOG();
					changeMotorDirection();
					SendFYChange(m_bPositiveDirec,ADASSSTEP);
					m_fAssValue[0] = m_fAssValue[2];
					m_iXYStep = Y_ASS1;
				}
			}
			break;
		default:
			if(isBadEffect()){ //effect bad

				printf("old %f new %f bad effect\n",m_fOldMean,m_fNewMean);
				switch(m_iXYStep){
				case X_SUB:	{ // x bad effect
					changeMotorDirection();	//change direct
					SendFXChange(m_bPositiveDirec);
					m_iXYStep = X_ADD;
					m_LogWindow << "into step 2 change x " << (m_bPositiveDirec?"forward":"backward") << LOGNL;
					COUTLOG();
					break;
				}
				case X_ADD:{
					changeMotorDirection();
					m_iXYStep = X_REV;

					m_LogWindow << "x max has passed change to step "<< m_iXYStep << LOGNL;
					COUTLOG();

					SendFXChange(m_bPositiveDirec);
					break;
				}
				case X_REV:{
					COUTLOG();
					CaptureData();
					m_LogWindow << "step " << m_iXYStep << " bad effect ,want capture again get "<< m_fNewMean << LOGNL;
					if(isBadEffect()){
						m_LogWindow << "step " << m_iXYStep << " should not in bad effect,x adjust failed!!!!back step" << X_ADD << LOGNL;
						COUTLOG();
						changeMotorDirection();
						SendFXChange(m_bPositiveDirec,MICROSTEP);
						m_iXYStep = X_ADD;
					}
					else{ // not bad effect
						m_LogWindow << "now start y adjust" << LOGNL;
						COUTLOG();
						m_iBackTime = 0;
						m_fAssValue[0] = m_fNewMean;
						m_iXYStep= Y_ASS1;
						SendFYChange(m_bPositiveDirec,ADASSSTEP);
					}
					break;
				}
				case Y_SUB:{
					changeMotorDirection();	//change direct
					SendFYChange(m_bPositiveDirec);
					m_iXYStep = Y_ADD;
					m_LogWindow << "change y " << (m_bPositiveDirec?"forward":"backward") << LOGNL;
					COUTLOG();
					break;
				}
				case Y_ADD:{
					changeMotorDirection();
					m_iXYStep = Y_REV;
					m_LogWindow << "y max has passed change to step " << Y_REV << LOGNL;
					COUTLOG();
					SendFYChange(m_bPositiveDirec);
					break;
				}
				case Y_REV:{
					CaptureData();
					m_LogWindow << "step "<< m_iXYStep << " want change direct capture again get " << m_fNewMean << LOGNL;
					COUTLOG();
					if(isBadEffect()){
						m_LogWindow << "step "<< m_iXYStep << " should not in bad effect, adjust failed!!!!back step" << Y_SUB << LOGNL;
						COUTLOG();
						changeMotorDirection();
						SendFYChange(m_bPositiveDirec,MICROSTEP);
						m_iXYStep = Y_SUB;
					}
					else{ // not bad effect
						m_LogWindow << "y adjust finished" << LOGNL;
						COUTLOG();
						m_bPositiveDirec = true;
						m_eADMode=ADJNORMAL;
						m_iXYStep = X_SUB;
					}
					break;
				}
				default:

					m_LogWindow << "Should not here" << __FILE__ << __LINE__ << LOGNL;
					COUTLOG();
					break;
				}
			}
			else{ // good effect
				printf("old %f new %f good effect\n",m_fOldMean,m_fNewMean);
				switch(m_iXYStep){
				case X_SUB:	{ // x good effect
					m_iXYStep = X_ADD;
					SendFXChange(m_bPositiveDirec);

					m_LogWindow << "into step "<< m_iXYStep<< " x " << (m_bPositiveDirec?"forward":"backward") << LOGNL;
					COUTLOG();
					break;
				}
				case X_ADD:{
					SendFXChange(m_bPositiveDirec);

					m_LogWindow << "still x " << (m_bPositiveDirec?"forward":"backward") << LOGNL;
					COUTLOG();
					break;
				}
				case X_REV:{
					if(compareHistoryFunc()){
						m_LogWindow << "step " << m_iXYStep << " bad mean "<< m_fNewMean  << " than history max " << m_fHistoryMM << " back to step" << X_SUB << LOGNL;
						m_iXYStep= X_SUB;
						SendFXChange(m_bPositiveDirec);
						COUTLOG();
					}
					else{
						m_LogWindow << "now start y adjust" << LOGNL;
						COUTLOG();
						m_iBackTime = 0;
						m_bPositiveDirec = true;
						m_fAssValue[0] = m_fNewMean;
						m_iXYStep= Y_ASS1;
						SendFYChange(m_bPositiveDirec,ADASSSTEP);
					}
					break;
				}
				case Y_SUB:{
					m_iXYStep = Y_ADD;
					SendFYChange(m_bPositiveDirec);
					m_LogWindow << "into step "<< Y_ADD <<  " y " << (m_bPositiveDirec?"forward":"backward") << LOGNL;
					COUTLOG();
					break;
				}
				case Y_ADD:{
					SendFYChange(m_bPositiveDirec);
					m_LogWindow << "still y " << (m_bPositiveDirec?"forward":"backward") << LOGNL;
					COUTLOG();
					break;
				}
				case Y_REV:{
					if(compareHistoryFunc()){
						m_LogWindow << "step" << m_iXYStep << " bad mean "<< m_fNewMean  << " than history max " << m_fHistoryMM << " back to step "<< Y_SUB << LOGNL;
						m_iXYStep= Y_SUB;
						SendFYChange(m_bPositiveDirec);
						COUTLOG();
					}
					else{
						m_LogWindow << "new value " << m_fNewMean << " is near historyMM " << m_fHistoryMM <<"y adjust finished" << LOGNL;
						COUTLOG();
						m_bPositiveDirec = true;
						m_eADMode=ADJNORMAL;
						m_iXYStep = X_SUB;
					}
					break;
				}
				default:

					m_LogWindow << "Should not here" << __FILE__ << __LINE__ << LOGNL;
					COUTLOG();
					break;
				}
			}
		}

	}

	updateHistoryMM(m_fNewMean);
}

#define MTBUFLEN	256
void CMotorSerialThread::threadedFunction()
{
	unsigned char buf[MTBUFLEN]={0};
	int recvOff = 0;
	//m_serial.writeBytes(cd5,sizeof(cd5));
	while(!m_bIsQuiting){
		if(m_serial.available()){
			recvOff += m_serial.readBytes(buf+recvOff,32);
			while(recvOff>=sizeof(motorpkg)){
				pmotorpkg pMPkg = (pmotorpkg)buf;
				if(pMPkg->head != MPHEAD)
				{
					continue;
				}
				if(pMPkg->action == HDONE){
					StepForward();
				}
				if(recvOff>= sizeof(motorpkg)){

					recvOff -= sizeof(motorpkg);
				}

			}
		}
		sleep(1);

	}
	//m_serial.close();
}

void CMotorSerialThread::SendXAdjust(int step,int needres)
{
	motorpkg mpkg = {MPHEAD,YADD,abs(step)};

	if(!needres)
		mpkg.action = YNADD;
	if(step < 0){
		if(mpkg.action == YNADD)
			mpkg.action = YNSUB;
		else
			mpkg.action = YSUB;
	}
	m_serial.writeBytes((unsigned char *)&mpkg,sizeof(motorpkg));
}

void CMotorSerialThread::SendYAdjust(int step,int needres)
{
	motorpkg mpkg = {MPHEAD,XADD,abs(step)};
	if(!needres)
		mpkg.action = XNADD;
	if(step < 0){
		if(mpkg.action == XADD)
			mpkg.action = XSUB;
		else
			mpkg.action = XNSUB;
	}
	m_serial.writeBytes((unsigned char *)&mpkg,sizeof(motorpkg));

}

void CMotorSerialThread::SendNear(int value)
{
	motorpkg mpkg = {MPHEAD,ZNEAR,0};
	mpkg.actionvalue = value;
	m_serial.writeBytes((unsigned char *)&mpkg,sizeof(motorpkg));
}

void CMotorSerialThread::SendFar(int value)
{
	motorpkg mpkg = {MPHEAD,ZFAR,0};
	mpkg.actionvalue = value;
	m_serial.writeBytes((unsigned char *)&mpkg,sizeof(motorpkg));
}

void CMotorSerialThread::StartAdjust(_E_ADMODE mode)
{
	m_eADMode = mode;
	m_iXYStep=X_ASS1;
	m_fNewMean = 0.0f;
	m_iBackTime = 0;
	switch(m_eADMode){
	case AXADJ:{
		for(int i = 0 ;i<m_lastfmibbx.size();i++){
			m_fNewMean += m_lastfmibbx[i];
		}
		m_fNewMean /= m_lastfmibbx.size();
		break;
	}
	case AYADJ:{
		for(int i = 0 ;i<m_lastfmibby.size();i++){
			m_fNewMean += m_lastfmibby[i];
		}
		m_fNewMean /= m_lastfmibby.size();
		break;
	}
	case AZADJ:{
		for(int i = 0 ;i<m_lastfmibbz.size();i++){
			m_fNewMean += m_lastfmibbz[i];
		}
		m_fNewMean /= m_lastfmibbz.size();
		break;
	}
	case GXADJ:{
		for(int i = 0 ;i<m_lastgmibbx.size();i++){
			m_fNewMean += m_lastgmibbx[i];
		}
		m_fNewMean /= m_lastgmibbx.size();
		break;
	}
	case GYADJ:{
		for(int i = 0 ;i<m_lastgmibby.size();i++){
			m_fNewMean += m_lastgmibby[i];
		}
		m_fNewMean /= m_lastgmibby.size();
		break;
	}
	case GZADJ:{
		for(int i = 0 ;i<m_lastgmibbz.size();i++){
			m_fNewMean += m_lastgmibbz[i];
		}
		m_fNewMean /= m_lastgmibbz.size();
		break;
	}
	case ADJNORMAL:
	default:
		break;
	}
	m_fAssValue[0] = m_fNewMean;
	m_fHistoryMM = fabsf(m_fNewMean);
	SendFXChange(m_bPositiveDirec,ADASSSTEP);
}


bool CMotorSerialThread::isBadEffect()
{
	if(m_bIsMax)
		return (fabsf(m_fOldMean) > fabsf(m_fNewMean));
	else
		return (fabsf(m_fOldMean) < fabsf(m_fNewMean));
}

bool CMotorSerialThread::compareFunc(float value1,float value2)
{
	if(m_bIsMax)
		return value1 < value2;
	else
		return (value1 > value2);
}

bool CMotorSerialThread::compareHistoryFunc()
{
	if( (int)(m_fNewMean * 1000) == (int)(m_fHistoryMM*1000))
		return false;
	if( fabsf(fabsf(m_fNewMean) - fabsf( m_fHistoryMM)) < 0.0003f)
		return false;
	return true;
}


void CMotorSerialThread::updateHistoryMM(float value)
{
	if(m_bIsMax){
		if(fabsf(value) > m_fHistoryMM)
			m_fHistoryMM = fabsf(value);
		else if(is_zero(m_fHistoryMM)){
			m_fHistoryMM = fabsf(value);
		}
	}
	else{
		if(fabsf(value) < m_fHistoryMM)
			m_fHistoryMM = fabsf(value);
		else if(is_zero(m_fHistoryMM)){
			m_fHistoryMM = fabsf(value);
		}
	}
}
