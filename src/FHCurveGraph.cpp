#include "FHCurveGraph.h"

#include "ofGraphics.h"
#include "ofImage.h"
#include "ofMain.h"

#define PIXELS_PER_SECOND	100

ofImage CFHCurveGraph::maxIcon;
ofImage CFHCurveGraph::minIcon;
ofImage CFHCurveGraph::normalIcon;


CFHCurveGraph::CFHCurveGraph()
:bGrabbed(false),m_eGUIMinMaxMode(FHNORMAL),m_fNormalWidth(DEFAULTNORMALWIDTH),m_fNormalHeight(300.0f){
	m_dispLatestTime = ofGetElapsedTimeMillis();
	m_curplindex = 0;
	plColors[0]=ofColor(255,0,0,200);
	plColors[1]=ofColor(255,255,0,200);
	plColors[2]=ofColor(0,0,255,200);
	plColors[3]=ofColor(0,255,255,200);
	for(int i = 0 ;i < FHCGLINENUM;i++){
		m_bDispData[i]=NULL;
	}
}

CFHCurveGraph::CFHCurveGraph(const ofParameterGroup & parameters, string filename, float x, float y,float width,FHGUI_MODE defaultmode)
: ofxGuiGroup(parameters, filename, x, y)
, bGrabbed(false),m_eGUIMinMaxMode(defaultmode),m_fNormalWidth(DEFAULTNORMALWIDTH),m_fNormalHeight(300.0f){
	m_dispLatestTime = ofGetElapsedTimeMillis();
	m_curplindex = 0;
	for(int i = 0 ;i < FHCGLINENUM;i++){
		m_bDispData[i]=NULL;
	}
}

CFHCurveGraph::~CFHCurveGraph(){
	/*for(int i = 0 ;i < FHCGLINENUM;i++){
		if(m_bDispData[i]!=NULL)
			delete m_bDispData[i];
	}*/
}


CFHCurveGraph * CFHCurveGraph::setup(string collectionName, string filename, float x, float y,float width,FHGUI_MODE defaultmode){
	if(!normalIcon.isAllocated() || !minIcon.isAllocated() || !maxIcon.isAllocated()){
		loadIcons();
	}
	registerMouseEvents();
	CFHCurveGraph * pRet =  (CFHCurveGraph*)ofxGuiGroup::setup(collectionName,filename,x,y);
	m_fNormalWidth = width;
    m_eGUIMinMaxMode=defaultmode;
	m_fLastX = getPosition().x;
	m_fLastHeight = getHeight();
	m_fMinHeight = b.height;
	setSize(m_fNormalWidth,m_fLastHeight);
	ofxToggle* pSymmatic = new ofxToggle(m_bSymmatic.set("Symm",false),70);
	collection.push_back( pSymmatic );
	ofPoint lpos ;
	lpos.x = getWidth() - pSymmatic->getWidth();
	lpos.y = b.y + header + 2;
	pSymmatic->setPosition(lpos);
	return pRet;
}

CFHCurveGraph * CFHCurveGraph::setup(const ofParameterGroup & parameters, string filename, float x, float y,float width,FHGUI_MODE defaultmode){
	if(!normalIcon.isAllocated() || !minIcon.isAllocated() || !maxIcon.isAllocated()){
		loadIcons();
	}
	registerMouseEvents();
	CFHCurveGraph * pRet =  (CFHCurveGraph*)ofxGuiGroup::setup(parameters,filename,x,y);
	m_fNormalWidth = width;
	m_fLastX = getPosition().x;
	m_fLastHeight = getHeight();
	m_fMinHeight = b.height;
    m_eGUIMinMaxMode=defaultmode;
	setSize(m_fNormalWidth,m_fLastHeight);

	return pRet;
}

void CFHCurveGraph::setData(ofParameter<float> & parameter,int index)
{
	ofColor tmpColor;
	switch(index){
	case 0:
		tmpColor.set(100,0,0,200);
		break;
	case 1:
		tmpColor.set(0,100,0,200);
		break;
	case 2:
	default:
		tmpColor.set(0,0,100,200);
		break;
	}
	setData(parameter,index,tmpColor);
}

void CFHCurveGraph::setData(ofParameter<float> & parameter,int index,ofColor & color)
{
	if(index < 0 || index > FHCGLINENUM)
		return;
	if((collection.size()-1)/2 > index)
		return;
	m_pValueParams[index]=&parameter;
	add(parameter);
	plColors[index] = color;
	m_bDispData[index] = new ofxToggle(m_bDispDataParameter[index].set(parameter.getName(),true),70);
	ofxBaseGui * element = m_bDispData[index];
	index = index * 2 + 1;
	collection[index]->setFillColor(color);
	collection[index]->setSize(ofxBaseGui::defaultWidth,collection[index]->getHeight());
	if(index<0)index = 0;

	ofPoint lpos = collection[index]->getPosition();
	lpos.x += collection[index]->getWidth();
	collection.push_back( element );

	element->setPosition(lpos);

	//b.height += element->getHeight() + spacing;

	//if(b.width<element->getWidth()) b.width = element->getWidth();

    element->unregisterMouseEvents();

	ofxGuiGroup * subgroup = dynamic_cast<ofxGuiGroup*>(element);
	if(subgroup!=NULL){
		//subgroup->filename = filename;
		//subgroup->parent = this;
		subgroup->setWidthElements(b.width*.98);
	}else{
		if(parent!=NULL){
			element->setSize(b.width*.98,element->getHeight());
			element->setPosition(lpos);
		}
	}

	parameters.add(element->getParameter());
	generateDraw();
}

void CFHCurveGraph::loadIcons(){
	unsigned char minIconData[] = {0x00,0x20,0xE0,0xC0,0xC1,0x87,0x8F,0x3F,0x00};
	unsigned char normalIconData[] = {0x00,0xFC,0xF1,0xE1,0x83,0x03,0x07,0x04,0x00};
	unsigned char maxIconData[] = {0xF8,0xF1,0x23,0xFC,0xF9,0x33,0x7C,0x88,0x1F};
	//unsigned char minmaxIconData[] = {0x1C,0x44,0x0C,0x19,0x00,0x2E,0x98,0x28,0x0E};
	//unsigned char minmaxIconData[] = {0xbB,0x00,0xbB,0x00,0xbb,0x00,0xbb,0x00,0xbb};
	normalIcon.allocate(9, 8, OF_IMAGE_COLOR_ALPHA);
	minIcon.allocate(9, 8, OF_IMAGE_COLOR_ALPHA);
	maxIcon.allocate(9,8,OF_IMAGE_COLOR_ALPHA);
	loadStencilFromHex(normalIcon, normalIconData);
	loadStencilFromHex(minIcon, minIconData);
	loadStencilFromHex(maxIcon,maxIconData);

	maxIcon.getTextureReference().setTextureMinMagFilter(GL_NEAREST,GL_NEAREST);
	minIcon.getTextureReference().setTextureMinMagFilter(GL_NEAREST,GL_NEAREST);
	normalIcon.getTextureReference().setTextureMinMagFilter(GL_NEAREST,GL_NEAREST);
}

void CFHCurveGraph::updatePolylines()
{
	int i = 0;
	float vymm = -1000.0f;
	float vynm = 1000.0f;
	if(m_bSymmatic){
		for(i = 0 ;i < FHCGLINENUM;i++){
			polyline[i].clear();
			if(!m_bDispDataParameter[i])
				continue;
			float vymax = -1000.0f;
			float vymin = 1000.0f;
			float tvalue = 0.0f;
			std::vector<std::pair<float,float> >::iterator iter = pldata[i].begin();
			while(iter != pldata[i].end()){
				if(m_dispLatestTime - iter->first > m_fDispSeconds * 1000){
					iter = pldata[i].erase(iter);
				}
				else{
					if(vymax<iter->second)
						vymax = iter->second;
					if(vymin > iter->second)
						vymin = iter->second;
					iter++;
				}
			}
			float vymaxmin = fabsf(vymax)>fabsf(vymin)?fabsf(vymax):fabsf(vymin);
			if(vymaxmin < 0.000000001f){
				vymaxmin = 0.000000001f;
			}
			else if(vymaxmin < 1.0f){
				int j = 0 ;
				tvalue = vymaxmin;
				while(tvalue<1.0f){
					tvalue*=10.0f;
					j++;
				}
				tvalue = 1.0f;
				while(--j>0){
					tvalue /= 10.0f;
				}
			}
			else{
				int j = 0;
				tvalue = vymaxmin;
				while(tvalue>1.0f){
					tvalue /= 10.0f;
					j++;
				}
				tvalue = 1.0f;
				while(j-->0){
					tvalue *= 10.0f;
				}
			}
			if(tvalue / vymaxmin >= 5.0f){
				tvalue /= 5.0f;
			}
			if(vymm < tvalue){
				vymm = tvalue;
			}
		}
		if(vymm < 0.0000001f)
			vymm = 0.001f;
		vynm = - vymm;
		//update x axis string
		for(i = -5 ;i< 6;i++){
			std::stringstream ostr;
			ostr <<  - vymm / 5  * i;
			mxlabels[i+5] = ostr.str();
		}
	}
	else{
		float valuespan = 0;
		for(i = 0 ;i < FHCGLINENUM;i++){
			polyline[i].clear();
			if(!m_bDispDataParameter[i])
				continue;
			float vymax = -1000.0f;
			float vymin = 1000.0f;
			std::vector<std::pair<float,float> >::iterator iter = pldata[i].begin();
			while(iter != pldata[i].end()){
				if(m_dispLatestTime - iter->first > m_fDispSeconds * 1000){
					iter = pldata[i].erase(iter);
				}
				else{
					if(vymax<iter->second)
						vymax = iter->second;
					if(vymin > iter->second)
						vymin = iter->second;
					iter++;
				}
			}
			valuespan = (vymax - vymin) * 0.1;
			if(valuespan < 0.0000001f)
				valuespan = 0.001f;
			if(vynm > vymin - valuespan)
				vynm = vymin - valuespan ;
			if(vymm < vymax + valuespan)
				vymm = vymax + valuespan ;
		}
		//update x axis string
		valuespan = (vymm + vynm) / 2.0f;
		for(i = -5 ;i< 6;i++){
			std::stringstream ostr;
			ostr <<  valuespan - (vymm - vynm) / 10  * i;
			mxlabels[i+5] = ostr.str();
		}
	}
	
	for(i = 0 ;i < FHCGLINENUM;i++){
		std::vector<std::pair<float,float> >::iterator iter = pldata[i].begin();
		while(iter != pldata[i].end()){
			float vx,vy;
			vx = ofMap(iter->first,m_dispLatestTime - m_fDispSeconds * 1000,m_dispLatestTime,paintboardBox.x,paintboardBox.x+paintboardBox.width,true);
			vy = ofMap(iter->second,vynm,vymm,paintboardBox.y + paintboardBox.height,paintboardBox.y,true);
			//printf("%f %f %f\n",iter->second,vynm,vymm);
			polyline[i].addVertex(vx,vy);
			iter++;
		}

	}
}

void CFHCurveGraph::generateDraw(){
	border.clear();
	border.setStrokeColor(thisBorderColor);
	border.setStrokeWidth(1);
	border.setFilled(false);
	if(m_eGUIMinMaxMode!= FHMIN)
		border.rectangle(b.x,b.y,b.width+1,b.height-spacingNextElement+m_fNormalHeight);
	else
		border.rectangle(b.x,b.y,b.width+1,m_fMinHeight);
	headerBg.clear();
	headerBg.setFillColor(ofColor(thisHeaderBackgroundColor,180));
	headerBg.setFilled(true);
	headerBg.rectangle(b.x,b.y+1,b.width,header);

	float iconHeight = header*.5;
	float iconWidth = maxIcon.getWidth()/maxIcon.getHeight()*iconHeight;
	int iconSpacing = iconWidth*.5;

	btnNormalBox.x = b.getMaxX() - (iconWidth * 2 + iconSpacing + textPadding);
	btnNormalBox.y = b.y + header / 2. - iconHeight / 2.;
	btnNormalBox.width = iconWidth;
	btnNormalBox.height = iconHeight;
	btnMinBox.set(btnNormalBox);
	btnMinBox.x += iconWidth + iconSpacing;
	btnMaxBox.set(btnNormalBox);
	btnMaxBox.x -= iconWidth + iconSpacing;

	textMesh = getTextMesh(getName(), textPadding + b.x, header / 2 + 4 + b.y);
	if(m_eGUIMinMaxMode!= FHMIN){
		m_fDispSeconds = getWidth() / PIXELS_PER_SECOND;
		ofPoint luP;
		luP.x = b.x +1;

		luP.y = b.y + header + collection.size()/2* ofxBaseGui::defaultHeight + (collection.size()/2 -1) * 2;
		paintboardBox.set(luP,getWidth()-2,m_fNormalHeight);
		m_fXAxisPos = paintboardBox.y + paintboardBox.height / 2;

		graphyBG.clear();
		graphyBG.rectangle(paintboardBox);
		graphyBG.setFillColor(ofColor(85,85,85,85));
		graphyBG.setFilled(true);

		xMasterAxis.setStrokeColor(ofColor(0,0,0,255));
		xMasterAxis.setFilled(false);
		xMasterAxis.setStrokeWidth(3);

		xSlaveAxis.setStrokeWidth(1);
		xSlaveAxis.setStrokeColor(ofColor(0,128,128,128));
		xSlaveAxis.setFilled(false);

		yAxis.setStrokeWidth(1);
		yAxis.setStrokeColor(ofColor(128,0,128,128));
		yAxis.setFilled(false);

		yAxis.clear();
		for(int i = b.x + getWidth() - 1 ;i> b.x ;i-=100){
			yAxis.moveTo(i,paintboardBox.y);
			yAxis.lineTo(i,paintboardBox.y+paintboardBox.height);
		}

		xMasterAxis.clear();
		xMasterAxis.moveTo(paintboardBox.x,m_fXAxisPos);
		xMasterAxis.lineTo(paintboardBox.x + paintboardBox.width,m_fXAxisPos);

		mxLabelPos[5].x = paintboardBox.x + paintboardBox.width-80;
		mxLabelPos[5].y = m_fXAxisPos;

		xSlaveAxis.clear();

		//update x axis label pos
		float linespan = m_fNormalHeight / 10;
		for(int i = -5;i<6;++i){
			float thinlineypos = m_fXAxisPos + i * linespan;
			if(i==0)continue;
			xSlaveAxis.moveTo(paintboardBox.x,thinlineypos);
			xSlaveAxis.lineTo(paintboardBox.x + paintboardBox.width,thinlineypos);
			mxLabelPos[i+5].x = paintboardBox.x + paintboardBox.width-80;
			mxLabelPos[i+5].y = thinlineypos;
		}

		updatePolylines();
	}
}

void CFHCurveGraph::render(){
	border.draw();
	headerBg.draw();


	ofBlendMode blendMode = ofGetStyle().blendingMode;
	if(blendMode!=OF_BLENDMODE_ALPHA){
		ofEnableAlphaBlending();
	}
	ofColor c = ofGetStyle().color;
	ofSetColor(thisTextColor);

	bindFontTexture();
	textMesh.draw();
	unbindFontTexture();

	bool texHackEnabled = ofIsTextureEdgeHackEnabled();
	ofDisableTextureEdgeHack();
	normalIcon.draw(btnNormalBox);
	minIcon.draw(btnMinBox);
	maxIcon.draw(btnMaxBox);

	if(m_eGUIMinMaxMode != FHMIN)
	{
		if(texHackEnabled){
			ofEnableTextureEdgeHack();
		}
		graphyBG.draw();
		xMasterAxis.draw();
		xSlaveAxis.draw();
		yAxis.draw();
		float orilinewidth = ofStyle().lineWidth;
		ofSetLineWidth(2);
		for(int i = 0 ;i<FHCGLINENUM;i++){
			if(!m_bDispDataParameter[i])
				continue;
			ofSetColor(plColors[i]);
			polyline[i].draw();
		}
		ofSetLineWidth(orilinewidth);
		for(int i= 0 ;i<11;i++){
			ofDrawBitmapString(mxlabels[i],mxLabelPos[i]);
		}

		for(int i = 0; i < (int)collection.size(); i++){
			collection[i]->draw();
		}
	}
	ofSetColor(c);
	if(blendMode!=OF_BLENDMODE_ALPHA){
		ofEnableBlendMode(blendMode);
	}
}

bool CFHCurveGraph::mouseReleased(ofMouseEventArgs & args){
    this->bGrabbed = false;
    if(ofxGuiGroup::mouseReleased(args)) return true;
    if(isGuiDrawing() && b.inside(ofPoint(args.x,args.y))){
    	return true;
    }else{
    	return false;
    }
}


bool CFHCurveGraph::setValue(float mx, float my, bool bCheck){

	if( !isGuiDrawing() ){
		bGrabbed = false;
		bGuiActive = false;
		return false;
	}
	if( bCheck ){
		if( b.inside(mx, my) ){
			bGuiActive = true;

			if( my > b.y && my <= b.y + header ){
				bGrabbed = true;
				grabPt.set(mx-b.x, my-b.y);
			} else{
				bGrabbed = false;
			}

			if(btnNormalBox.inside(mx, my)) {
				//loadFromFile(filename);
				//ofNotifyEvent(loadPressedE,this);
				if(m_eGUIMinMaxMode!=FHNORMAL){
					m_eGUIMinMaxMode = FHNORMAL;
					checkGUIMinMax();
				}
				return true;
			}
			if(btnMinBox.inside(mx, my)) {
				if(m_eGUIMinMaxMode!= FHMIN){
					m_eGUIMinMaxMode = FHMIN;
					checkGUIMinMax();
				}
				return true;
			}
			if(btnMaxBox.inside(mx,my)){
				if(m_eGUIMinMaxMode!= FHMIN)
					changeGUINormalMax();
				//ofNotifyEvent(maxPressedE,this);
			}
		}
	} else if( bGrabbed ){
		setPosition(mx - grabPt.x,my - grabPt.y);
		return true;
	}
	return false;
}

void CFHCurveGraph::checkGUIMinMax()
{
	switch(m_eGUIMinMaxMode){
	case FHNORMAL:
		bGuiActive=true;
		setSize(getWidth(),m_fLastHeight);
	case FHMAX:
		break;
	case FHMIN:
		m_fLastHeight = getHeight();
		setSize(getWidth(),m_fMinHeight);
		break;
	}
}

void CFHCurveGraph::changeGUINormalMax()
{
	switch(m_eGUIMinMaxMode){
	case FHNORMAL:
		setSize(ofGetWindowWidth(),getHeight());
		m_fLastX = getPosition().x;
		setPosition(0,getPosition().y);
		m_eGUIMinMaxMode = FHMAX;
		break;
	case FHMAX:
		setPosition(m_fLastX,getPosition().y);
		setSize(m_fNormalWidth,getHeight());
		m_eGUIMinMaxMode = FHNORMAL;
		break;
	case FHMIN:
		break;
	}
}

CFHCurveGraph & CFHCurveGraph::operator << (std::pair<float,float> data)
{
	if(!m_bDispDataParameter[m_curplindex])
		return *this;
	pldata[m_curplindex].push_back(data);
	m_dispLatestTime = data.first;
	updatePolylines();
	m_pValueParams[m_curplindex]->set(data.second);
	return *this;
}

CFHCurveGraph & CFHCurveGraph::operator << (int index)
{
	if(index<0 || index > FHCGLINENUM-1)
		return *this;
	m_curplindex = index;
	return *this;
}
