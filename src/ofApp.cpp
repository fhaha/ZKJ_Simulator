#include "ofApp.h"
#include "ofColor.h"

#include "invmath.h"
#include "../../GPS_Simulator/src/gpspkg.h"
#include "InitFWThread.h"

#include "comhead.h"
#include "ofNoise.h"

#if defined(TARGET_OSX) || defined(TARGET_LINUX)
#define _isnan isnan
static int _finite(double x) { return !isnan(x) && isnan(x - x); }
#endif


#define USE_FIBB

#define SQUAR(num)	((num)*(num))
#define SIN		sinf
#define COS		cosf
#define TAN		tanf
#define ASIN	asinf
#define ATAN	atanf
#define SQRT	sqrtf

static float r1value = 0.0f;
static float r2value = 0.0f;
static float r3value = 0.0f;


void ofApp::exit()
{
	m_FWConfig.stopThread();
	
	m_ZKJSim.stopThread();
	m_ZKJSim.quit();
	m_Motor.stopThread();
	m_Motor.quit();
    motorPanelDeinit();
    ofRemoveListener(GravityCheckBtn.btnEvent,this,&ofApp::GravityCheckPressed);
	ofRemoveListener(btnCalcAV.btnEvent,this,&ofApp::CalcAVPressed);
	ofRemoveListener(btnAC.btnEvent,this,&ofApp::ACPressed);
    ofRemoveListener(btnZD.btnEvent,this,&ofApp::ZDPressed);
    ofRemoveListener(PostureCheckBtn.btnEvent,this,&ofApp::PostureCheckPressed);
    ofRemoveListener(btnInitPos.btnEvent,this,&ofApp::InitPostureCheckPressed);
	ofRemoveListener(btnStartAccelCali.btnEvent,this,&ofApp::StartAccelCali);
	ofRemoveListener(btnOriAccel.btnEvent,this,&ofApp::OriAccelMode);
	ofRemoveListener(btnIdle.btnEvent,this,&ofApp::IdleMode);
	ofRemoveListener(btnCar.btnEvent,this,&ofApp::CarPressed);
	ofRemoveListener(btnCap.btnEvent,this,&ofApp::CapturePressed);
	ofRemoveListener(btnStop.btnEvent,this,&ofApp::StopPressed);
	ofRemoveListener(sendData.btnEvent,this,&ofApp::SendDataPressed);
	ofRemoveListener(btnBurnFW.btnEvent,this,&ofApp::BurnFWPressed);
	ofRemoveListener(btnAdjust.btnEvent,this,&ofApp::HFAdjustPressed);
	ofRemoveListener(GA5Btn.btnEvent,this,&ofApp::GA5Pressed);
	ofRemoveListener(GA10Btn.btnEvent,this,&ofApp::GA10Pressed);
	ofRemoveListener(GA20Btn.btnEvent,this,&ofApp::GA20Pressed);
	ofRemoveListener(btnDynG.btnEvent,this,&ofApp::DynGPressed);
	
	if(lcfile.is_open())
		lcfile.close();
	m_FWConfig.waitForThread();
	m_ZKJSim.waitForThread();
	m_Motor.waitForThread();
}
//--------------------------------------------------------------
void ofApp::setup(){
	int i ,j;
	m_getLVn=m_getLVe=m_getLVu=0.0f;
	for(i=0;i<3;i++){
		for(j=0;j<3;j++){
			if(i==j){
				m_fa[i][j]=0.0f;
				m_wa[i][j]=0.0f;
			}
			else{
				m_fa[i][j]=S_PI/2.0f;
				m_wa[i][j]=S_PI/2.0f;
			}
		}
	}
	m_bDemo =false;
	m_GPSDataCount = 0;
	notCalc=canCalc=totalCalc=notNiuton = 0;
    m_fPsi = 0.0f;
    m_fTheta = 0.0f;
    m_fGamma = 0.0f;
	m_fPsiP = m_fThetaP = m_fGammaP = 0.0f;
	m_bZDEnabled = false;
	int width = ofGetWindowWidth();
	int height = ofGetWindowHeight();

	loadConfig();
	//cout << "ADAssStep:" << m_Motor.ADASSSTEP << " TunningStep: " << m_Motor.TUNNINGSTEP << " MicroStep: " << m_Motor.MICROSTEP;
	udpConnection.Create();
    udpConnection.SetEnableBroadcast(true);
	udpConnection.Bind(11999);

	udpConnection.SetNonBlocking(true);
	ofSetLogLevel("ZKJ", OF_LOG_VERBOSE);
	ofSetLogLevel("GPS", OF_LOG_VERBOSE);
	if(m_szLogFile.empty())
		ofLogToConsole(); //set channel to console
	else
		ofLogToFile(m_szLogFile,true);

	m_ModelPlane.loadModel(m_szPlanFilename);
    m_ModelPlaneP=m_ModelPlane;
    m_ModelPlaneL = m_ModelPlaneP;
	planeResize(width,height);
	
	zwFont.loadFont(m_sFontName,fontsize,true,true);
	titleFont.loadFont("FZSTK.TTF",16,true,true);
	btnFont.loadFont("STHUPO.TTF",14,true,true);
	//Posture Panel
	PosturePanelInit(width,height);

	//Gravity Check
	DeltaConfigInit(width,height);

	//Sensor Data Panel Init
	SensorDataPanelInit(width,height);

	//Calculated Data Panel Init
	CalcDataPanelInit(width,height);

    m_LogWindow.setup("LogWindow",width/2);

	m_FoibbCurve.setup("FoibbCurve");
	m_FoibbCurve.setData(foibbx.set("Foibbx",0.0f,-2.5f,2.5f),0);
	m_FoibbCurve.setData(foibby.set("Foibby",0.0f,-2.5f,2.5f),1);
	m_FoibbCurve.setData(foibbz.set("Foibbz",0.0f,-2.5f,2.5f),2);

#ifdef USE_FIBB
	m_FibbCurve.setup("FibbCurve");
	m_FibbCurve.setData(fibbx.set("Fibbx",0.0f,-50.0f,50.0f),0);
	m_FibbCurve.setData(fibby.set("Fibby",0.0f,-50.0f,50.0f),1);
	m_FibbCurve.setData(fibbz.set("Fibbz",0.0f,-50.0f,50.0f),2);

#endif
	m_FmibbCurve.setup("MeanFibb");
	m_FmibbCurve.setData(fmibbx.set("mFibbx",0.0f,-2.5f,2.5f),0);
	m_FmibbCurve.setData(fmibby.set("mFibby",0.0f,-2.5f,2.5f),1);
	m_FmibbCurve.setData(fmibbz.set("mFibbz",0.0f,-2.5f,2.5f),2);

	m_WpibbCurve.setup("WpibbCurve");
	m_WpibbCurve.setData(wpibbx.set("Wibbx",0.0f,-5.0 * G1,5.0*G1),0);
	m_WpibbCurve.setData(wpibby.set("Wibby",0.0f,-5.0 * G1,5.0*G1),1);
	m_WpibbCurve.setData(wpibbz.set("Wibbz",0.0f,-5.0 * G1,5.0*G1),2);

	m_WibbCurve.setup("WibbCurve");
	m_WibbCurve.setData(wibbx.set("Wibbx",0.0f,-5.0 * G1,5.0*G1),0);
	m_WibbCurve.setData(wibby.set("Wibby",0.0f,-5.0 * G1,5.0*G1),1);
	m_WibbCurve.setData(wibbz.set("Wibbz",0.0f,-5.0 * G1,5.0*G1),2);

	m_AeggCurve.setup("Aegg");
	m_AeggCurve.setData(m_fAegg[0].set("AeggX",0.0f,-1000.0f,1000.0f),0);
	m_AeggCurve.setData(m_fAegg[1].set("AeggY",0.0f,-1000.0f,1000.0f),1);
	m_AeggCurve.setData(m_fAegg[2].set("AeggZ",0.0f,-1000.0f,1000.0f),2);

	m_VeggCurve.setup("Vegg");
	m_VeggCurve.setData(m_fVegg[0].set("VeggX",0.0f,-1000.0f,1000.0f),0);
	m_VeggCurve.setData(m_fVegg[1].set("VeggY",0.0f,-1000.0f,1000.0f),1);
	m_VeggCurve.setData(m_fVegg[2].set("VeggZ",0.0f,-1000.0f,1000.0f),2);

	m_GAeggCurve.setup("GAegg");
	m_GAeggCurve.setData(m_fGAegg[0].set("GAeggX",0.0f,-1000.0f,1000.0f),0);
	m_GAeggCurve.setData(m_fGAegg[1].set("GAeggY",0.0f,-1000.0f,1000.0f),1);
	m_GAeggCurve.setData(m_fGAegg[2].set("GAeggZ",0.0f,-1000.0f,1000.0f),2);

	m_GVeggCurve.setup("GVegg");
	m_GVeggCurve.setData(m_fGVegg[0].set("GVeggX",0.0f,-1000.0f,1000.0f),0);
	m_GVeggCurve.setData(m_fGVegg[1].set("GVeggY",0.0f,-1000.0f,1000.0f),1);
	m_GVeggCurve.setData(m_fGVegg[2].set("GVeggZ",0.0f,-1000.0f,1000.0f),2);

	m_GammaCurve.setup("Gamma");
	m_GammaCurve.setData(m_GCGamma.set("GraGa",0.0f,-180.0f,180.0f),0);
	m_GammaCurve.setData(m_PosGamma.set("PosGa",0.0f,-180.0f,180.0f),1);
	m_GammaCurve.setData(m_DeltaGamma.set("DeltaGa",0.0f,-360.0f,360.0f),2);

	m_CustomCurve.setup("Custom");
	m_CustomCurve.setData(m_calcG.set("G",0.0f,-10.0f,10.0f),0);

	m_WibbCurve.minimize();
	m_WpibbCurve.minimize();
	m_FibbCurve.minimize();
	m_FmibbCurve.minimize();
	m_FoibbCurve.minimize();
	m_AeggCurve.minimize();
	m_VeggCurve.minimize();
	m_GAeggCurve.minimize();
	m_GVeggCurve.minimize();
	m_GammaCurve.minimize();
	m_CustomCurve.minimize();

	FibbCurveResize(width,height);
	WibbCurveResize(width,height);
    logWindowResize(width,height);
	btnZD.setup("ZhuangDing");
	GravityCheckBtn.setup("GrevityCheck");
	btnCalcAV.setup("CalvAV");
	btnAC.setup("AC");
	PostureCheckBtn.setup("PostureCheck");
	btnInitPos.setup("InitPos");

	
	btnPanel.setFont(&titleFont);
	btnPanel.setName(L"控制面板");
	wstring tlabel = L"开始计算";
	btnPanel.addButton(btnCar.setup(tlabel,&btnFont));
	
	tlabel = L"停止计算";
	btnPanel.addButton(btnStop.setup(tlabel,&btnFont));
	tlabel = L"发送数据";
	btnPanel.addButton(sendData.setup(tlabel,&btnFont,L"停止发送"));
	tlabel = L"启用修正";
	btnPanel.addButton(btnAdjust.setup(tlabel,&btnFont,L"取消修正"));
	tlabel = L"修正５Ｓ";
	btnPanel.addButton(GA5Btn.setup(tlabel,&btnFont));
	tlabel = L"修正10Ｓ";
	btnPanel.addButton(GA10Btn.setup(tlabel,&btnFont));
	tlabel = L"修正20Ｓ";
	btnPanel.addButton(GA20Btn.setup(tlabel,&btnFont));
	tlabel = L"修正加表";
	btnPanel.addButton(btnStartAccelCali.setup(tlabel,&btnFont));
	tlabel = L"纯加模式";
	btnPanel.addButton(btnOriAccel.setup(tlabel,&btnFont));
	tlabel = L"IDLE模式";
	btnPanel.addButton(btnIdle.setup(tlabel,&btnFont));
	tlabel = L"动态Ｇｎ";
	btnPanel.addButton(btnDynG.setup(tlabel,&btnFont,L"固定Ｇｎ"));
	

	btnCap.setup("Capture");
	btnBurnFW.setup("BurnFW");

	ofAddListener(GravityCheckBtn.btnEvent,this,&ofApp::GravityCheckPressed);
	ofAddListener(btnCalcAV.btnEvent,this,&ofApp::CalcAVPressed);
	ofAddListener(btnAC.btnEvent,this,&ofApp::ACPressed);
	ofAddListener(btnZD.btnEvent,this,&ofApp::ZDPressed);
	ofAddListener(PostureCheckBtn.btnEvent,this,&ofApp::PostureCheckPressed);
	ofAddListener(btnStartAccelCali.btnEvent,this,&ofApp::StartAccelCali);
	ofAddListener(btnOriAccel.btnEvent,this,&ofApp::OriAccelMode);
	ofAddListener(btnIdle.btnEvent,this,&ofApp::IdleMode);

	ofAddListener(btnInitPos.btnEvent,this,&ofApp::InitPostureCheckPressed);
	ofAddListener(btnCar.btnEvent,this,&ofApp::CarPressed);
	ofAddListener(btnCap.btnEvent,this,&ofApp::CapturePressed);
	ofAddListener(btnStop.btnEvent,this,&ofApp::StopPressed);
	ofAddListener(sendData.btnEvent,this,&ofApp::SendDataPressed);
	ofAddListener(btnBurnFW.btnEvent,this,&ofApp::BurnFWPressed);
	ofAddListener(btnAdjust.btnEvent,this,&ofApp::HFAdjustPressed);
	ofAddListener(GA5Btn.btnEvent,this,&ofApp::GA5Pressed);
	ofAddListener(GA10Btn.btnEvent,this,&ofApp::GA10Pressed);
	ofAddListener(GA20Btn.btnEvent,this,&ofApp::GA20Pressed);
	ofAddListener(btnDynG.btnEvent,this,&ofApp::DynGPressed);

    motorPanelInit(width,height);
	btnZD.disable();
	btnInitPos.disable();
	//btnCar.disable();
	buttonsResize(width,height);
    lightResize(width,height);
    for(int i = 0 ;i<LIGHTNUM;++i){
        light[i].enable();
    }
	m_FWConfig.startThread();
	m_bGuiInited = false;
	
	m_ZKJSim.setup(m_szZKJSerialName,m_iZKJBaud);
	m_GPSSerial.setup(m_szGPSSerialName,115200);
    m_Motor.setup(m_szMotorSerialName,115200);
}

void ofApp::writeFWCfg(fwcfg & cfg)
{
	m_ZKJSim<<cfg;
}

void ofApp::buttonsResize(int w,int h)
{
	//btnCar.move(w-btnCar.getWidth() - 10,10);
	//btnAdjust.move(w-btnAdjust.getWidth() - 10,btnCar.getY() + btnCar.getHeight() + 5);
	//GA5Btn.move(w-GA5Btn.getWidth() - 10,btnAdjust.getY() + btnAdjust.getHeight() + 5);
	//GA10Btn.move(w-GA10Btn.getWidth() - 10,GA5Btn.getY() + GA5Btn.getHeight() + 5);
	//GA20Btn.move(w-GA20Btn.getWidth() - 10,GA10Btn.getY() + GA10Btn.getHeight() + 5);
	//btnStop.move(w-btnStop.getWidth() - 10,GA20Btn.getY() + GA20Btn.getHeight() + 5);
	//sendData.move(w-sendData.getWidth()-10,btnStop.getY() + btnStop.getHeight() + 5);
	//btnZD.move(w-btnZD.getWidth()-10,sendData.getY() + sendData.getHeight() + 5);
	//btnDynG.move(w-btnDynG.getWidth()-10,btnZD.getY() + btnDynG.getHeight() + 5);
	//ofPoint curp ;
	//curp.x = btnZD.getX();
	//curp.y = btnZD.getY()+btnZD.getHeight()+5;
	GravityCheckBtn.move(w-GravityCheckBtn.getWidth()-10,10);
	btnCalcAV.move(w-btnCalcAV.getWidth()-10,GravityCheckBtn.getY()+GravityCheckBtn.getHeight() + 5);
	btnAC.move(w-btnAC.getWidth()-10,btnCalcAV.getY()+btnCalcAV.getHeight()+5);
	//curp.y  += GravityCheckBtn.getHeight() + 5;
	PostureCheckBtn.move(w-PostureCheckBtn.getWidth()-10,btnAC.getY()+btnAC.getHeight()+5);
	//curp.y += PostureCheckBtn.getHeight() + 5;
	btnInitPos.move(w-btnInitPos.getWidth()-10,PostureCheckBtn.getY()+PostureCheckBtn.getHeight()+5);

	btnCap.move(w-btnCap.getWidth() - 10,btnInitPos.getY() + btnInitPos.getHeight() + 5);
	btnBurnFW.move(w-btnBurnFW.getWidth() - 10,btnCap.getY() + btnCap.getHeight() + 5);
	
}

void ofApp::motorPanelResize(int w , int h)
{
    ofPoint calcpos = m_CalcDataPanel.getPosition();
    motorPanel.setPosition(calcpos.x +  m_CalcDataPanel.getWidth()+5,calcpos.y);
}

void ofApp::motorPanelInit(int w ,int h)
{
    motorPanel.setup("Motor");
	motorPanel.add(m_bAdjustMax.set("MAX/EY",false));
    motorPanel.add(btnAXAdjust.setup("AXAdjust"));
    motorPanel.add(btnAYAdjust.setup("AYAdjust"));
    motorPanel.add(btnAZAdjust.setup("AZAdjust"));
    motorPanel.add(btnGXAdjust.setup("GXAdjust"));
    motorPanel.add(btnGYAdjust.setup("GYAdjust"));
    motorPanel.add(btnGZAdjust.setup("GZAdjust"));
    motorPanel.add(btnFar.setup("Far"));
    motorPanel.add(btnNear.setup("Near"));
	motorPanel.add(btnXAdjust.setup("XMotor"));
	motorPanel.add(btnYAdjust.setup("YMotor"));
    motorPanel.setSize(motorPanel.getWidth()/2,motorPanel.getHeight());
    motorPanel.setWidthElements(motorPanel.getWidth());
    btnAXAdjust.addListener(this, &ofApp::AXAdjustFunc);
    btnAYAdjust.addListener(this, &ofApp::AYAdjustFunc);
    btnAZAdjust.addListener(this, &ofApp::AZAdjustFunc);
    btnGXAdjust.addListener(this, &ofApp::GXAdjustFunc);
    btnGYAdjust.addListener(this, &ofApp::GYAdjustFunc);
    btnGZAdjust.addListener(this, &ofApp::GZAdjustFunc);
    btnFar.addListener(this, &ofApp::FarFunc);
    btnNear.addListener(this, &ofApp::NearFunc);
	btnXAdjust.addListener(this,&ofApp::XAdjustFunc);
	btnYAdjust.addListener(this,&ofApp::YAdjustFunc);
    motorPanelResize(w,h);
}

void ofApp::motorPanelDeinit()
{
    btnAXAdjust.removeListener(this, &ofApp::AXAdjustFunc);
    btnAYAdjust.removeListener(this, &ofApp::AYAdjustFunc);
    btnAZAdjust.removeListener(this, &ofApp::AZAdjustFunc);
    btnGXAdjust.removeListener(this, &ofApp::GXAdjustFunc);
    btnGYAdjust.removeListener(this, &ofApp::GYAdjustFunc);
    btnGZAdjust.removeListener(this, &ofApp::GZAdjustFunc);
    btnFar.removeListener(this, &ofApp::FarFunc);
    btnNear.removeListener(this, &ofApp::NearFunc);

	btnXAdjust.removeListener(this,&ofApp::XAdjustFunc);
	btnYAdjust.removeListener(this,&ofApp::YAdjustFunc);
}

void ofApp::AXAdjustFunc()
{
    m_LogWindow << "start adjust accelerator x" << LOGNL;
	m_Motor.StartAdjust(AXADJ);
}

void ofApp::AYAdjustFunc()
{
    m_LogWindow << "start adjust accelerator y" << LOGNL;
    m_Motor.StartAdjust(AYADJ);
}


void ofApp::AZAdjustFunc()
{
	m_LogWindow << "start adjust accelerator z" << LOGNL;
	m_Motor.StartAdjust(AZADJ);
}

void ofApp::GXAdjustFunc()
{
    m_LogWindow << "start adjust groy x" << LOGNL;
    m_Motor.StartAdjust(GXADJ);
}

void ofApp::GYAdjustFunc()
{
    m_LogWindow << "start adjust groy y" << LOGNL;
    m_Motor.StartAdjust(GYADJ);
}
void ofApp::GZAdjustFunc()
{
    m_LogWindow << "start adjust groy z" << LOGNL;
    m_Motor.StartAdjust(GZADJ);
}

void ofApp::NearFunc()
{
    string inputstring = ofSystemTextBoxDialog("near:millimeters","10.0");
    if(!isdigit(inputstring[0])){
        m_LogWindow<< "input not digital" << '\n';
        return;
    }

    std::stringstream tstr(inputstring);
    float inputsize =0.0f;
    tstr >> inputsize;
    if(is_zero(inputsize)){
        return;
    }
	m_Motor.SendNear((int)(inputsize * 200 / 6));
    m_LogWindow << "want change near " << inputsize << "mm" << '\n';
}

void ofApp::XAdjustFunc()
{
	string inputstring = ofSystemTextBoxDialog("x:steps","0");
    if(!isdigit(inputstring[0]) && inputstring[0] != '-'){
        m_LogWindow<< "input not digital" << '\n';

        return;
    }
    std::stringstream tstr(inputstring);
    int inputstep=0;
    tstr >> inputstep;
    if(inputstep==0){
        m_LogWindow<<"input Zero" << '\n';
        return;
    }
	m_Motor.SendXAdjust(inputstep,0);
    m_LogWindow << "want move x axis " << inputstep << "step" << '\n';
}

void ofApp::YAdjustFunc()
{
	string inputstring = ofSystemTextBoxDialog("y:steps","0");
    if(!isdigit(inputstring[0])&& inputstring[0] != '-'){
        m_LogWindow<< "input not digital" << '\n';

        return;
    }
    std::stringstream tstr(inputstring);
    int inputstep=0;
    tstr >> inputstep;
    if(inputstep==0){
        m_LogWindow<<"input Zero" << '\n';
        return;
    }
	m_Motor.SendYAdjust(inputstep,0);
    m_LogWindow << "want move y axis " << inputstep << "step" << '\n';
}

void ofApp::FarFunc()
{
    string inputstring = ofSystemTextBoxDialog("far:millimeters","10.0");
    if(!isdigit(inputstring[0])){
        m_LogWindow<< "input not digital" << '\n';

        return;
    }
    std::stringstream tstr(inputstring);
    float inputsize=0.0f;
    tstr >> inputsize;
    if(is_zero(inputsize)){
        m_LogWindow<<"input Zero" << '\n';
        return;
    }
	m_Motor.SendFar((int)(inputsize * 200 / 6));
    m_LogWindow << "want change far " << inputsize << "mm" << '\n';
}

void ofApp::CalcDataPanelInit(int w,int h)
{
	m_CalcDataPanel.setup("CalcData");

	m_CalcDataPanel.add(Ggc.set("g",9.8f,-5.0f,15.0f));
	m_CalcDataPanel.add(Gngc.set("gn",9.8f,-5.0f,15.0f));
	m_CalcDataPanel.add(figgz.set("figgz",9.8f,-5.0f,15.0f));
	m_CalcDataPanel.add(dg1.set("dg1",0.0f,-G1,G1));
	m_CalcDataPanel.add(dg2.set("dg2",0.0f,-G1,G1));
	m_CalcDataPanel.add(dg3.set("dg3",0.0f,-G1,G1));
#if 0
	m_CalcDataPanel.add(totalCalc.set("TotalCalc",0,0,50000));
	m_CalcDataPanel.add(notCalc.set("notCalc",0,0,50000));
	m_CalcDataPanel.add(canCalc.set("canCalc",0,0,50000));
	m_CalcDataPanel.add(notNiuton.set("notNiuton",0,0,50000));

	Ggc.set("g",9.8f,-5.0f,15.0f);
	Gngc.set("gn",9.8f,-5.0f,15.0f);
	figgz.set("figgz",9.8f,-5.0f,15.0f);
	dg1.set("dg1",0.0f,-G1,G1);
	dg2.set("dg2",0.0f,-G1,G1);
	dg3.set("dg3",0.0f,-G1,G1);
#else

	totalCalc.set("TotalCalc",0,0,50000);
	notCalc.set("notCalc",0,0,50000);
	canCalc.set("canCalc",0,0,50000);
	notNiuton.set("notNiuton",0,0,50000);
#endif

	m_ComparePanel.setup("CalcCompare");
	m_ComparePanel.add(m_lFibbx.set("LFibbx",0.0f,-100.0f,100.0f));
	m_ComparePanel.add(m_rFibbx.set("RFibbx",0.0f,-100.0f,100.0f));
	m_ComparePanel.add(m_lFibby.set("LFibby",0.0f,-100.0f,100.0f));
	m_ComparePanel.add(m_rFibby.set("RFibby",0.0f,-100.0f,100.0f));
	m_ComparePanel.add(m_lFibbz.set("LFibbz",0.0f,-100.0f,100.0f));
	m_ComparePanel.add(m_rFibbz.set("RFibbz",0.0f,-100.0f,100.0f));
#if 0
	m_CalcDataPanel.add(m_getLVe.set("LVe",0,-20000,20000));
	m_CalcDataPanel.add(m_getVe.set("Ve",0,-20000,20000));
	m_CalcDataPanel.add(m_getLVn.set("LVn",0,-20000,20000));
	m_CalcDataPanel.add(m_getVn.set("Vn",0,-20000,20000));
	m_CalcDataPanel.add(m_getLVu.set("LVu",0,-20000,20000));
	m_CalcDataPanel.add(m_getVu.set("Vu",0,-20000,20000));
	m_CalcDataPanel.add(m_getLVeLast.set("LVe1",0,-20000,20000));
	m_CalcDataPanel.add(m_getVeLast.set("Ve1",0,-20000,20000));
	m_CalcDataPanel.add(m_getLVnLast.set("LVn1",0,-20000,20000));
	m_CalcDataPanel.add(m_getVnLast.set("Vn1",0,-20000,20000));
	m_CalcDataPanel.add(m_getLVuLast.set("LVu1",0,-20000,20000));
	m_CalcDataPanel.add(m_getVuLast.set("Vu1",0,-20000,20000));
#endif
	m_CalcDataPanel.add(m_iGraCircle.set("GC",0,0,2000));
	m_CalcDataPanel.add(m_iPosCircle.set("PC",0,0,2000));
	m_CalcDataPanel.add(m_CircleSpeed.set("CC",0,-100,100));
	CalcDataPanelResize(w,h);
}

void ofApp::CalcDataPanelResize(int w,int h)
{
	m_CalcDataPanel.setPosition(m_SensorDataPanel.getPosition().x + m_SensorDataPanel.getWidth() + 5,10);
}

void ofApp::SensorDataPanelInit(int w,int h)
{
	m_SensorDataPanel.setup("SensorData");

	m_SensorDataPanel.add(m_bDebug.set("Debug",false));
	//for(int k = 0 ;k<4;k++){
	//	char tmpname[24]={0};
	//	sprintf(tmpname,"Temperature%d",k);
	//	m_SensorDataPanel.add(temperature[k].set(tmpname,20.0f,-40.0f,120.0f));
	//}
	m_SensorDataPanel.add(i2ctemp.set("I2CTemp",0.0f,-40.0f,120.0f));
	SensorDataPanelResize(w,h);
}

void ofApp::SensorDataPanelResize(int w,int h)
{
	float xpos = m_DeltaConfigPanel.getPosition().x + m_DeltaConfigPanel.getWidth()+ 5;
	m_SensorDataPanel.setPosition(xpos ,10);

	m_ComparePanel.setPosition(xpos ,m_SensorDataPanel.getPosition().y+m_SensorDataPanel.getHeight() + 10);
}
void ofApp::PosturePanelInit(int w ,int h)
{
	m_PosturePanel.setup("Posture");
	m_PosturePanel.add(m_fPsi.set("PSI",0,0,360));
	m_PosturePanel.add(m_fPsiP.set("PSIP",0,0,360));
	m_PosturePanel.add(m_fPsiL.set("PSIL",0,0,360));
	m_PosturePanel.add(m_fTheta.set("Theta",0,-90.0f,90.0f));
	m_PosturePanel.add(m_fThetaP.set("ThetaP",0,-90.0f,90.0f));
	m_PosturePanel.add(m_fThetaL.set("ThetaL",0,-90.0f,90.0f));
	m_PosturePanel.add(m_fGamma.set("Gamma",0,-180.0f,180.0f));
	m_PosturePanel.add(m_fGammaP.set("GammaP",0,-180.0f,180.0f));
	m_PosturePanel.add(m_fGammaL.set("GammaL",0,-180.0f,180.0f));

	m_CarPanel.setup("Car");
	m_CarPanel.add(m_bRealTime.set("RealTime",true,false,true));
	m_CarPanel.add(m_bGPSData.set("GPSData",true,false,true));
	m_CarPanel.add(m_bFilter.set("Filter",true,false,true));
	m_CarPanel.add(m_bMean.set("Mean",true,false,true));
	m_CarPanel.add(m_bDemo.set("Demo",m_bDemo.get()));
	//m_CarPanel.add(m_bExtraWY.set("ExtraY",true,false,true));
	m_CarPanel.add(m_fLambda.set("Longitude",0.0f,-1000.0f,1000.0f));
	m_CarPanel.add(m_fFai.set("Latitude",0.0f,-1000.0f,1000.0f));
	m_CarPanel.add(m_fH.set("Height",0.0f,-1000.0f,1000.0f));
	 
	m_CarPanel.add(m_fGLambda.set("GLongitude",0.0f,-1000.0f,1000.0f));
	m_CarPanel.add(m_fGFai.set("GLatitude",0.0f,-1000.0f,1000.0f));
	m_CarPanel.add(m_fGH.set("GHeight",0.0f,-1000.0f,1000.0f));
	m_CarPanel.add(m_fDelT.set("DeltaT",0.0f,-0.1f,1.0f));
	m_CarPanel.add(m_fGPSDelT.set("GPSDeltaT",0.0f,0.0f,10.0f));

	PosturePanelResize(w,h);
}

void ofApp::PosturePanelResize(int w,int h)
{
	m_CarPanel.setPosition(w - m_CarPanel.getWidth()-10 ,h - m_CarPanel.getHeight()-10);
	m_PosturePanel.setPosition(m_CarPanel.getPosition().x - m_PosturePanel.getWidth() - 10,h - m_PosturePanel.getHeight()-10);
}
void ofApp::DeltaConfigInit(int w,int h)
{
	m_DeltaConfigPanel.setup("DeltaConfig");
	ofAddListener(m_DeltaConfigPanel.loadPressedE,this,&ofApp::loadGravityListener);
	ofAddListener(m_DeltaConfigPanel.savePressedE,this,&ofApp::saveGravityListener);
	loadGravityListener();
	m_DeltaConfigPanel.add(dfx.set("Delta Fx",dfx,-2.5f,2.5f));
	m_DeltaConfigPanel.add(dfy.set("Delta Fy",dfy,-2.5f,2.5f));
	m_DeltaConfigPanel.add(dfz.set("Delta Fz",dfz,-2.5f,2.5f));
	m_DeltaConfigPanel.add(ffactorx.set("FFactorX",ffactorx,-100.0f,100.0f));
	m_DeltaConfigPanel.add(ffactory.set("FFactorY",ffactory,-100.0f,100.0f));
	m_DeltaConfigPanel.add(ffactorz.set("FFactorZ",ffactorz,-100.0f,100.0f));
	m_DeltaConfigPanel.add(dwx.set("Delta Wx",dwx,-2.5f,2.5f));
	m_DeltaConfigPanel.add(dwy.set("Delta Wy",dwy,-2.5f,2.5f));
	m_DeltaConfigPanel.add(dwz.set("Delta Wz",dwz,-2.5f,2.5f));
	m_DeltaConfigPanel.add(wxfactor.set("WXFactor",wxfactor,-100.0f,100.0f));
	m_DeltaConfigPanel.add(wyfactor.set("WYFactor",wyfactor,-100.0f,100.0f));
	m_DeltaConfigPanel.add(wzfactor.set("WZFactor",wzfactor,-100.0f,100.0f));
	m_DeltaConfigPanel.add(wypfactor.set("WEYFactor",wypfactor,-100.0f,100.0f));
	m_DeltaConfigPanel.add(mgamma.set("ManualGamma",180.0f,0.0f,360.0f));

	GravityPanelResize(w,h);
}

void ofApp::BurnFWPressed(int &eventarg)
{
	ofFileDialogResult openFileResult= ofSystemLoadDialog("Select a deltaconfig file");

	//Check if the user opened a file
	if (openFileResult.bSuccess){

		ofLogVerbose("User selected a file");

		//We have a file, check it and process it
		//processOpenFileSelection(openFileResult);
		std::string filepath = openFileResult.getPath();
		m_FWConfig.loadData(filepath);

	}else {
		ofLogVerbose("User hit cancel");
	}
}

void ofApp::SendDataPressed(int & eventarg)
{
	static int couldsend = 0;
	m_SendDataPkg.phead = COMHEAD;
	m_SendDataPkg.len = sizeof(st_senddata_pkg);
	m_SendDataPkg.id = SDATAID;
	m_SendDataPkg.cmd = couldsend;
	m_ZKJSim.Send(m_SendDataPkg);
	couldsend = !couldsend;
	//wstring label = couldsend?L"停止数据":L"发送数据";
	//sendData.setLabelString(label.c_str(),wcslen(label.c_str()));
	//sendData.setup(label.c_str());
	//sendData.enable();
}

void ofApp::StopPressed(int & eventarg)
{
	m_InitPosPkg.phead = COMHEAD;
	m_InitPosPkg.len = sizeof(initpos_pkg);
	m_InitPosPkg.id = IDLEID;
	m_ZKJSim.Send(m_InitPosPkg);

	if(m_ZKJSim.hasFileOpen()){
		string filename = ofSystemTextBoxDialog("filename",m_ZKJSim.oldfilename);
		m_ZKJSim.Idle(filename);
	}
}

void ofApp::CapturePressed(int & eventarg)
{
	m_InitPosPkg.phead = COMHEAD;
	m_InitPosPkg.len = sizeof(initpos_pkg);
	m_InitPosPkg.id = CAPPKGID;
	m_InitPosPkg.dfx = dfx ;
	m_InitPosPkg.dfy = dfy ;
	m_InitPosPkg.dfz = dfz ;
	m_InitPosPkg.ffactorx = ffactorx;
	m_InitPosPkg.ffactory = ffactory;
	m_InitPosPkg.ffactorz = ffactorz;
	m_InitPosPkg.dwx = dwx;
	m_InitPosPkg.dwy = dwy;
	m_InitPosPkg.dwz = dwz;
	m_InitPosPkg.wxfactor = wxfactor;
	m_InitPosPkg.wyfactor = wyfactor;
	m_InitPosPkg.wypfactor = wypfactor;
	m_InitPosPkg.wzfactor = wzfactor;
	m_InitPosPkg.flag |= m_bFilter?(m_bMean?1:2):(m_bMean?1:0);
	m_ZKJSim.Send(m_InitPosPkg);
}



void ofApp::CarPressed(int & eventarg)
{
	int i = 0 ;
	int j = 0 ;
	carinit_pkg tmpPkg ;
	tmpPkg.phead = COMHEAD;
	tmpPkg.len = sizeof(carinit_pkg);
	tmpPkg.id = CARINITID;
	for(i=0;i<3;i++){
		for(j=0;j<3;j++){
			tmpPkg.fa[i][j] = m_fa[i][j];
			tmpPkg.wa[i][j] = m_wa[i][j];
		}
	}
	tmpPkg.df[0]=dfx;
	tmpPkg.df[1]=dfy;
	tmpPkg.df[2]=dfz;
	tmpPkg.ff[0] = ffactorx;
	tmpPkg.ff[1] = ffactory;
	tmpPkg.ff[2] = ffactorz;
	tmpPkg.dw[0] = dwx;
	tmpPkg.dw[1] = dwy;
	tmpPkg.dw[2] = dwz;
	tmpPkg.dw[3] = 0;
	tmpPkg.wf[0] = wxfactor;
	tmpPkg.wf[1] = wyfactor;
	tmpPkg.wf[2] = wzfactor;
	tmpPkg.wf[3] = wypfactor;
	tmpPkg.psi = m_fPsi / 180 * S_PI;
	tmpPkg.flag = 0;
	tmpPkg.flag |= m_bFilter?(m_bMean?1:2):(m_bMean?1:0);
	tmpPkg.flag |= btnZD.isEnabled()?4:0;
	tmpPkg.flag |= m_bAdjustMax?0x10:0;
	m_ZKJSim.Send(tmpPkg);
	m_iGraCircle=0;
	m_iPosCircle=0;
	m_CircleSpeed= 0;
}

void ofApp::InitPostureCheckPressed(int & eventarg)
{
	m_InitPosPkg.phead = COMHEAD;
	m_InitPosPkg.len = sizeof(initpos_pkg);
	m_InitPosPkg.id = INITPOSID;
	m_InitPosPkg.dfx = dfx ;
	m_InitPosPkg.dfy = dfy ;
	m_InitPosPkg.dfz = dfz ;
	m_InitPosPkg.ffactorx = ffactorx;
	m_InitPosPkg.ffactory = ffactory;
	m_InitPosPkg.ffactorz = ffactorz;
	printf("send InitPos DeltaF %f %f %f\n",m_InitPosPkg.dfx*m_InitPosPkg.ffactorx,m_InitPosPkg.dfy*m_InitPosPkg.ffactory,m_InitPosPkg.dfz*m_InitPosPkg.ffactorz);
	m_InitPosPkg.dwx = dwx;
	m_InitPosPkg.dwy = dwy;
	m_InitPosPkg.dwz = dwz;
	m_InitPosPkg.wxfactor = wxfactor;
	m_InitPosPkg.wyfactor = wyfactor;
	m_InitPosPkg.wypfactor = wypfactor;
	m_InitPosPkg.wzfactor = wzfactor;
	m_ZKJSim.Send(m_InitPosPkg);
}

void ofApp::StartAccelCali(int & eventarg)
{
	m_ZKJSim.startAccelCali();
}
void ofApp::OriAccelMode(int & eventarg)
{
	m_ZKJSim.oriAccelMode();
}

void ofApp::IdleMode(int & eventarg)
{
	m_ZKJSim.IdleMode();
}

void ofApp::PostureCheckPressed(int& eventarg)
{
	pos_pkg tmpPkg;
	tmpPkg.phead = COMHEAD;
	tmpPkg.id = POSID;
	tmpPkg.len = POSPKGLEN;
	tmpPkg.dfx = dfx ;
	tmpPkg.dfy = dfy ;
	tmpPkg.dfz = dfz ;
	tmpPkg.ffactorx = ffactorx;
	tmpPkg.ffactory = ffactory;
	tmpPkg.ffactorz = ffactorz;
	tmpPkg.dwx = dwx;
	tmpPkg.dwy = dwy;
	tmpPkg.dwz = dwz;
	tmpPkg.wxfactor = wxfactor;
	tmpPkg.wyfactor = wyfactor;
	tmpPkg.wzfactor = wzfactor;
	tmpPkg.wypfactor = wypfactor;
	tmpPkg.flag = 0;
	tmpPkg.flag |= m_bFilter?(m_bMean?1:2):(m_bMean?1:0);
	m_iGraCircle=0;
	m_iPosCircle=0;
	m_CircleSpeed= 0;
	m_ZKJSim.PC(tmpPkg);
}
void ofApp::GravityPanelResize(int w,int h)
{
	m_DeltaConfigPanel.setPosition(100+w/100,h/100);
}

void ofApp::ACPressed(int & eventarg)
{
	acpkg tmpPkg = {COMHEAD,sizeof(acpkg),ACID};
	int i = 0 ;
	for(i=0;i<3;i++){
		tmpPkg.gpsv_1[i]=1;
		tmpPkg.gpsv[i]=2;
	}
	//tmpPkg.fibb[0]=-3.18178639321271f;
	//tmpPkg.fibb[1]=10.1822359751592f;
	//tmpPkg.fibb[2]=3.98456485234475f;
	tmpPkg.starti=2;
	m_ZKJSim.Send(tmpPkg);
}

void ofApp::DynGPressed(int&eventarg)
{
	scpkg tmpPkg={COMHEAD,sizeof(scpkg),SCID,0,0};
	if(btnDynG.isToggled()){
		tmpPkg.cmd = DYNGE;
	}
	else
	{
		tmpPkg.cmd = DYNGD;
	}
	
	m_ZKJSim.Send(tmpPkg);
}

void ofApp::GA5Pressed(int & eventarg)
{
	scpkg tmpPkg={COMHEAD,sizeof(scpkg),SCID,0,0};
	tmpPkg.cmd = GAPER5;	
	m_ZKJSim.Send(tmpPkg);
}


void ofApp::GA10Pressed(int & eventarg)
{
	scpkg tmpPkg={COMHEAD,sizeof(scpkg),SCID,0,0};
	tmpPkg.cmd = GAPER10;	
	m_ZKJSim.Send(tmpPkg);
}
void ofApp::GA20Pressed(int & eventarg)
{
	scpkg tmpPkg={COMHEAD,sizeof(scpkg),SCID,0,0};
	tmpPkg.cmd = GAPER20;	
	m_ZKJSim.Send(tmpPkg);
}
void ofApp::HFAdjustPressed(int &eventarg)
{
	scpkg tmpPkg={COMHEAD,sizeof(scpkg),SCID,0,0};
	if(btnAdjust.isToggled()){
		tmpPkg.cmd = SCHAE;
	}
	else
	{
		tmpPkg.cmd = SCHAD;
	}
	
	m_ZKJSim.Send(tmpPkg);
}

void ofApp::CalcAVPressed(int &eventarg)
{
	scpkg tmpPkg={COMHEAD,sizeof(scpkg),SCID,0,0};
	tmpPkg.cmd = SCPOSFIX;
	m_ZKJSim.Send(tmpPkg);
}

void ofApp::GravityCheckPressed(int& eventarg)
{
	gc_pkg tmpPkg;
	tmpPkg.phead = COMHEAD;
	tmpPkg.id = GCID;
	tmpPkg.len = GCPKGLEN;
	tmpPkg.dfx = dfx ;
	tmpPkg.dfy = dfy ;
	tmpPkg.dfz = dfz ;
	tmpPkg.ffactorx = ffactorx ;
	tmpPkg.ffactory = ffactory ;
	tmpPkg.ffactorz = ffactorz ;
	tmpPkg.dw[0] = dwx;
	tmpPkg.dw[1] = dwy;
	tmpPkg.dw[2] = dwz;
	tmpPkg.wfactor[0] = wxfactor;
	tmpPkg.wfactor[1] = wyfactor;
	tmpPkg.wfactor[2] = wzfactor;
	tmpPkg.wfactor[3] = wypfactor;
	tmpPkg.flag = m_bFilter?(m_bMean?1:2):(m_bMean?1:0);
	m_ZKJSim.GC(tmpPkg);
}

void ofApp::saveGravityListener()
{
	ofXml deltaConfig;
	deltaConfig.addChild("DeltaConfig");
	deltaConfig.setTo("DeltaConfig");
	deltaConfig.addValue<float>("DeltaFx",dfx);
	deltaConfig.addValue<float>("DeltaFy",dfy);
	deltaConfig.addValue<float>("DeltaFz",dfz);
	deltaConfig.addValue<float>("FFactorX",ffactorx);
	deltaConfig.addValue<float>("FFactorY",ffactory);
	deltaConfig.addValue<float>("FFactorZ",ffactorz);
	deltaConfig.addValue<float>("DeltaWx",dwx);
	deltaConfig.addValue<float>("DeltaWy",dwy);
	deltaConfig.addValue<float>("DeltaWz",dwz);
	deltaConfig.addValue<float>("WXFactor",wxfactor);
	deltaConfig.addValue<float>("WYFactor",wyfactor);
	deltaConfig.addValue<float>("WZFactor",wzfactor);
	deltaConfig.addValue<float>("WYPFactor",wypfactor);
	deltaConfig.save("DeltaConfig.xml");
}

void ofApp::loadGravityListener()
{
	ofXml deltaConfig;
	if(deltaConfig.load("DeltaConfig.xml")){
		dfx = deltaConfig.getValue<float>("DeltaFx",-0.903891712f);
		dfy = deltaConfig.getValue<float>("DeltaFy",-0.91010108f);
		dfz = deltaConfig.getValue<float>("DeltaFz",-0.899398396f);
		ffactorx = deltaConfig.getValue<float>("FFactorX",64.19504035f);
		ffactory = deltaConfig.getValue<float>("FFactorY",64.19504035f);
		ffactorz = deltaConfig.getValue<float>("FFactorZ",64.19504035f);
		dwx = deltaConfig.getValue<float>("DeltaWx",0.0f);
		dwy = deltaConfig.getValue<float>("DeltaWy",0.0f);
		dwz = deltaConfig.getValue<float>("DeltaWz",0.0f);
		wxfactor = deltaConfig.getValue<float>("WXFactor",0.0f);
		wyfactor = deltaConfig.getValue<float>("WYFactor",0.0f);
		wzfactor = deltaConfig.getValue<float>("WZFactor",0.0f);
		wypfactor = deltaConfig.getValue<float>("WYPFactor",0.0f);
	}
	else{
		saveGravityListener();
	}

}

void ofApp::ZDPressed(int& eventarg)
{
	m_ZDPkg.phead = COMHEAD;
	m_ZDPkg.len = ZDPKGLEN;
	m_ZDPkg.id = ZDID;

	m_ZKJSim.ZD(m_ZDPkg);
}

void ofApp::loadConfig(){
	if(m_MainConfig.load("zkjconfig.xml")){
		m_szLogFile = m_MainConfig.getValue<string>("logfile");
		m_szHXZSerialName = m_MainConfig.getValue<string>("hxzdev");
		m_szZKJSerialName = m_MainConfig.getValue<string>("zkjdev");
		m_iZKJBaud = m_MainConfig.getValue<int>("zkjbaud",115200 );
		m_szGPSSerialName = m_MainConfig.getValue<string>("gpsdev");
		m_Motor.ADASSSTEP = m_MainConfig.getValue<int>("assstep",2000);
		m_Motor.MICROSTEP = m_MainConfig.getValue<int>("microstep",90);
		m_Motor.TUNNINGSTEP = m_MainConfig.getValue<int>("tunningstep",600);
		m_bDemo = m_MainConfig.getValue<bool>("isDemo",false);
		m_sFontName = m_MainConfig.getValue<string>("fontname","STHUPO.TTF");
		fontsize = m_MainConfig.getValue<int>("fontsize",24);
        if(m_MainConfig.exists("motordev"))
            m_szMotorSerialName = m_MainConfig.getValue<string>("motordev");
        else{
            m_szMotorSerialName="COM6";
            m_MainConfig.addValue("motordev",m_szMotorSerialName);
            m_MainConfig.save("zkjconfig.xml");
        }
		if(!m_MainConfig.exists("assstep")){
			m_MainConfig.addValue("assstep",m_Motor.ADASSSTEP);
			m_MainConfig.addValue("microstep",m_Motor.MICROSTEP);
			m_MainConfig.addValue("tunningstep",m_Motor.TUNNINGSTEP);
			m_MainConfig.save("zkjconfig.xml");
		}
		m_szPlanFilename = m_MainConfig.getValue<string>("planfile");
	}
	else{
		m_szHXZSerialName = "COM16";
		m_szZKJSerialName = "COM3";
		m_szGPSSerialName = "COM5";
        m_szMotorSerialName = "COM6";
		m_szPlanFilename = "plane.dae";
		m_szLogFile = "";
		if(m_MainConfig.addChild("zkjconfig")){
			if(m_MainConfig.setTo("zkjconfig")){
				m_MainConfig.addValue("zkjdev",m_szZKJSerialName);
				m_MainConfig.addValue("gpsdev",m_szGPSSerialName);
                m_MainConfig.addValue("motordev",m_szMotorSerialName);
				m_MainConfig.addValue("logfile",m_szLogFile);
				m_MainConfig.addValue("hxzdev",m_szHXZSerialName);
				m_MainConfig.addValue("planfile",m_szPlanFilename);

				m_MainConfig.addValue("assstep",2000);
				m_MainConfig.addValue("microstep",90);
				m_MainConfig.addValue("tunningstep",600);
				m_MainConfig.addValue("isDemo",0);
				m_MainConfig.addValue("fontname","STUHPO");
				m_MainConfig.save("zkjconfig.xml");
			}
		}
	}
}
#define ITERCOUNT	500

float ofApp::localnewton(float * fibb,float * Gfegg,float theta,float initGamma)
{
	float nteps = 1e-3f;
	float y;
	float yp;
	float gamma1;
	float gamma = initGamma;
	float mmy = 100.0f;
	float mmgamma = 0;
	int iter = 0;
	float fhe = SQRT(SQUAR(fibb[0])+SQUAR(fibb[1])+SQUAR(fibb[2]));
	float ghe = SQRT(SQUAR(Gfegg[0])+SQUAR(Gfegg[1])+SQUAR(Gfegg[2]));
	ofLogVerbose("ZKJ") << "init gamma " << gamma <<  " fibb he;" << fhe << " gfegg he:" << ghe << " diff:" <<  fabsf(fhe-ghe);
	if(fabsf(fhe - ghe)>0.5f){
		ofLogVerbose("ZKJ") << "should no solution" << std::endl;
		addCannotCalc();
	}
	do{
		y = -COS(theta) * SIN(gamma) * fibb[0] + SIN(theta) * fibb[1] + COS(theta) * COS(gamma) * fibb[2] - Gfegg[2];
		if(mmy > fabsf(y)){
			mmy = fabsf(y);
			mmgamma = gamma;
		}
		yp = -COS(theta) * COS(gamma) * fibb[0] - COS(theta) * SIN(gamma) * fibb[2];
		if(is_zero(yp))
			return HUGE_VAL;
		gamma1 = gamma - y/yp;
		if(fabs(gamma-gamma1)<nteps)
			break;
		//ofLogVerbose("ZKJ") << "iter " << iter << " delta " << fabs(gamma-gamma1) << " result: gamma "<< gamma << " gamma1 "  << gamma1 ;
		gamma= gamma1;

	}while(iter++<ITERCOUNT);
	if(iter>=ITERCOUNT){
		addNotNiuton();
		ofLogVerbose("ZKJ") << "bad iter mmgamma " << mmgamma << " mmy " << mmy;
		return mmgamma;
	}
	else{
		ofLogVerbose("ZKJ") << "good iter " << iter << " gamma " << gamma ;
	}
	return gamma;
}


//--------------------------------------------------------------
void ofApp::update(){
	if(!m_bGuiInited){
		m_bGuiInited = true;
	}
	GravityCheckBtn.m_bHide = m_bDemo;
    PostureCheckBtn.m_bHide = m_bDemo;
    btnZD.m_bHide  = m_bDemo;
    btnInitPos.m_bHide  = m_bDemo;
	btnCap.m_bHide  = m_bDemo;
	btnAC.m_bHide  = m_bDemo;
	
	btnBurnFW.m_bHide  = m_bDemo;
	btnCalcAV.m_bHide  = m_bDemo;
	btnDynG.m_bHide = m_bDemo;
	st_gps_inv gpspkg;
	m_ZKJSim.m_bPrintData = m_bDebug;
	m_Motor.m_bIsMax = m_bAdjustMax;
	if(udpConnection.Receive((char *)&gpspkg,sizeof(gpspkg))==sizeof(gpspkg)){
		static unsigned short lastruntime = gpspkg.runtime;
		//ofLogVerbose("ZKJ") << "longitude " << gpspkg.latitude/1e+6 << "latitude "<< gpspkg.longitude/1e+6 << endl;
		m_ZDPkg.fsdlongitude = gpspkg.longitude;
		m_ZDPkg.fsdlatitude = gpspkg.latitude;
		m_ZDPkg.fsdhegiht = gpspkg.height;
		//float psi
		m_ZDPkg.psi = get_refpsi(gpspkg.vele,gpspkg.veln)/PI * 180*100;
		//m_fPsi = m_ZDPkg.psi ;
		//fhaha TODO
		m_ZDPkg.theta = get_reftheta(gpspkg.vele,gpspkg.veln,gpspkg.velu)/PI * 180*100;
		//m_fTheta = m_ZDPkg.theta;
		m_ZDPkg.qktime = 45000;
		m_ZDPkg.workmode = DIMIANCESHI;

		m_ZDPkg.phead = COMHEAD;
		m_ZDPkg.len= ZDPKGLEN;
#if 1
		m_getLVnLast = m_getLVn;
		m_getLVn = gpspkg.veln;
		m_getLVeLast = m_getLVe;
		m_getLVe = gpspkg.vele;
		m_getLVuLast = m_getLVu;
		m_getLVu = gpspkg.velu;

		m_fGLambda = gpspkg.longitude/1e6f;
		m_fGFai = gpspkg.latitude/1e6f;
		m_fGH = gpspkg.height / 10.0f;
#endif
		m_GPSDataCount++;
		if(m_GPSSerial.isInitialized()){
			//printf("send gpspkg(%d) head %x len %x itow %d \n",sizeof(gpspkg),gpspkg.fhead,gpspkg.len,gpspkg.itow);
			if(m_bGPSData)
				m_GPSSerial.writeBytes((unsigned char*)&gpspkg,sizeof(gpspkg));
		}
		if(!btnZD.isEnabled()){
			btnZD.enable();
			btnInitPos.enable();
			//btnCar.enable();
			
			m_GPSLastTime = m_GPSCurTime = ofGetElapsedTimef();
		}
		else{
			time_t tnow;
			time(&tnow);
			m_GPSLastTime = m_GPSCurTime;
			m_GPSCurTime = ofGetElapsedTimef();
			//ofLogVerbose("GPS") << "Get package(last " << m_GPSLastTime << ") at : "<< m_GPSCurTime << " asctime: " << asctime(localtime(&tnow));

			//ofLogVerbose("GPS")  <<" deltatime:" << m_GPSCurTime - m_GPSLastTime;
		}

		//lastruntime = gpspkg.runtime;
	}
	
	float nowms = ofGetElapsedTimeMillis();
	AutoPtr<Notification> pN2f(m_zkjQueue.waitDequeueNotification(1));
#ifdef _DEBUG
	int getPkgCount = 0;
#endif
	while(pN2f){
		do{
			ZKJNotification * pZKJMNf = dynamic_cast<ZKJNotification*>(pN2f.get());
			if(pZKJMNf){
				inv_pkg &invpkg = pZKJMNf->m_zkjdata;
				m_fPsi = invpkg.psi / 100.0f;
				m_fTheta = invpkg.theta / 100.0f;
				m_fGamma = invpkg.gamma / 100.0f;
				break;
			}
			
			OriAccelNotification * pOAN = dynamic_cast<OriAccelNotification*>(pN2f.get());
			if(pOAN){
				m_FoibbCurve << 0 << std::pair<float,float>(nowms,pOAN->m_ax);
				m_FoibbCurve << 1 << std::pair<float,float>(nowms,pOAN->m_ay);
				m_FoibbCurve << 2 << std::pair<float,float>(nowms,pOAN->m_az);
				m_CustomCurve << 0 << std::pair<float,float> (nowms,SQRT(SQUAR(pOAN->m_ax)+SQUAR(pOAN->m_ay)+ SQUAR(pOAN->m_az)));
				
				break;
			}
			
			GCResNotification * pGCRes = dynamic_cast<GCResNotification*>(pN2f.get());
			if(pGCRes){
				gcres_pkg & gcrespkg = pGCRes->m_gcresdata;

				if(gcrespkg.id != INITPOSRESID){
					Ggc = gcrespkg.g;
					Gngc = gcrespkg.gn;
					dg1 = gcrespkg.figgz - gcrespkg.g;
					dg2 = gcrespkg.g - (-gcrespkg.gn);
					dg3 = gcrespkg.figgz - gcrespkg.g;
				}
				
				m_FoibbCurve << 0 << std::pair<float,float>(nowms,gcrespkg.foibbx);
				m_FoibbCurve << 1 << std::pair<float,float>(nowms,gcrespkg.foibby);
				m_FoibbCurve << 2 << std::pair<float,float>(nowms,gcrespkg.foibbz);
#ifdef USE_FIBB
				
				m_FibbCurve << 0 << std::pair<float,float>(nowms,gcrespkg.fibbx);
				m_FibbCurve << 1 << std::pair<float,float>(nowms,gcrespkg.fibby);
				m_FibbCurve << 2 << std::pair<float,float>(nowms,gcrespkg.fibbz);
#endif
				//m_CustomCurve << 0 << std::pair<float,float> (nowms,gcrespkg.g);
				m_CustomCurve << 0 << std::pair<float,float> (nowms,SQRT(SQUAR(gcrespkg.fibbx)+SQUAR(gcrespkg.fibby)+ SQUAR(gcrespkg.fibbz)));
				
				m_FmibbCurve << 0 << std::pair<float,float>(nowms,gcrespkg.fmibbx/ffactorx);
				m_FmibbCurve << 1 << std::pair<float,float>(nowms,gcrespkg.fmibby/ffactory);
				m_FmibbCurve << 2 << std::pair<float,float>(nowms,gcrespkg.fmibbz/ffactorz);
				m_fDelT += gcrespkg.deltatime;
				static unsigned long long llasttime = ofGetElapsedTimeMillis();
				unsigned long long lnowtime = ofGetElapsedTimeMillis();
				m_fGPSDelT += (lnowtime - llasttime)/1000.0f;
				llasttime = lnowtime;
				if(gcrespkg.id == GCRESID ){
					//ofLogVerbose("ZKJ") << "GC psi " << gcrespkg.psi << " theta " << gcrespkg.theta << " gamma " << gcrespkg.gamma << " g " << gcrespkg.g
					//	<< " gn " << gcrespkg.gn << endl;
					m_fPsi = gcrespkg.psi / PI * 180;
					m_fTheta = gcrespkg.theta/ PI * 180;
					m_fGamma = gcrespkg.gamma/ PI * 180;
					i2ctemp = gcrespkg.i2ctemp;
					//for(int k = 0 ;k<4;k++)
					//	temperature[k] = gcrespkg.temperature[k];

					m_AeggCurve << 0 << std::pair<float,float>(nowms,gcrespkg.figgx);
					m_AeggCurve << 1 << std::pair<float,float>(nowms,gcrespkg.figgy);
					m_AeggCurve << 2 << std::pair<float,float>(nowms,gcrespkg.figgz);

					m_VeggCurve << 0 << std::pair<float,float>(nowms,gcrespkg.gpsv[0]);
					m_VeggCurve << 1 << std::pair<float,float>(nowms,gcrespkg.gpsv[1]);
					m_VeggCurve << 2 << std::pair<float,float>(nowms,gcrespkg.gpsv[2]);
					
					m_GCGamma = m_fGamma;

					m_GammaCurve << 0 << std::pair<float,float>(nowms,gcrespkg.gamma / PI * 180);
					if(m_lastGGamma * m_GCGamma < 0 && fabsf(m_lastGGamma)>20.0f)
						m_iGraCircle ++;
					m_lastGGamma = m_GCGamma;

				}
				else{
					//ofLogVerbose("ZKJ") << "POS psi " << gcrespkg.psi << " theta " << gcrespkg.theta << " gamma " << gcrespkg.gamma << " g " << gcrespkg.g
					//	<< " gn " << gcrespkg.gn << endl;
					m_fPsiP = gcrespkg.psi/ PI * 180;
					m_fThetaP = gcrespkg.theta/ PI * 180;
					m_fGammaP = gcrespkg.gamma/ PI * 180;

					m_getVe = gcrespkg.gpsv[0]*100;
					m_getVn = gcrespkg.gpsv[1]*100;
					m_getVu = gcrespkg.gpsv[2]*100;

					m_getVeLast = gcrespkg.gpsv1[0]*100;
					m_getVnLast = gcrespkg.gpsv1[1]*100;
					m_getVuLast = gcrespkg.gpsv1[2]*100;


					wpibbx = gcrespkg.wpibbx;
					wpibby = gcrespkg.wpibby;
					wpibbz = gcrespkg.wpibbz;

					m_WpibbCurve << 0 << std::pair<float,float>(nowms,gcrespkg.wpibbx);
					m_WpibbCurve << 1 << std::pair<float,float>(nowms,gcrespkg.wpibby);
					m_WpibbCurve << 2 << std::pair<float,float>(nowms,gcrespkg.wpibbz);

					wibbx = gcrespkg.wibbx;
					wibby = gcrespkg.wibby;
					wibbz = gcrespkg.wibbz;
#ifdef WIN32
					m_WibbCurve << 0 << std::pair<float,float>(nowms,gcrespkg.wibbx);
					m_WibbCurve << 1 << std::pair<float,float>(nowms,gcrespkg.wibby);
					m_WibbCurve << 2 << std::pair<float,float>(nowms,gcrespkg.wibbz);
#endif

					m_fpibb[0] = gcrespkg.fmibbx;
					m_fpibb[1] = gcrespkg.fmibby;
					m_fpibb[2] = gcrespkg.fmibbz;
#if 1
					m_rFibbx = gcrespkg.fmibbx;
					m_rFibby = gcrespkg.fmibby;
					m_rFibbz = gcrespkg.fmibbz;
#elif 0
					m_rFibbx = gcrespkg.fibbx;
					m_rFibby = gcrespkg.fibby;
					m_rFibbz = gcrespkg.fibbz;
#else
					m_rFibbx = gcrespkg.dfx;
					m_rFibby = gcrespkg.dfy;
					m_rFibbz = gcrespkg.dfz;
#endif
					if(gcrespkg.id == INITPOSRESID)
						localCalcPosture();
					m_PosGamma = m_fGammaP;
					m_DeltaGamma = m_GCGamma - m_PosGamma;
					m_GammaCurve << 1 << std::pair<float,float>(nowms,gcrespkg.gamma/ PI * 180);
					m_GammaCurve << 2 << std::pair<float,float>(nowms,m_DeltaGamma);
					if(m_lastPGamma * m_PosGamma < 0 && fabsf(m_lastPGamma)>20.0f){
						m_iPosCircle ++;
					}
					else{

					}
					m_lastPGamma= m_PosGamma;
				}

				//m_LogWindow << "In GC LocalCalcPosture" <<  LOGNL;

                m_Motor << gcrespkg;
				break;
			}
			ZKJCAPNotification * pCapRes = dynamic_cast<ZKJCAPNotification*>(pN2f.get());
			if(pCapRes){
				cap_pkg & cappkg = pCapRes->m_capdata;
				//foibbx = cappkg.data[0];
				//foibby = cappkg.data[1];
				//foibbz = cappkg.data[2];
				//m_fpibb[0] = foibbx * ffactorx;
				//m_fpibb[1] = foibby * ffactory;
				//m_fpibb[2] = foibbz * ffactorz;
				//localCalcPosture();

				//wpibbx = cappkg.data[3];
				//wpibby = cappkg.data[4];
				//wpibbz = cappkg.data[5];

				m_WpibbCurve << 0 << std::pair<float,float>(nowms,cappkg.data[3]);
				if(m_bAdjustMax){	
					m_WpibbCurve << 1 << std::pair<float,float>(nowms,cappkg.data[6]);
				}
				else{

					m_WpibbCurve << 1 << std::pair<float,float>(nowms,cappkg.data[4]);
				}
				m_WpibbCurve << 2 << std::pair<float,float>(nowms,cappkg.data[5]);

				i2ctemp = cappkg.i2ctemp;
				//for(int k = 0 ;k<4;k++)
				//	temperature[k] = cappkg.adtemps[k];
				m_FoibbCurve << 0 << std::pair<float,float>(nowms,cappkg.data[0]);
				m_FoibbCurve << 1 << std::pair<float,float>(nowms,cappkg.data[1]);
				m_FoibbCurve << 2 << std::pair<float,float>(nowms,cappkg.data[2]);
				break;
			}
			ZKJERRORNotification * pErrPkg = dynamic_cast<ZKJERRORNotification*>(pN2f.get());
			if(pErrPkg){
				std::stringstream logstr;
				zkjerr_st & zkjerr = pErrPkg->m_zkjerr;
				if(pErrPkg->m_zkjerr.errnum==SDATA_ERROR){
					logstr << "Get Sensor Data Error " << zkjerr.extra1 << " " <<zkjerr.extra2 << " " << zkjerr.extra3;
				}
				else if(pErrPkg->m_zkjerr.errnum==CALCGAMMA_ERROR){
					logstr << "Get ZKJ Error cannot get gamma from newton";
				}
				else{
					logstr << "Get ZKJ Error " << std::hex << (int)zkjerr.errnum;
				}
				m_LogWindow <<  logstr.str()  << LOGNL;
				logstr.clear();
				logstr.str("");
				break;
			}
			CarPosNotification * pCarPkg = dynamic_cast<CarPosNotification*>(pN2f.get());
			if(pCarPkg){
				carpos_pkg &carpos = pCarPkg->m_carposdata;
				if(carpos.id == CARGPSPOSID){
					m_LogWindow << "gps car posture adjust once" << LOGNL;
					//std::cout << "gps car pos once" << endl;
				}
				//else if(carpos.id == CARADJPOSID){
				//	m_LogWindow << "gps car acce vel adjust once" << LOGNL; 
				//}
				if(m_bRealTime){
					m_fPsi = carpos.psi/ PI * 180;
					m_fTheta = carpos.theta/ PI * 180;
					m_fGamma = carpos.gamma/ PI * 180;
					if(!m_bFilter){
						//m_fPsiP = carpos.psiT/ PI * 180;
						//m_fThetaP = carpos.thetaT/ PI * 180;
						//m_fGammaP = carpos.gammaT/ PI * 180;
					}
				}
				m_fPsiL = carpos.Gpsi/ PI * 180;
				m_fThetaL = carpos.Gtheta/ PI * 180;
				m_fGammaL = carpos.Ggamma/ PI * 180;

				i2ctemp = carpos.i2ctemp;
				m_fLambda = carpos.lambda*180.0f/PI;
				m_fFai = carpos.fai*180.0f/PI;
				m_fH = carpos.H;
				m_fGLambda = carpos.Glambda*180.0f/PI;
				m_fGFai = carpos.Gfai * 180.0f /PI;
				m_fDelT = carpos.delT;
				m_fGPSDelT = carpos.gpsdelT;
				m_fGH = carpos.GH;

				//wibbx = carpos.wibb[0];
				//wibby = carpos.wibb[1];
				//wibbz = carpos.wibb[2];

				m_WibbCurve << 0 << std::pair<float,float>(nowms,carpos.wibb[0]);
				m_WibbCurve << 1 << std::pair<float,float>(nowms,carpos.wibb[1]);
				m_WibbCurve << 2 << std::pair<float,float>(nowms,carpos.wibb[2]);

				//wpibbx = carpos.woibb[0];
				//wpibbz = carpos.woibb[2];

				m_WpibbCurve << 0 << std::pair<float,float>(nowms,carpos.woibb[0]);
				if(m_bAdjustMax){
					//wpibby = carpos.woibb[3];
					m_WpibbCurve << 1 << std::pair<float,float>(nowms,carpos.woibb[3]);
				}
				else{
					//wpibby = carpos.woibb[1];
					m_WpibbCurve << 1 << std::pair<float,float>(nowms,carpos.woibb[1]);
				}
				m_WpibbCurve << 2 << std::pair<float,float>(nowms,carpos.woibb[2]);

				//wypfactor = carpos.wefactor[1];

				//fibbx = carpos.fibb[0];
				//fibby = carpos.fibb[1];
				//fibbz = carpos.fibb[2];
				m_FibbCurve << 0 << std::pair<float,float>(nowms,carpos.fibb[0]);
				m_FibbCurve << 1 << std::pair<float,float>(nowms,carpos.fibb[1]);
				m_FibbCurve << 2 << std::pair<float,float>(nowms,carpos.fibb[2]);

				for(int i = 0 ;i<3;i++){
					//m_fAegg[i] = carpos.Aegg[i];
					m_AeggCurve << i << std::pair<float,float>(nowms,carpos.Aegg[i]);
					//m_fGAegg[i] = carpos.GAegg[i];
					m_GAeggCurve << i << std::pair<float,float>(nowms,carpos.GAegg[i]);
					//m_fVegg[i] = carpos.Vegg[i];
					m_VeggCurve << i << std::pair<float,float>(nowms,carpos.Vegg[i]);
					//m_fGVegg[i] = carpos.GVegg[i];
					m_GVeggCurve << i << std::pair<float,float>(nowms,carpos.GVegg[i]);
				}
				m_getVe = carpos.Vegg[0]*100;
				m_getVn = carpos.Vegg[1]*100;
				m_getVu = carpos.Vegg[2]*100;

				//m_PosGamma = carpos.gamma/ PI * 180;
				m_GammaCurve << 1 << std::pair<float,float>(nowms,carpos.gamma/ PI * 180);
				m_GammaCurve << 2 << std::pair<float,float>(nowms,m_DeltaGamma);
				if(m_lastPGamma * m_PosGamma < 0 && fabsf(m_lastPGamma)>20.0f)
					m_iPosCircle ++;
				else{
					m_CircleSpeed = (m_PosGamma - m_lastPGamma ) / (nowms - m_lastdtime) * 1000  / 360;
				}
				m_lastPGamma= m_PosGamma;
				m_lastdtime = nowms;
				m_CustomCurve << 0 << std::pair<float,float> (nowms,sqrtf(carpos.fibb[0]*carpos.fibb[0] + carpos.fibb[1] * carpos.fibb[1] + carpos.fibb[2] * carpos.fibb[2]));
				break;
			}
				
			FWNotification * pNot = dynamic_cast<FWNotification*>(pN2f.get());
			if(pNot){
				if(pNot->getSource() == 1){
					m_FWConfig.m_fwQueue.enqueueNotification(new FWNotification(0) );
				}
				else
					m_LogWindow << "left " << pNot->getTemp() << " to burn" << LOGNL;
				break;
			}
			CustomNotification<acrespkg> * pacres = dynamic_cast<CustomNotification<acrespkg> *> (pN2f.get());
			if(pacres){
				acrespkg & acpkg =pacres->m_customdata;
				m_LogWindow << "ac res(" << acpkg.loopi << ")Vx:"  << acpkg.gpsv[0]  << " Vy:" << acpkg.gpsv[1] << "Vz:" << acpkg.gpsv[2] << LOGNL;
				std::cerr << "ac res(" << acpkg.loopi << "):"<<std::endl;
				for(int k=0;k<3;k++){
					for(int s=0;s<3;s++){
						std::cerr << " " << acpkg.Cgb[k][s] ;
					}
					std::cerr << std::endl;
				}
				std::cerr << "local psi: " << acpkg.psi << " theta: " << acpkg.theta << "gamma: " << acpkg.gamma << std::endl;
				std::cerr << "gps psi: " << acpkg.Gpsi << " theta: " << acpkg.Gtheta << "gamma: " << acpkg.Ggamma << std::endl;
				std::cerr << "Aegg: " << acpkg.Aegg[0] << " " << acpkg.Aegg[1] << " "<< acpkg.Aegg[2] << std::endl;
				std::cerr << "Vx:" << acpkg.gpsv[0]  << " Vy:" << acpkg.gpsv[1] << "Vz:" << acpkg.gpsv[2] << std::endl<<std::endl;
				break;
			}
		}while(0);
#ifdef _DEBUG
		if(getPkgCount++>20){
			break;
		}
#endif
		//static int getpcount=0;
		//std::cout << getpcount++ << endl;
		pN2f = m_zkjQueue.waitDequeueNotification(1);
	}
	m_ModelPlane.setRotation(0,m_fPsi,0.0f,1.0f,0.0f);
	m_ModelPlane.setRotation(1,m_fTheta,1.0f,0.0f,0.0f);
	m_ModelPlane.setRotation(2,m_fGamma,0.0f,0.0f,1.0f);
	m_ModelPlaneP.setRotation(0,m_fPsiP,0.0f,1.0f,0.0f);
	m_ModelPlaneP.setRotation(1,m_fThetaP,1.0f,0.0f,0.0f);
    m_ModelPlaneP.setRotation(2,m_fGammaP,0.0f,0.0f,1.0f);
    m_ModelPlaneL.setRotation(0,m_fPsiL,0.0f,1.0f,0.0f);
    m_ModelPlaneL.setRotation(1,m_fThetaL,1.0f,0.0f,0.0f);
    m_ModelPlaneL.setRotation(2,m_fGammaL,0.0f,0.0f,1.0f);
	
}

void ofApp::localCalcPosture()
{
	float aPsi,aTheta,aGamma;
	ofLogVerbose("ZKJ") << "local calc once" ;
    m_df[0] = dfx * ffactorx;
    m_df[1] = dfy * ffactory;
    m_df[2] = dfz * ffactorz;
	if(!lcfile.is_open()){
		char tmpbuf[64];
		time_t nowtime;
		struct tm * pnowtm;
		nowtime = time(NULL);
		pnowtm = localtime(&nowtime);
		#ifdef TARGET_WINDOWS
		asctime_s(tmpbuf,pnowtm);
		#else
		strcpy(tmpbuf,asctime(pnowtm));
		#endif
		char * ppos = strchr(tmpbuf,'\n');
		while(ppos){
			*ppos='_';
			ppos = strchr(tmpbuf,' ');
		}
		ppos = strchr(tmpbuf,':');
		while(ppos){
			*ppos = '_';
			ppos = strchr(tmpbuf,':');
		}
		strcat(tmpbuf,".calc.txt");
		lcfile.open(tmpbuf);
		if(!lcfile.is_open())
			printf("open file %s failed\n",tmpbuf);
		else{
			m_LogWindow << "open file " << tmpbuf << LOGNL;
			lcfile << "Time\tFx\tFy\tFz\tFex\tFey\tFez\tPsi\tTheta\tGamma\tManualGamma\tDiffGamma" << std::endl;
		}

	}
    //Step 2
    if(m_GPSDataCount<=0)
		return ;
#if 1
    m_Gvegg[0][0] = m_getLVe /100.0f;
    m_Gvegg[0][1] = m_getLVn /100.0f;
    m_Gvegg[0][2] = m_getLVu /100.0f;
    m_Gvegg[1][0] = m_getLVeLast /100.0f;
    m_Gvegg[1][1] = m_getLVnLast /100.0f;
    m_Gvegg[1][2] = m_getLVuLast /100.0f;
#endif
	addCalcTime();
    //Step 3
	ofLogVerbose("ZKJ")<< "Step 3";
	ofLogVerbose("ZKJ") << "fpibb:" <<  m_fpibb[0] << " " <<  m_fpibb[1] << " " <<   m_fpibb[2] ;
	ofLogVerbose("ZKJ") << "df:" <<   m_df[0] << " " <<  m_df[1] << " " <<   m_df[2] ;
    update_fw(m_fpibb,m_df,m_fibb);
	ofLogVerbose("ZKJ") << "fibb:" <<   m_fibb[0] << " " <<  m_fibb[1] << " " <<   m_fibb[2] ;

    //Step 4
	float deltatime = m_GPSCurTime - m_GPSLastTime;
	if(is_zero(deltatime)){
		ofLogVerbose("ZKJ") << "deltatime 0" ;
		addCannotCalc();
		return;
	}

	ofLogVerbose("ZKJ") << "Step 4" ;
	ofLogVerbose("GPS") << "time:" << m_GPSCurTime << " Gvegg[n]:" << m_Gvegg[0][0] << " " << m_Gvegg[0][1] << " " <<  m_Gvegg[0][2];
	ofLogVerbose("GPS") << "Gvegg[n-1]:" << m_Gvegg[1][0] << " " << m_Gvegg[1][1] << " " <<  m_Gvegg[1][2] ;
	ofLogVerbose("GPS") << "deltatime:" << deltatime ;
    get_Gaegg(m_Gvegg[0],m_Gvegg[1],deltatime,m_Gaegg);
	ofLogVerbose("ZKJ") << "Gaegg:" << m_Gaegg[0] << " " << m_Gaegg[1] << " " << m_Gaegg[2] ;
	m_gn = get_gn(CFAI);
    get_Gfegg(m_Gaegg,m_Gfegg,m_gn);
	ofLogVerbose("GPS") << "Gfegg:" << m_Gfegg[0] << " " << m_Gfegg[1] << " " << m_Gfegg[2] ;
	lcfile << m_GPSCurTime << '\t' << m_fibb[0] << '\t' << m_fibb[1] << '\t' << m_fibb[2] << '\t' ;
	lcfile << m_Gfegg[0] << '\t' << m_Gfegg[1] << '\t' << m_Gfegg[2] << '\t' ;
    //Step 6
    aTheta = get_reftheta(m_Gvegg[0][0],m_Gvegg[0][1],m_Gvegg[0][2]);
    aPsi = get_psi_byGvegg(m_Gvegg[0]);
	ofLogVerbose("ZKJ") << "parameter fibb " << m_fibb[0] << " " << m_fibb[1] << " " << m_fibb[2];
#if 1
	m_lFibbx = m_fpibb[0];
	m_lFibby = m_fpibb[1];
	m_lFibbz = m_fpibb[2];
#elif 0
	m_lFibbx = m_fibb[0];
	m_lFibby = m_fibb[1];
	m_lFibbz = m_fibb[2];
#else
	m_lFibbx = m_df[0];
	m_lFibby = m_df[1];
	m_lFibbz = m_df[2];
#endif
	ofLogVerbose("ZKJ") << "parameter Gfegg " << m_Gfegg[0] << " " << m_Gfegg[1] << " " << m_Gfegg[2];
	aGamma = localnewton(m_fibb,m_Gfegg,aTheta,m_fGammaL/180*S_PI);

	ofLogVerbose("ZKJ") << "Result: theta " <<aTheta << " psi " << aPsi << " gamma " << aGamma << mgamma ;

    //shift_vec3(m_Gvegg);
	if(!_isnan(aGamma) && _finite(aGamma)){
		addCanCalc();
		while(aGamma<0)
			aGamma += 2*S_PI;
		while(aGamma>=2*S_PI)
			aGamma -= 2*S_PI;
		m_fGammaL = aGamma* 180 / S_PI;

		m_fThetaL = aTheta* 180 / S_PI;
		m_fPsiL = aPsi* 180 / S_PI;
		lcfile << m_fPsiL << '\t' << m_fThetaL << '\t' << m_fGammaL << '\t' << mgamma << '\t' << m_fGammaL - mgamma << endl;
		ofLogVerbose("ZKJ") << "Result: in degree theta " <<m_fTheta << " psi " << m_fPsi << " gamma " << m_fGamma << endl ;
	}
	else{
		lcfile << m_fPsiL << '\t' << m_fThetaL << '\t' << m_fGammaL << '\t' << mgamma << '\t' << m_fGammaL - mgamma << endl;

	}
}

#define DRAWZWSTR()	tmpstr = strs.str(); \
		zwFont.drawString(tmpstr,leftPos.x,leftPos.y);\
		leftPos.y += fontsize + 2*LINESPACE; \
		strs.clear(); \
		strs.str(L"")

//--------------------------------------------------------------
void ofApp::draw(){
	ofColor textColor(255,255,255,255);
	ofxGuiSetTextColor(textColor);
	ofSetColor(200,200,100,255);
	ofEnableDepthTest();

	m_ModelPlane.draw(OF_MESH_FILL);
	if(!m_bDemo){
		ofSetColor(100,100,200,200);
		m_ModelPlaneP.draw(OF_MESH_FILL);
		ofSetColor(255,0,0,100);
		m_ModelPlaneL.draw(OF_MESH_WIREFRAME);
	}
	ofDisableDepthTest();
	ofSetColor(255);
	if(!m_bDemo){
		m_PosturePanel.draw();
		//m_ComparePanel.draw();
		motorPanel.draw();
		m_CalcDataPanel.draw();
		m_FoibbCurve.draw();
	#ifdef USE_FIBB
		m_FibbCurve.draw();
	#endif
		m_FmibbCurve.draw();
		m_WibbCurve.draw();
		m_WpibbCurve.draw();
		m_AeggCurve.draw();
		m_VeggCurve.draw();
		m_GAeggCurve.draw();
		m_GVeggCurve.draw();
		m_GammaCurve.draw();
		m_CustomCurve.draw();
	}
	else{
#define LINESPACE 8
		ofPoint leftPos = m_DeltaConfigPanel.getPosition();
		leftPos.y += m_DeltaConfigPanel.getHeight();
		ofColor oldColor = ofGetStyle().color;
		ofSetColor(200,30,10,255);
		std::wstringstream strs;
		leftPos.y += 4* LINESPACE;
		strs.fill(L'0');
		strs << L"横滚转速：" << wibby / (2 * PI) << L"圈／秒";
		std::wstring tmpstr ;
		DRAWZWSTR();
		
		strs << std::setw(6) << setiosflags(ios::fixed) ;
		strs.precision(3);
		strs << L"ＧＰＳ速度：";
		DRAWZWSTR();
		strs << L"东：" << m_fGVegg[0] << L"米／秒　北：" << m_fGVegg[1] << L"米／秒　天：" << m_fGVegg[2] <<L"米／秒";
		DRAWZWSTR();
		strs << L"ＧＰＳ姿态：";
		DRAWZWSTR();
		strs.precision(2);
		strs << L"航向：" << m_fPsiL << L"度　俯仰：" << m_fThetaL << L"度 横滚：" << m_fGammaL <<L"度";
		DRAWZWSTR();

		leftPos.x = ofGetWindowWidth() - 800;
		leftPos.y = m_SensorDataPanel.getPosition().y + m_SensorDataPanel.getHeight() + 2 * LINESPACE + fontsize /2 ;
		ofSetColor(10,20,200,255);
		strs.precision(3);
		strs << L"惯导速度：";
		DRAWZWSTR();
		strs << L"东：" << m_fVegg[0] << L"米／秒　北：" << m_fVegg[1] << L"米／秒　天：" << m_fVegg[2] <<L"米／秒";
		DRAWZWSTR();
		strs << L"惯导姿态：";
		DRAWZWSTR();
		strs.precision(2);
		strs << L"航向：" << m_fPsi << L"度　俯仰：" << m_fTheta << L"度 横滚：" << m_fGamma <<L"度";
		DRAWZWSTR();

		ofSetColor(oldColor);
		
	}
	m_DeltaConfigPanel.draw();
	m_SensorDataPanel.draw();
	m_CarPanel.draw();
    m_LogWindow.draw();
    for(int i = 0 ;i < LIGHTNUM ; ++i){
        ofPoint tpos;
        tpos = light[i].getPosition();
        ofCircle(tpos,15.0f);
    }
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	if(key == 'a'){
		m_fPsi-=0.10f;
	}
	if(key == 'd'){
		m_fPsi+= 0.10f;
	}
	if(key == 'w'){
        r2value+=0.10f;
	}
	if(key == 's'){
		r2value-= 0.10f;
	}
	if(key == 'q'){
		r3value-=0.10f;
	}
	if(key == 'e'){
		r3value+= 0.10f;
    }
	if(key == '1'){
        char nowtime[64]={0};
        time_t nt ;
        time(&nt);
        struct tm * nttm = localtime(&nt);
#ifdef WIN32
		strcpy(nowtime,asctime(nttm));
#else
        asctime_r(nttm,nowtime);
#endif
        char * ppos = strrchr(nowtime,'\n');
        if(ppos){
            *ppos=' ';
        }
        m_LogWindow << nowtime ;
        m_LogWindow << ofGetElapsedTimef() << (char)'\n';
	}

	if(key == '2'){
        m_LogWindow << ofRandom(0.1f, 1100.9f) << '\n';
	}
	//ofLogVerbose("ZKJ")<< r1value << "," << r2value << "," << r3value << endl;
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){
	//ofLogVerbose("ZKJ") << x << "," << y << endl;
	//int width = ofGetWindowWidth();
	//int height = ofGetWindowHeight();
	//m_fGamma = ofMap(x,0,width,-180,180,true);
	//m_fTheta = ofMap(y,0,height,-180,180,true);
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){
	//ofLogVerbose("ZKJ")<< "mouse Dragged " << button <<endl;
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
	//ofLogVerbose("ZKJ")<< "mouse Pressed " << button <<endl;
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){
	//ofLogVerbose("ZKJ")<< "mouse Released " << button <<endl;
}

void ofApp::planeResize(int w,int h)
{
	m_ModelPlane.setPosition(w*0.5f,h*0.7f,0);
	m_ModelPlaneP.setPosition(w*0.5f,h*0.7f,-20);
    m_ModelPlaneL.setPosition(w*0.5f,h*0.7f,-40);
}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
	btnZD.move(w-btnZD.getWidth() - 10,40);
	planeResize(w,h);
	//lightPan.setPosition(mpPos.x+200,mpPos.y,20);
	GravityPanelResize(w,h);
	PosturePanelResize(w,h);
	SensorDataPanelResize(w,h);
	CalcDataPanelResize(w,h);
	FibbCurveResize(w,h);
	WibbCurveResize(w,h);
	buttonsResize(w,h);
    logWindowResize(w,h);
    motorPanelResize(w,h);
    //lightResize(w,h);
}
void ofApp::lightResize(int w,int h)
{
    m_lightRadius = w > h ? h*2/5:w*2/5;
    for(int i = 0 ;i<LIGHTNUM ;++i){
        ofPoint lightPos ;

        //float xpos = sin(i*2*PI / LIGHTNUM)* m_lightRadius;
        //float ypos = cos(i*2*PI / LIGHTNUM) * m_lightRadius;

        //lightPos.x = w / 2 + xpos;
        //lightPos.y = h / 2 + ypos;
        //lightPos.z = 0.0f;
        lightPos.x = i*2*w ;
        lightPos.y = i*h ;
        lightPos.z = 0.0f;
        //printf("x %f y %f\n",xpos,ypos);
        light[i].disable();
        light[i].setPosition(lightPos);
        light[i].enable();
    }

}
void ofApp::logWindowResize(int w,int h)
{
    m_LogWindow.setPosition(0,h-m_LogWindow.getHeight());
}

void ofApp::WibbCurveResize(int w,int h)
{
	m_WibbCurve.setPosition(m_FibbCurve.getPosition().x,m_FibbCurve.getPosition().y + m_FibbCurve.getHeight()+5);
	m_WpibbCurve.setPosition(m_WibbCurve.getPosition().x,m_WibbCurve.getPosition().y + m_WibbCurve.getHeight()+5);

	m_AeggCurve.setPosition(m_WibbCurve.getPosition().x,m_WpibbCurve.getPosition().y + m_WpibbCurve.getHeight()+5);
	m_VeggCurve.setPosition(m_WibbCurve.getPosition().x,m_AeggCurve.getPosition().y + m_AeggCurve.getHeight()+5);
	m_GAeggCurve.setPosition(m_WibbCurve.getPosition().x,m_VeggCurve.getPosition().y + m_VeggCurve.getHeight()+5);
	m_GVeggCurve.setPosition(m_WibbCurve.getPosition().x,m_GAeggCurve.getPosition().y + m_GAeggCurve.getHeight()+5);
#if 1
	m_GammaCurve.setPosition(m_GVeggCurve.getPosition().x,m_GVeggCurve.getPosition().y+m_GVeggCurve.getHeight() + 5);
#else
	m_GammaCurve.setPosition(w-m_GammaCurve.getWidth() - 5,(h - m_GammaCurve.getHeight()) / 2);
#endif
	m_CustomCurve.setPosition(m_GammaCurve.getPosition().x,m_GammaCurve.getPosition().y + m_GammaCurve.getHeight() + 5);
}

void ofApp::FibbCurveResize(int w,int h)
{
	m_FoibbCurve.setPosition(10,m_DeltaConfigPanel.getHeight() + 20 );

	m_FmibbCurve.setPosition(m_FoibbCurve.getPosition().x ,m_FoibbCurve.getPosition().y+m_FoibbCurve.getHeight()+5);
#ifdef USE_FIBB
	m_FibbCurve.setPosition(m_FoibbCurve.getPosition().x ,m_FmibbCurve.getPosition().y + m_FmibbCurve.getHeight()+5);
#endif
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}

int MAVLINK_SEND_UART_BYTES(mavlink_channel_t chan, const uint8_t *buf, uint16_t len)
{
	static ofApp * myApp = (ofApp*)ofGetAppPtr();
	static CZKJSerialThread & zkj = myApp->m_ZKJSim;
	static ofSerial & se = zkj.m_serial;
	return se.writeBytes((unsigned char *)buf,len);
}
mavlink_system_t mavlink_system = { 1 , 5};
