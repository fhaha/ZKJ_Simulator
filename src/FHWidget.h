#pragma once
#include <string>
#include "ofMain.h"

class CFHWidget
{
public:
	CFHWidget(void);
	virtual ~CFHWidget(void);

protected:
	void render();
	void generateDraw();
private:
	std::wstring m_wsLabel;
};

