#pragma once

#define SELFALIGNED	(1u<<3)
#define GEOMAGRECVD	(1u<<2)
#define SATELOCED	(1u<<1)
#define SATERECVD	(1u)
#define INVFLAGMASK	(0xf)

#pragma pack(push)
#pragma pack(1)
typedef struct _st_inv_pkg{
	unsigned short phead;	//0x90EB
	unsigned char len;		//71
	unsigned short id;		//0x5A0A
	unsigned int curtick;
	int theta;				//0.01 degree
	int thetavelocity;	//0.001 degree/second
	int psi;			//0.01 degree
	int psivelocity;	//0.001 degree/second
	int gamma;			//0.01 degree
	int gammavelocity;	//0.001 degree/second
	int x_displacement;	//meter
	int y_displacement;	//meter
	int z_dispalcement;	//meter
	int x_velocity;		//0.01m/s
	int y_velocity;		//0.01m/s
	int z_velocity;		//0.01m/s
	int x_acceleration;	//0.0001g
	int y_acceleration;	//0.0001g
	int z_acceleration;	//0.0001g
	unsigned char invflag;	//bit 0 - satellite data status(1:recvd,0:not recvd)
							//bit 1 - satellite location status(1:fixed, 0: not fixed)
							//bit 2 - geomagnetism(1:recvd, 0:not recvd)
							//bit 3 - self alignment(1:aligned,0:not aligned)
	unsigned char crc;
}inv_pkg,*pinv_pkg;

typedef struct _st_roll_pkg{
	unsigned short phead;	//0x90EB
	unsigned char len;		//10
	unsigned short id;		//0x5A0B
	unsigned int curtick;	//0.001s
	unsigned char crc;
}roll_pkg,*proll_pkg;

#define INVPKGLEN	sizeof(inv_pkg)
#define ROLLPKGLEN	sizeof(roll_pkg)

#pragma pack(pop)
