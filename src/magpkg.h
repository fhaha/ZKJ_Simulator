#pragma once

#define MAGPKGLEN	12
#define MAGPKG_ID	0x530D
#define MAGPKGID1	0x0D
#define MAGPKGID2	0x53

#pragma pack(push)
#pragma pack(1)
typedef struct _st_mag_pkg{
	unsigned short phead;	//0x90EB
	unsigned char len;		//12
	unsigned short id;		//0x530D
	short gamma;			//0.1 degree(0~360)
	int	gammavelocity;		//0.001 degree/s
	unsigned char crc;
}mag_pkg,*pmag_pkg;
#pragma pack(pop)