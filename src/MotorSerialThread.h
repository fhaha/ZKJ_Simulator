#pragma once
#include <ofThread.h>
#include <ofMain.h>
#include "comhead.h"
#include "zdpkg.h"
#include "motorpkg.h"

enum _E_ADMODE {
    AXADJ,
    AYADJ,
	AZADJ,
    GXADJ,
    GYADJ,
	GZADJ,
    ADJNORMAL
};

enum _E_XYSTEP {
	X_ASS1,
	X_ASS2,
	X_SUB,
	X_ADD,
	X_REV,
	Y_ASS1,
	Y_ASS2,
	Y_SUB,
	Y_ADD,
	Y_REV
};

class CMotorSerialThread :
	public ofThread
{
public:
	CMotorSerialThread(void);
	~CMotorSerialThread(void);

public:
	ofSerial m_serial;
	bool setup(std::string device, int baudrate);
	void threadedFunction();
	void quit();
	bool isBadEffect();
	bool compareFunc(float value1,float value2);
	void updateHistoryMM(float value);
	bool compareHistoryFunc();
	void StepForward();
	CMotorSerialThread & operator<<(const gcres_pkg &gcrespkg);
	void StartAdjust(_E_ADMODE mode);
	
	void SendNear(int value);
	void SendFar(int value);
	void SendXAdjust(int step,int needres=1);
	void SendYAdjust(int step,int needres=1);
	_E_ADMODE m_eADMode;
	bool m_bPositiveDirec;
	bool m_bIsMax;
	
	unsigned short MICROSTEP;
	unsigned short TUNNINGSTEP;
	unsigned short ADASSSTEP;
	_E_XYSTEP m_iXYStep;	//1 for x bad effect
							//2 for x good effect
							//3 for x bad effect 
							//4 for y bad effect
							//5 for y good effect
							//6 for y bad effect
	
private:
	bool m_bIsQuiting;
	float m_fNewMean;
	float m_fHistoryMM;
	float m_fOldMean;
	float m_fAssValue[3];
	int m_iBackTime;
	vector<float> m_lastfmibbx;
	vector<float> m_lastfmibby;
	vector<float> m_lastfmibbz;
	vector<float> m_lastgmibbx;
	vector<float> m_lastgmibby;
	vector<float> m_lastgmibbz;
	void SendFXChange(bool isAdd,unsigned short step);
	void SendFYChange(bool isAdd,unsigned short step);
	
	void SendFXChange(bool isAdd){
		SendFXChange(isAdd,TUNNINGSTEP);
	}
	void SendFYChange(bool isAdd){
		SendFYChange(isAdd,TUNNINGSTEP);
	}
	void changeMotorDirection();
	void CaptureData();
};