
#include "ofApp.h"
#include "HXZSerialThread.h"
#include "comhead.h"
#include "invpkg.h"
#include "zdpkg.h"


CHXZSerialThread::CHXZSerialThread(void)
{
	m_bIsQuiting = false;
}


CHXZSerialThread::~CHXZSerialThread(void)
{
}

void CHXZSerialThread::quit()
{
	m_bIsQuiting = true;
}

bool CHXZSerialThread::setup(std::string device, int baudrate)
{
	m_serial.listDevices();
	vector <ofSerialDeviceInfo> deviceList = m_serial.getDeviceList();
	vector<ofSerialDeviceInfo>::iterator iter = deviceList.begin();
	string devname;
	while(iter != deviceList.end()){
		if(device == (*iter).getDeviceName()){
			devname = device;
		}
		++iter;
	}
	if(devname.empty()){
		return false;
	}
	m_serial.setup(devname,baudrate);
	startThread();
	return true;
}

unsigned char CHXZSerialThread::calcCRC(comhead_st* pPkg)
{
	int i = 0 ;
	int len = pPkg->len;
	unsigned char ret = 0;
	unsigned char * pData = (unsigned char*)pPkg;
	for(i=0;i<len;i++){
		ret ^= pData[i];
	}
	return ret;
}

#define BUFLEN	2048
void CHXZSerialThread::threadedFunction()
{
	unsigned char buf[BUFLEN]={0};
	int recvOff = 0;
	//m_serial.writeBytes(cd5,sizeof(cd5));
	while(!m_bIsQuiting){
		if(m_serial.available()|| recvOff >= sizeof(comhead_st)){
			recvOff += m_serial.readBytes(buf+recvOff,32);
			if(recvOff>=sizeof(comhead_st)){
				pcomhead_st pPkgHead = (pcomhead_st)buf;
				if(pPkgHead->phead != COMHEAD)
				{
					int i = 0 ;
					bool findHead = false;
#ifdef _DEBUG
					printf("HXZ drop flag %x len %d\n",pPkgHead->phead,recvOff);
#endif
					for(i = 0 ;i<recvOff;i++){
						if(buf[i]==0xEB){
							findHead = true;
							if(i<recvOff-1){
								if(buf[i+1] == 0x90){
									char tmpbuf[BUFLEN]={0};
									memcpy(tmpbuf,buf+i,recvOff-i);
									memcpy(buf,tmpbuf,recvOff-i);
									recvOff -= i;
									break;
								}
							}
							else{
								recvOff = 1;
								buf[0]=0xEB;
							}
						}
					}
					if(!findHead){
						recvOff = 0;
					}
					continue;
				}

				if(recvOff>= pPkgHead->len){
					if(parseNBXData(buf,recvOff)){

					}
					else{
						printf("!!!!!!parse failed,left len %d\n",recvOff - (pPkgHead->len));
					}
					int leftlen = recvOff - (pPkgHead->len);
					if(leftlen>0){
						char tmpbuf[BUFLEN]={0};
						printf("leftdata: ");
						for(int k = 0 ;k < leftlen;++k){
							printf("%02x " , (int)buf[pPkgHead->len +k]);
						}
						printf("\n");
						memcpy(tmpbuf,buf+pPkgHead->len,leftlen);
						memcpy(buf,tmpbuf,leftlen);
					}
					recvOff = leftlen;
				}
				else{
#ifdef _DEBUG
					printf("preParse off %d need %d,id %x\n",recvOff,pPkgHead->len,pPkgHead->id);
#endif
				}
			}
		}
		sleep(1);

	}
	//m_serial.close();

}



bool CHXZSerialThread::parseNBXData(unsigned char * pdata,int len)
{
	pcomhead_st pHead = (pcomhead_st) pdata;

	time_t now;
	time(&now);
	char * lt = asctime(localtime(&now));
	switch(pHead->id){
	case ZDID:{
			st_zd_pkg * pZDPkg = (st_zd_pkg*)pHead;
			ofLogVerbose("HXJ") << lt << " ZD Info" << endl;
			ofLogVerbose("HXJ") << "\t" << "launch point longitude" << pZDPkg->fsdlongitude/1e+6 << "" <<endl;
			ofLogVerbose("HXJ") << "\t" << "launch point latitude" << pZDPkg->fsdlatitude/1e+6 << "" <<endl;
			ofLogVerbose("HXJ") << "\t" << "launch point altitude" << pZDPkg->fsdhegiht/10.0f << "" <<endl;
			ofLogVerbose("HXJ") << "\t" << "init psi��range 0~360��" << pZDPkg->psi/100.0f << "" <<endl;
			ofLogVerbose("HXJ") << "\t" << "init theta��range��0~80����" << pZDPkg->theta/100.0f << "" <<endl;
			ofLogVerbose("HXJ") << "\t" << "start control time" << pZDPkg->qktime/1000.0f << "sec" <<endl;
			ofLogVerbose("HXJ") << "\t" << "work mode" <<std::hex << pZDPkg->workmode <<endl;
			break;
		}
	case ROLLPKG_ID:{
			roll_pkg * pRollPkg = (roll_pkg *)pHead;
			ofLogVerbose("HXJ") << lt << " roll info" << endl;
			ofLogVerbose("HXJ") << "\t" << "relative power on time" << pRollPkg->curtick/1000.0f << "sec" <<endl;
			break;
		}
	case INVPKG_ID:{
			pinv_pkg pINVPkg = (pinv_pkg)pdata;
			ofLogVerbose("HXJ") << lt << " nav info" << endl;
			ofLogVerbose("HXJ") << "\t" << "theta" << pINVPkg->theta/100.0f << "degree" <<endl;
			ofLogVerbose("HXJ") << "\t" << "theta angular speed" << pINVPkg->thetavelocity/1000.0f << "degree/sec" <<endl;
			ofLogVerbose("HXJ") << "\t" << "psi" << pINVPkg->psi/100.0f << "degree" <<endl;
			ofLogVerbose("HXJ") << "\t" << "psi angular speed" << pINVPkg->psivelocity/1000.0f << "degree/sec" <<endl;
			ofLogVerbose("HXJ") << "\t" << "roll " << pINVPkg->gamma/100.0f << "degree" <<endl;
			ofLogVerbose("HXJ") << "\t" << "roll angular speed" << pINVPkg->gammavelocity/1000.0f << "degree/sec" <<endl;
			ofLogVerbose("HXJ") << "\t" << "X displacement" << pINVPkg->x_displacement << "m" <<endl;
			ofLogVerbose("HXJ") << "\t" << "Y displacement" << pINVPkg->y_displacement << "m" <<endl;
			ofLogVerbose("HXJ") << "\t" << "Z displacement" << pINVPkg->z_dispalcement << "m" <<endl;
			ofLogVerbose("HXJ") << "\t" << "X velocity" << pINVPkg->x_velocity/100.0f << "m/sec" <<endl;
			ofLogVerbose("HXJ") << "\t" << "Y velocity" << pINVPkg->y_velocity/100.0f << "m/sec" <<endl;
			ofLogVerbose("HXJ") << "\t" << "Z velocity" << pINVPkg->z_velocity/100.0f << "m/sec" <<endl;
			ofLogVerbose("HXJ") << "\t" << "X acceleration" << pINVPkg->x_acceleration/1000.0f << "g" <<endl;
			ofLogVerbose("HXJ") << "\t" << "Y acceleration" << pINVPkg->y_acceleration/1000.0f << "g" <<endl;
			ofLogVerbose("HXJ") << "\t" << "Z acceleration" << pINVPkg->z_acceleration/1000.0f << "g" <<endl;
			break;
		}
	}
	return true;
}
