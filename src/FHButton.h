/*
 *  ofxSimpleSlider.h
 *  Created by Golan Levin on 2/24/12.
 *
 */


#pragma once

#include <ofMain.h>
#include "ofxTrueTypeFontUC.h"

enum FHBT{
	FHBTNORMAL,
	FHBTTOGGLE
};


class CFHButton {

	public:
		CFHButton(){
			x = 0;
			y = 0;
			box.set(x,y, 50, 20);

			bHasFocus = false;
			bEnabled = true;
			bPressed = false;
			m_bToggled = false;
			m_bHide = false;
			m_eButtonType = FHBTNORMAL;
			m_labelshape.clear();
		}
		~CFHButton(){
            removeDefaultListener(); 
		}
		//template<class strclass>
		//void    setup(strclass wstr,string fontname=DEFAULTFONTNAME);

		CFHButton &  setup(std::wstring & str,ofxTrueTypeFontUC * pFont,std::wstring altstr=L"");
		CFHButton &  setup(std::string & str,ofxTrueTypeFontUC * pFont,std::string  altstr="");
		void	resize(float inw,float inh);
		void	move(float inx,float iny);
		void	move(ofPoint & pos);
		void	enable();
		void	disable();
		bool	isEnabled();
		bool isToggled();
		void	draw(ofEventArgs& event);
		void  draw();
		virtual void	mouseMoved(ofMouseEventArgs& event);
		void	mouseDragged(ofMouseEventArgs& event);
		void	mousePressed(ofMouseEventArgs& event);
		void	mouseReleased(ofMouseEventArgs& event);
		void	defaultMouseHandler(ofMouseEventArgs & event);
		void	defaultTouchHandler(ofTouchEventArgs & event);

        template<class labeltype>
		void	setLabelString (labeltype str,int len);
		void setWidth(int width){
			box.width = width;
		}
		void setHeight(int height){
			box.height = height;
		}
		ofEvent<int> btnEvent;
		float getX(){return x;}
		float getY() { return y;}
		float getWidth() { return box.width;}
		float getHeight() { return box.height;}
		bool m_bHide;
        static ofxTrueTypeFontUC *pzhFont;
		void generateDraw();
    private:
        void addDefaultListener();
        void removeDefaultListener();
		ofPoint m_labelDrawPos;
	protected:
		float	x;
		float	y;
		ofRectangle box;
		bool	bHasFocus;
		bool	bEnabled;
		bool	bPressed;
		bool    m_bToggled;
		//string	labelString;
        vector<ofPath> m_labelshape;
		std::wstring m_wsLabel,m_wsAltLabel;
		FHBT m_eButtonType;
		void render();

};
