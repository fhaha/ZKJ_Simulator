#pragma once

#include "ofxGuiGroup.h"

class ofxGuiGroup;

#define DEFAULTNORMALWIDTH 500.0f
#define FHCGLINENUM	3

enum FHGUI_MODE {
	FHMIN,
	FHNORMAL,
	FHMAX
};

class CFHCurveGraph : public ofxGuiGroup {
public:
	CFHCurveGraph();
	CFHCurveGraph(const ofParameterGroup & parameters, string filename="settings.xml", float x = 10, float y = 10,float width=DEFAULTNORMALWIDTH,FHGUI_MODE defalutmode=FHNORMAL);
	~CFHCurveGraph();
	CFHCurveGraph * setup(string collectionName="", string filename="settings.xml", float x = 10, float y = 10,float width=DEFAULTNORMALWIDTH,FHGUI_MODE defalutmode=FHNORMAL);
	CFHCurveGraph * setup(const ofParameterGroup & parameters, string filename="settings.xml", float x = 10, float y = 10,float width=DEFAULTNORMALWIDTH,FHGUI_MODE defalutmode=FHNORMAL);

	bool mouseReleased(ofMouseEventArgs & args);

	ofEvent<void> loadPressedE;
	ofEvent<void> savePressedE;
	ofEvent<void> maxPressedE;

	void setData(ofParameter<float> & parameter,int index,ofColor & color);
	void setData(ofParameter<float> & parameter,int index);
	virtual bool keyPressed(ofMouseEventArgs & args)
	{
		return false;
	}
	virtual bool keyReleased(ofMouseEventArgs & args)
	{
		return false;
	}
#ifdef TARGET_LINUX
	virtual bool mouseMoved(ofMouseEventArgs & args){
        return false;
	}
	//virtual bool mousePressed(ofMouseEventArgs & args){
    //   return false;
	//}
	virtual bool mouseDragged(ofMouseEventArgs & args){
        return false;
	}
#endif
	void minimize(){
		m_eGUIMinMaxMode= FHMIN;
		checkGUIMinMax();
		ofxGuiGroup::minimize();
	}
	CFHCurveGraph & operator <<(std::pair<float,float> data);
	CFHCurveGraph & operator <<(int index);
protected:
	void render();
	bool setValue(float mx, float my, bool bCheck);
	void generateDraw();
	void loadIcons();
	void updatePolylines();
private:
	ofRectangle btnNormalBox, btnMinBox,btnMaxBox;
	static ofImage maxIcon, minIcon,normalIcon;
	vector<ofPolyline> m_polylines;
	//vector<
	ofParameter<float> * m_pValueParams[3];
    ofPoint grabPt;
	bool bGrabbed;
	FHGUI_MODE m_eGUIMinMaxMode;
	float m_fNormalWidth;
	float m_fMinHeight;
	float m_fNormalHeight;
	float m_fLastX;
	float m_fDispSeconds;
	float m_fPaintBoardHeight;
	ofxToggle * m_bDispData[FHCGLINENUM];
	ofParameter<bool> m_bDispDataParameter[FHCGLINENUM];
	ofParameter<bool> m_bSymmatic;
	ofRectangle paintboardBox;
	float m_fXAxisPos;
	ofPath xMasterAxis;
	ofPath xSlaveAxis;
	ofPath graphyBG;
	ofPath yAxis;
	ofPoint mxLabelPos[11];
	string mxlabels[11];
	ofPolyline polyline[FHCGLINENUM];
	std::vector<std::pair<float,float> > pldata[FHCGLINENUM];
	ofColor plColors[FHCGLINENUM];
	int m_curplindex;
	void checkGUIMinMax();
	void changeGUINormalMax();
	float m_dispLatestTime;
	float m_fLastHeight;
	ofParameter<bool> m_dispData[FHCGLINENUM];
};
