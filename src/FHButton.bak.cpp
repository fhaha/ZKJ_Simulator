/*
 *  ofxSimpleSlider.cpp
 *  Created by Golan Levin on 2/24/12.
 *
 */

#include "FHButton.h"
#define FONTSIZE 16
#define HMARGIN	4
#define VMARGIN 4
void CFHButton::removeDefaultListener()
{
    ofRemoveListener(ofEvents().draw, this, &CFHButton::draw);
	ofRemoveListener(ofEvents().mouseMoved, this, &CFHButton::mouseMoved);
	ofRemoveListener(ofEvents().mousePressed, this, &CFHButton::mousePressed);
	ofRemoveListener(ofEvents().mouseReleased, this, &CFHButton::mouseReleased);
}
void CFHButton::addDefaultListener()
{
    ofAddListener(ofEvents().draw, this, &CFHButton::draw);
	ofAddListener(ofEvents().mouseMoved, this, &CFHButton::mouseMoved);
	ofAddListener(ofEvents().mousePressed, this, &CFHButton::mousePressed);
	ofAddListener(ofEvents().mouseReleased, this, &CFHButton::mouseReleased);
}
/*
template<class strclass>
void CFHButton::setup(strclass str,string fontname)
{
    addDefaultListener();
    zhFont.loadFont(fontname,12,true,true);
    setLabelString(str);
}*/

void CFHButton::setup(const wchar_t * str,string fontname)
{
    addDefaultListener();
    zhFont.loadFont(fontname,FONTSIZE,true,true);
    setLabelString(str,wcslen(str));
}

void CFHButton::setup(const wchar_t * str,const wchar_t * altstr,string fontname)
{
	setup(str,fontname);
	m_altlabelshape = zhFont.getStringAsPoints(altstr);
	ofRectangle outRec = zhFont.getStringBoundingBox(altstr,0,0);
	outRec.width += 2*HMARGIN;
	outRec.height += 2*VMARGIN;
	if(outRec.width>box.width || outRec.height > box.height){
		box.set(outRec);
	}
	m_eButtonType = FHBTTOGGLE;
}

void CFHButton::setup(const char *str,string fontname)
{
	addDefaultListener();
	zhFont.loadFont(fontname,FONTSIZE,true,true);
	setLabelString(str,strlen(str));
}

bool CFHButton::isToggled()
{
	return bToggled;
}

void CFHButton::resize(float inw,float inh)
{
	/*
	width = inw;
	height = inh;
	box.set(x,y, width, height);\*/
	ofLogVerbose("FHB") << "not support resize!" << std::endl;
}

void CFHButton::enable()
{
	bEnabled = true;
}
void CFHButton::disable()
{
	bEnabled = false;
}

bool CFHButton::isEnabled()
{
	return bEnabled;
}

void CFHButton::move(float inx,float iny)
{
	x = inx;
	y = iny;
	box.setX(x);
	box.setY(y);
}

void CFHButton::move(ofPoint & pos)
{
	box.setPosition(pos);
}


//----------------------------------------------------
template<class labeltype>
void CFHButton::setLabelString (labeltype str,int len){
	labeltype labelString = str;
	m_labelshape = zhFont.getStringAsPoints(str);
	ofRectangle outRec = zhFont.getStringBoundingBox(str,0,0);
	outRec.width += 2* HMARGIN;
	outRec.height += 2* VMARGIN;
	box.set(outRec);
}


//----------------------------------------------------
void CFHButton::draw(ofEventArgs& event){
	if(m_bHide)
		return;
	ofEnableAlphaBlending();
	ofDisableSmoothing();
	ofPushMatrix();
	ofTranslate(x,y,0);

	// Use different alphas if we're actively maniupulating me.
	float sliderAlpha = (bHasFocus) ? 128:64;
	float spineAlpha  = (bHasFocus) ? 255:160;

	// draw box outline
	ofNoFill();
	ofSetLineWidth(1.0);
	ofSetColor(64,64,64, sliderAlpha);
	ofRect(0,0, box.width,box.height);

	// draw spine
	//float labelStringWidth = m_labelshape.size()*14;
	ofSetLineWidth(1.0);
	ofSetColor(0,0,0, spineAlpha);
	if (bPressed){
		ofLine(0,0, box.width,0);
		ofLine(0,0,0,box.height);
		//ofDrawBitmapString( labelString, width / 2 - labelStringWidth*8 /2 + 1, height/2 + 4 + 1);
		ofTranslate(HMARGIN+1 ,box.height /2 + VMARGIN+1);

	}
	else{
        //ofDrawBitmapString( labelString, width / 2 - labelStringWidth*8 /2, height/2 + 4);
        ofTranslate(HMARGIN ,box.height /2 + VMARGIN+1);
	}
	if(!bToggled){
		vector<ofPath>::iterator iter = m_labelshape.begin();
		for(;iter!=m_labelshape.end();iter++){
			(*iter).draw();
		}
	}
	else{
		vector<ofPath>::iterator iter = m_altlabelshape.begin();
		for(;iter!=m_altlabelshape.end();iter++){
			(*iter).draw();
		}
	}

	//ofDrawBitmapString( ofToString(getValue(),numberDisplayPrecision), width+5,height);
	ofPopMatrix();
	ofSetLineWidth(1.0);
	ofDisableAlphaBlending();
}

//----------------------------------------------------
void CFHButton::mouseMoved(ofMouseEventArgs& event)
{
	if(bEnabled && box.inside(event.x,event.y)){
		bHasFocus = true;
	}
	else{
		bHasFocus = false;
	}
}
void CFHButton::mouseDragged(ofMouseEventArgs& event){
	if (bEnabled && bHasFocus){
		//updatePercentFromMouse (event.x, event.y);
	}
}
void CFHButton::mousePressed(ofMouseEventArgs& event){

	if (bEnabled && box.inside(event.x, event.y) && bHasFocus){
		bPressed = true;
		if(m_eButtonType==FHBTTOGGLE)
			bToggled = !bToggled;
	}
}
void CFHButton::mouseReleased(ofMouseEventArgs& event){
	if (bEnabled && bHasFocus){
		if (box.inside(event.x, event.y)){
			int ea = 1;
			ofNotifyEvent(btnEvent,ea,this);
		}
	}
	bPressed = false;
}

void CFHButton::defaultMouseHandler(ofMouseEventArgs & event){
	if(box.inside(event.x,event.y)){
		std::ostringstream ost;
		ost << "defaultMouseHandler:" << " x:" << event.x << " y:" << event.y;
		ost << " button: " << event.button;
		ofLog(OF_LOG_ERROR,"Failed to get the environment using GetEnv()");
	}
}

void CFHButton::defaultTouchHandler(ofTouchEventArgs & event){
	if(box.inside(event.x,event.y)){
		std::ostringstream ost;
		ost << "defaultTouchHandler:" << " x:" << event.x << " y:" << event.y;
		ost << " type: " << event.type;
		ofLog(OF_LOG_ERROR,"Failed to get the environment using GetEnv()");
	}
}



