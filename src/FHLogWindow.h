//
//  FHLogWindow.h
//  ZKJ_Simulator
//
//  Created by auser on 15/4/23.
//
//

#ifndef ZKJ_Simulator_FHLogWindow_h
#define ZKJ_Simulator_FHLogWindow_h
#include "ofxBaseGui.h"

#include "ofxSlider.h"
#include "ofxButton.h"
#include "ofParameterGroup.h"

#define LOGLINENUM 5
#define LOGBUFNUM   (LOGLINENUM*10)
#define LOGWINDEFWIDTH  600
#define LOGNL	CFHLogWindow::nl

class CFHLogWindow : public ofxBaseGui {
public:
    CFHLogWindow();
    CFHLogWindow(const ofParameterGroup & parameters, string _filename="settings.xml", float x = 10, float y = 10);
    virtual ~CFHLogWindow() {}
    virtual CFHLogWindow * setup(string collectionName,float windowwidth=LOGWINDEFWIDTH,string filename="settings.xml", float x = 10, float y = 10);
    virtual CFHLogWindow * setup(const ofParameterGroup & parameters, float windowwidth=LOGWINDEFWIDTH,string filename="settings.xml", float x = 10, float y = 10);
    
    void add(ofxBaseGui * element);
    void add(const ofParameterGroup & parameters);
    void add(ofParameter<float> & parameter);
    void add(ofParameter<int> & parameter);
    void add(ofParameter<bool> & parameter);
    void add(ofParameter<string> & parameter);
    void add(ofParameter<ofVec2f> & parameter);
    void add(ofParameter<ofVec3f> & parameter);
    void add(ofParameter<ofVec4f> & parameter);
    void add(ofParameter<ofColor> & parameter);
    void add(ofParameter<ofShortColor> & parameter);
    void add(ofParameter<ofFloatColor> & parameter);
    
    void minimize();
    void maximize();
    void minimizeAll();
    void maximizeAll();
    
    void setWidthElements(float w);
    
    void clear();
    
    virtual bool mouseMoved(ofMouseEventArgs & args);
    virtual bool mousePressed(ofMouseEventArgs & args);
    virtual bool mouseDragged(ofMouseEventArgs & args);
    virtual bool mouseReleased(ofMouseEventArgs & args);
    
    
    vector<string> getControlNames();
    int getNumControls();
    
    ofxIntSlider & getIntSlider(string name);
    ofxFloatSlider & getFloatSlider(string name);
    ofxToggle & getToggle(string name);
    ofxButton & getButton(string name);
    CFHLogWindow & getGroup(string name);

	static const char nl='\n';
    
    ofxBaseGui * getControl(string name);
    ofxBaseGui * getControl(int num);
    
    virtual ofAbstractParameter & getParameter();
    
    virtual void setPosition(ofPoint p);
    virtual void setPosition(float x, float y);
    template<typename Foo>
    CFHLogWindow & operator<< (const Foo & value){
        strbuf << value;
        return *this;
    }
    ofPoint m_lastDragPos;
    CFHLogWindow & operator<<(const char value){
        if(value=='\n'){
            strvalues.push_back(strbuf.str());
            strbuf.clear();
            strbuf.str("");
            if(strvalues.size() > LOGBUFNUM){
                strvalues.erase(strvalues.begin());
            }
            if(m_logindex == strvalues.size() - 2)
                m_logindex++;
            generateDraw();
        }
        else
            strbuf << value;
		return *this;
    }
protected:
    virtual void render();
    virtual bool setValue(float mx, float my, bool bCheck);
    void sizeChangedCB();
    
    float spacing,spacingNextElement;
    float header;
    
    template<class ControlType>
    ControlType & getControlType(string name);
    
    virtual void generateDraw();
    
    vector <ofxBaseGui *> collection;
    ofParameterGroup parameters;
    
    string filename;
    bool minimized;
    bool bGuiActive;
    
    CFHLogWindow * parent;
    ofPath border, headerBg;
    ofVboMesh textMesh;
    stringstream strbuf;
private:
    ofParameter<string> logstring[LOGLINENUM];
    vector<string> strvalues;
    int m_logindex;
};



#endif
