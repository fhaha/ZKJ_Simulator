#pragma once

#include "ofMain.h"
#include <ofxNetwork.h>
#include "ofxXmlSettings.h"
#include "ofxAssimpModelLoader.h"
#include "zdpkg.h"
#include "myButton.h"
#include "FHButton.h"
#include "FHButtonPanel.h"

#include <Poco/Notification.h>
#include <Poco/NotificationQueue.h>
#include <Poco/AutoPtr.h>
#include <ofxAssimpModelLoader.h>
#include <ofxNetwork.h>
#include <ofxGui.h>
#include "FHCurveGraph.h"
#include "ZKJSerialThread.h"
#include "MotorSerialThread.h"
#include "InitFWThread.h"
#include "FHLogWindow.h"
#include "ofxTrueTypeFontUC.h"
#include "invpkg.h"


using Poco::Notification;
using Poco::NotificationQueue;
using Poco::AutoPtr;

#define LIGHTNUM 2

class ZKJNotification:public Notification{
public:
	ZKJNotification(inv_pkg &pkg):m_zkjdata(pkg){}
	ZKJNotification(pinv_pkg pkg):m_zkjdata(*pkg){}
	inv_pkg m_zkjdata;
};

class GCResNotification:public Notification{
public:
	GCResNotification(gcres_pkg &pkg):m_gcresdata(pkg){}
	GCResNotification(pgcres_pkg pkg):m_gcresdata(*pkg){}
	gcres_pkg m_gcresdata;
};

class OriAccelNotification:public Notification{
public:
	OriAccelNotification(float ax,float ay,float az):m_ax(ax),m_ay(ay),m_az(az){}
	float m_ax;
	float m_ay;
	float m_az;
};

template<class pkgclass>
class  CustomNotification:public Notification{
public:
	CustomNotification(pkgclass & pkg):m_customdata(pkg){}
	CustomNotification(pkgclass * pkg):m_customdata(*pkg){}
	pkgclass m_customdata;
};

class CarPosNotification:public Notification{
public:
	CarPosNotification(carpos_pkg &pkg):m_carposdata(pkg){}
	CarPosNotification(pcarpos_pkg pkg):m_carposdata(*pkg){}
	carpos_pkg m_carposdata;
};

class ZKJCAPNotification:public Notification{
public:
	ZKJCAPNotification(cap_pkg &pkg):m_capdata(pkg){}
	ZKJCAPNotification(pcap_pkg pkg):m_capdata(*pkg){}
	cap_pkg m_capdata;
};

class ZKJERRORNotification:public Notification{
public:
	ZKJERRORNotification(pzkjerr_st pkg):m_zkjerr(*pkg){}
	ZKJERRORNotification(zkjerr_st & pkg):m_zkjerr(pkg){}
	zkjerr_st m_zkjerr;
};


class ofApp : public ofBaseApp{
    
public:
    void setup();
    virtual void update();
    void draw();
    virtual void exit(); 
    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y );
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);
    void ZDPressed(int& eventarg);
    void loadConfig(void);
	void localCalcPosture();
    NotificationQueue m_zkjQueue;
	CFHLogWindow & getLW(){
		return m_LogWindow;
	}
	void writeFWCfg(fwcfg & cfg);

	 ofParameter<int> m_getLVn;
    ofParameter<int> m_getLVe;
    ofParameter<int> m_getLVu;
	ofParameter<float> m_fGVegg[3];
	ofParameter<float> m_fGH;
	ofParameter<float> m_fGLambda;
	ofParameter<float> m_fGFai;
    CZKJSerialThread m_ZKJSim;
private:
	bool m_bGuiInited;
    ofSerial m_GPSSerial;
    ofxUDPManager udpConnection;
    ofxAssimpModelLoader m_ModelPlane;
    ofxAssimpModelLoader m_ModelPlaneP;
    ofxAssimpModelLoader m_ModelPlaneL;
	CMotorSerialThread m_Motor;
	CInitFWThread m_FWConfig;
    string m_szZKJSerialName;
	int m_iZKJBaud;
    string m_szModeName;
    string m_szHXZSerialName;
    string m_szGPSSerialName;
    string m_szMotorSerialName;
    string m_szPlanFilename;
    string  m_szLogFile;
    ofLight	light[LIGHTNUM];
    float m_lightRadius;
    ofParameter<float> m_fPsi;
    ofParameter<float> m_fTheta;
    ofParameter<float> m_fGamma;
    ofParameter<float> m_fPsiP;	//For Posture
    ofParameter<float> m_fThetaP;
    ofParameter<float> m_fGammaP;
    ofParameter<float> m_fPsiL; //For LocalCalc
    ofParameter<float> m_fThetaL;
    ofParameter<float> m_fGammaL;

	ofParameter<bool> m_bDebug;

    ofxPanel m_SensorDataPanel;

    ofxPanel m_PosturePanel;
    ofxPanel m_CalcDataPanel;

	ofParameter<float> m_fCarGpsDeltaTime;
	ofParameter<float> m_fAegg[3];
	ofParameter<float> m_fVegg[3];
	ofParameter<float> m_fH;
	ofParameter<float> m_fLambda;
	ofParameter<float> m_fFai;
	ofParameter<float> m_fGAegg[3];
	ofParameter<float> m_fGPSDelT;
	ofParameter<float> m_fDelT;
	ofParameter<bool> m_bRealTime;
	ofParameter<bool> m_bGPSData;
	ofParameter<bool> m_bFilter;
	ofParameter<bool> m_bMean;
	ofParameter<bool> m_bDemo;

	ofxPanel m_CarPanel;
    bool m_bZDEnabled;
    st_zd_pkg m_ZDPkg;
    //st_gps_inv m_GPSPkg;
    initpos_pkg m_InitPosPkg;
	st_senddata_pkg m_SendDataPkg;
    void PosturePanelInit(int w , int h);
    void PosturePanelResize(int w ,int h);
    ofXml m_MainConfig;

    //Gravity Check
    ofxPanel m_DeltaConfigPanel;
    ofParameter<float> dfx;
    ofParameter<float> dfy;
    ofParameter<float> dfz;
    ofParameter<float> ffactorx;
    ofParameter<float> ffactory;
    ofParameter<float> ffactorz;
    ofParameter<float> dwx;
    ofParameter<float> dwy;
    ofParameter<float> dwz;
    ofParameter<float> wxfactor;
    ofParameter<float> wyfactor;
    ofParameter<float> wypfactor;
    ofParameter<float> wzfactor;
	ofParameter<float>	mgamma;
	//ofParameter<bool> m_bExtraWY;

	float m_fa[3][3]; 
	float m_wa[3][3];

    ofParameter<float> fibbx;
    ofParameter<float> fibby;
    ofParameter<float> fibbz;
    ofParameter<float> foibbx;
    ofParameter<float> foibby;
    ofParameter<float> foibbz;

	//for motor 
	ofParameter<float> fmibbx;
    ofParameter<float> fmibby;
    ofParameter<float> fmibbz;


    ofParameter<float> fpibbx;
    ofParameter<float> fpibby;
    ofParameter<float> fpibbz;
    ofParameter<float> wibbx;
    ofParameter<float> wibby;
    ofParameter<float> wibbz;
    ofParameter<float> wpibbx;
    ofParameter<float> wpibby;
    ofParameter<float> wpibbz;
    //ofParameter<float> temperature[4];
	ofParameter<float> i2ctemp;
    ofParameter<float> Ggc;	//for Gravity Check step 4 output g
    ofParameter<float> Gngc;//for Gravity Check step4 output gn
    ofParameter<float> dg1;	//for gravity Check step4 output delta g 1
    ofParameter<float> dg2;
    ofParameter<float> dg3;
    ofParameter<float> figgz;
	ofParameter<int> totalCalc;
	ofParameter<int> notCalc;
	ofParameter<int> canCalc;
	ofParameter<int> notNiuton;

	void addCalcTime(){
		totalCalc++;
	}
	void addCannotCalc(){
		notCalc++;
	}
	void addCanCalc(){
		canCalc++;
	}
	void addNotNiuton(){
		notNiuton++;
	}
	float localnewton(float * fibb,float * Gfegg,float theta,float initGamma);
    
    ofXml m_GravityConfig;
    void CalcDataPanelResize(int w,int h);
    void CalcDataPanelInit(int w,int h);
    void DeltaConfigInit(int w,int h);
    void GravityPanelResize(int w ,int h);
    void FibbCurveResize(int w,int h);
    void WibbCurveResize(int w,int h);
    void logWindowResize(int w,int h);
    void lightResize(int w,int h);
    void SensorDataPanelInit(int w,int h);
    void SensorDataPanelResize(int w , int h);

    CMyButton GravityCheckBtn;
    CMyButton PostureCheckBtn;
	CFHButtonPanel btnPanel;
	CFHButton GA5Btn;
	CFHButton GA10Btn;
	CFHButton GA20Btn;
    CMyButton btnZD;
    CMyButton btnInitPos;
	CMyButton btnCap;
	CFHButton btnCar;
	CFHButton btnStop;
	CFHButton sendData;
	CFHButton btnAdjust;
	CFHButton btnDynG;
	CFHButton btnStartAccelCali;
	CFHButton btnOriAccel;
	CFHButton btnIdle;
	CMyButton btnBurnFW;
	CMyButton btnCalcAV;
	CMyButton btnAC;

    void loadGravityListener();
    void saveGravityListener();
    void GravityCheckPressed(int& eventarg);
	void CalcAVPressed(int & eventarg);
	void HFAdjustPressed(int & eventarg);
	void GA5Pressed(int & eventarg);
	void GA10Pressed(int & eventarg);
	void GA20Pressed(int & eventarg);
	void DynGPressed(int & eventarg);
	void ACPressed(int & eventarg);
    void PostureCheckPressed(int& eventarg);
    void InitPostureCheckPressed(int & eventarg);
	void CarPressed(int &eventarg);
	void CapturePressed(int & eventarg);
	void StopPressed(int& eventarg);
	void BurnFWPressed(int &eventarg);
	void SendDataPressed(int & eventarg);
	void StartAccelCali(int & eventarg);
	void OriAccelMode(int & eventarg);
	void IdleMode(int & eventarg);

    CFHCurveGraph m_FoibbCurve;
    CFHCurveGraph m_FibbCurve;
	CFHCurveGraph m_FmibbCurve;
    CFHCurveGraph m_WibbCurve;
	CFHCurveGraph m_WpibbCurve;
	CFHCurveGraph m_AeggCurve;
	CFHCurveGraph m_VeggCurve;
	CFHCurveGraph m_GAeggCurve;
	CFHCurveGraph m_GVeggCurve;
	CFHCurveGraph m_GammaCurve;
	CFHCurveGraph m_CustomCurve;
    CFHLogWindow m_LogWindow;


    void buttonsResize(int w,int h);
    
    ofxPanel motorPanel;
    ofxButton btnAXAdjust;
    ofxButton btnAYAdjust;
	ofxButton btnAZAdjust;
    ofxButton btnGXAdjust;
    ofxButton btnGYAdjust;
	ofxButton btnGZAdjust;
    ofxButton btnFar;
    ofxButton btnNear;
	ofxButton btnXAdjust;
	ofxButton btnYAdjust;
	ofParameter<bool> m_bAdjustMax;
    void AXAdjustFunc();
    void AYAdjustFunc();
	void AZAdjustFunc();
    void GXAdjustFunc();
    void GYAdjustFunc();
	void GZAdjustFunc();
    void FarFunc();
    void NearFunc();
	void XAdjustFunc();
	void YAdjustFunc();
    void motorPanelDeinit();
    
    
    void motorPanelResize(int w , int h);
    void motorPanelInit(int w,int h);
    
   
	ofParameter<int> m_getLVnLast;
	ofParameter<int> m_getLVeLast;
	ofParameter<int> m_getLVuLast;
	ofParameter<int> m_getVn;
    ofParameter<int> m_getVe;
    ofParameter<int> m_getVu;
	ofParameter<int> m_getVnLast;
	ofParameter<int> m_getVeLast;
	ofParameter<int> m_getVuLast;
	ofParameter<float> m_GCGamma;
	ofParameter<float> m_calcG;
	ofParameter<float> m_PosGamma;
	ofParameter<float> m_DeltaGamma;
	ofParameter<int> m_iPosCircle;
	ofParameter<int> m_iGraCircle;
	ofParameter<float> m_CircleSpeed;
	float m_lastGGamma;
	float m_lastPGamma;

	ofxPanel m_ComparePanel;
	ofParameter<float> m_lFibbx;
	ofParameter<float> m_rFibbx;
	ofParameter<float> m_lFibby;
	ofParameter<float> m_rFibby;
	ofParameter<float> m_lFibbz;
	ofParameter<float> m_rFibbz;

	int m_GPSDataCount;
    float m_GPSLastTime;
    float m_GPSCurTime;
	float m_lastdtime;
	std::ofstream lcfile; 
	void planeResize(int w,int h);	

	ofxTrueTypeFontUC zwFont;
	ofxTrueTypeFontUC titleFont;
	ofxTrueTypeFontUC btnFont;
	std::string m_sFontName;
	int fontsize ;
};
