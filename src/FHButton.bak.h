/*
 *  ofxSimpleSlider.h
 *  Created by Golan Levin on 2/24/12.
 *
 */


#pragma once

#include <ofMain.h>
#include "ofxTrueTypeFontUC.h"

enum FHBT{
	FHBTNORMAL,
	FHBTTOGGLE
};

class CFHButton {

	public:
		CFHButton(){
			x = 0;
			y = 0;
			box.set(x,y, 50, 20);

			bHasFocus = false;
			bEnabled = true;
			bPressed = false;
			bToggled = false;
			m_bHide = false;
			m_eButtonType = FHBTNORMAL;
			m_labelshape.clear();
		}
		~CFHButton(){
            removeDefaultListener();
		}
		//template<class strclass>
		//void    setup(strclass wstr,string fontname="STHUPO.TTF");

        void setup(const wchar_t * str,string fontname="STHUPO.TTF");
		void setup(const char * str,string fontname="STHUPO.TTF");
		void setup(const wchar_t * str,const wchar_t * altstr,string fontanme="STHUPO.TTF");
		void	resize(float inw,float inh);
		void	move(float inx,float iny);
		void	move(ofPoint & pos);
		void	enable();
		void	disable();
		bool	isEnabled();
		bool isToggled();
		void	draw(ofEventArgs& event);
		void	mouseMoved(ofMouseEventArgs& event);
		void	mouseDragged(ofMouseEventArgs& event);
		void	mousePressed(ofMouseEventArgs& event);
		void	mouseReleased(ofMouseEventArgs& event);
		void	defaultMouseHandler(ofMouseEventArgs & event);
		void	defaultTouchHandler(ofTouchEventArgs & event);

        template<class labeltype>
		void	setLabelString (labeltype str,int len);

		ofEvent<int> btnEvent;
		float getX(){return x;}
		float getY() { return y;}
		float getWidth() { return box.width;}
		float getHeight() { return box.height;}
		bool m_bHide;
    private:
        void addDefaultListener();
        void removeDefaultListener();
	protected:
		float	x;
		float	y;
		ofRectangle box;
		bool	bHasFocus;
		bool	bEnabled;
		bool	bPressed;
		bool    bToggled;
        ofxTrueTypeFontUC zhFont;
		//string	labelString;
        vector<ofPath> m_labelshape;
		vector<ofPath> m_altlabelshape;
		FHBT m_eButtonType;


};
